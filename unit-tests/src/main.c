#include "unity.h"
#include "ll-backhaul-config.h"
#include "ll-backhaul-hal.h"


extern void set_up_LTE_M_NON_RTOS(void);
extern void tear_down_LTE_M_NON_RTOS(void);

extern void test_LTE_M_NON_RTOS_SendOneMessage_TooSmall(void);
extern void test_LTE_M_NON_RTOS_SendOneMessage_Mailbox(void);
extern void test_LTE_M_NON_RTOS_SendOneMessage_Small(void);
extern void test_LTE_M_NON_RTOS_SendOneMessage_Medium(void);
extern void test_LTE_M_NON_RTOS_SendOneMessage_Large(void);
extern void test_LTE_M_NON_RTOS_SendOneMessage_TooLarge(void);
extern void test_LTE_M_NON_RTOS_SendHalfQueue(void);
extern void test_LTE_M_NON_RTOS_SendFullQueue(void);
extern void test_LTE_M_NON_RTOS_SendMoreThanFullQueue(void);
extern void test_LTE_M_NON_RTOS_RecieveOneDownlinkMessage(void);
extern void test_LTE_M_NON_RTOS_RecieveTwoDownlinkMessages(void);
extern void test_LTE_M_NON_RTOS_RecieveThreeDownlinkMessages(void);


void setUp(void)
{
    // Ran before every test.
}

void tearDown(void)
{
    // Ran after every test.
}

int main(int argc, char** argv)
{
//    UnityParseOptions(argc, argv);

    // TODO: Make this driver specific.
    int ret = ll_tty_open(argv[1], 115200, LL_LTE_M_BACKHAUL);
    if (ret != 0)
    {
        return -1;
    }


    int test_ret = 0;
#if LL_BH_LTE_M_EN
#if LL_LTE_USE_RTOS
#if LL_LTE_FOTA_EN
    // TODO: LTE_RTOS_FOTA
    UnityBegin("test-LTE-Rtos.c");

    test_ret = UnityEnd();
    if (test_ret != 0)
    {
        return test_ret;
    }
#endif // LL_LTE_FOTA_EN
    // TODO: LTE_RTOS
    UnityBegin("test-LTE-Rtos.c");

    test_ret = UnityEnd();
    if (test_ret != 0)
    {
        return test_ret;
    }
#else // NOT LL_LTE_USE_RTOS
#if LL_LTE_FOTA_EN
    // TODO: LTE_NON_RTOS_FOTA
    UnityBegin("test-LTE-NonRtos.c");

    test_ret = UnityEnd();
    if (test_ret != 0)
    {
        return test_ret;
    }
#endif // LL_LTE_FOTA_EN
    // LTE_NON_RTOS
    UnityBegin("test-LTE-NonRtos.c");

    set_up_LTE_M_NON_RTOS();

    RUN_TEST(test_LTE_M_NON_RTOS_SendOneMessage_TooSmall);
    RUN_TEST(test_LTE_M_NON_RTOS_SendOneMessage_Mailbox);
    RUN_TEST(test_LTE_M_NON_RTOS_SendOneMessage_Small);
    RUN_TEST(test_LTE_M_NON_RTOS_SendOneMessage_Medium);
    RUN_TEST(test_LTE_M_NON_RTOS_SendOneMessage_Large);
    RUN_TEST(test_LTE_M_NON_RTOS_SendOneMessage_TooLarge);
    RUN_TEST(test_LTE_M_NON_RTOS_SendHalfQueue);
    RUN_TEST(test_LTE_M_NON_RTOS_SendFullQueue);
    RUN_TEST(test_LTE_M_NON_RTOS_SendMoreThanFullQueue);
    RUN_TEST(test_LTE_M_NON_RTOS_RecieveOneDownlinkMessage);
    RUN_TEST(test_LTE_M_NON_RTOS_RecieveTwoDownlinkMessages);
    RUN_TEST(test_LTE_M_NON_RTOS_RecieveThreeDownlinkMessages);

    tear_down_LTE_M_NON_RTOS();

    test_ret = UnityEnd();
    if (test_ret != 0)
    {
        return test_ret;
    }
#endif // LL_LTE_USE_RTOS
#endif // LL_BH_LTE_M_EN
#if LL_BH_SYMPHONY_EN
#if LL_SYM_USE_RTOS
#if LL_SYM_FOTA_EN
    // TODO: SYM_RTOS_FOTA
    UnityBegin("test/TestProductionCode.c");

    test_ret = UnityEnd();
    if (test_ret != 0)
    {
        return test_ret;
    }
#endif // LL_SYM_FOTA_EN
    // TODO: SYM_RTOS
    UnityBegin("test/TestProductionCode.c");

    test_ret = UnityEnd();
    if (test_ret != 0)
    {
        return test_ret;
    }
#else // NOT LL_SYM_USE_RTOS
#if LL_SYM_FOTA_EN
    // TODO: SYM_NON_RTOS_FOTA
    UnityBegin("test/TestProductionCode.c");

    test_ret = UnityEnd();
    if (test_ret != 0)
    {
        return test_ret;
    }
#endif // LL_SYM_FOTA_EN
    // TODO: SYM_NON_RTOS
    UnityBegin("test/TestProductionCode.c");

    test_ret = UnityEnd();
    if (test_ret != 0)
    {
        return test_ret;
    }
#endif // LL_SYM_USE_RTOS
#endif // LL_BH_SYMPHONY_EN
}
