///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                  Thomas Steinholz  <thomas.steinholz@link-labs.com>,     August 2018        ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#include "ll-lte_m-task.h"
#include "ll_ifc_utils.h"
#include "unity.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


///////////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------- Private Variable Declarations --------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////

typedef enum
{
    NONE             = 0,
    TX_QUEUE_CLEARED = 1,
    MSG_FAILED       = 2,
    MSG_RXED         = 3,
} cb_t;

typedef struct
{
    uint8_t num_txed;
    uint8_t buff[MAX_USER_UPLINK_LENGTH_BYTES];
    int16_t len;
    int8_t port;
    cb_t type;
} callback_evt_t;

static lte_callbacks_t _callbacks;
static struct time start_time; // Used to track the start time of functions.
static struct time end_time;   // Used to track the end time of functions.
static callback_evt_t _callback_event; // Used for callback context.

#undef INTERNAL_CHECK // Does not work.
#ifdef INTERNAL_CHECK
// Peak into the data buffers of the LTE-M driver.
extern lte_msg_t _tx_msgs[LL_LTE_MAX_TX_MSGS];
#endif // INTERNAL_CHECK



///////////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------------ Callback Definitions  ------------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////

static void _lte_tx_queue_cleared(uint8_t num_txed, void *context)
{
    // Convert the context to an event.
    callback_evt_t *evt = (callback_evt_t*) context;

    // Insure that there is not a pending event left unprocessed.
    TEST_ASSERT_EQUAL(NONE, evt->type);
    evt->num_txed = num_txed;
    evt->type = TX_QUEUE_CLEARED;
}

static void _lte_message_failed(uint8_t *buff, uint16_t len, int8_t port, void *context)
{
    // Convert the context to an event.
    callback_evt_t *evt = (callback_evt_t*) context;

    // Insure that there is not a pending event left unprocessed.
    TEST_ASSERT_EQUAL(NONE, evt->type);

    memcpy(evt->buff, buff, len);
    evt->len = len;
    evt->port = port;
    evt->type = MSG_FAILED;
}

static void _lte_received_message(uint8_t *buff, uint16_t len, int8_t port, void *context)
{
    // Convert the context to an event.
    callback_evt_t *evt = (callback_evt_t*) context;

    // Insure that there is not a pending event left unprocessed.
    TEST_ASSERT_EQUAL(NONE, evt->type);

    memcpy(evt->buff, buff, len);
    evt->len = len;
    evt->port = port;
    evt->type = MSG_RXED;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//---------------------------------------- Priavte Functions  -----------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////

static void _generate_random_data(uint8_t *dest_buffer, int16_t len)
{
    for (uint16_t i = 0; i < len; ++i)
    {
        dest_buffer[i] = rand() % 255;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------- LTE-M NON-RTOS Test Definitions  ------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////

void set_up_LTE_M_NON_RTOS(void)
{
    // Initialize Timekeeping Variables.
    start_time.tv_sec  = 0;
    start_time.tv_nsec = 0;
    end_time.tv_sec    = 0;
    end_time.tv_nsec   = 0;

    // Initialize Callback Event.
    _callback_event.num_txed = 0;
    _callback_event.len      = 0;
    _callback_event.port     = 0;
    _callback_event.type     = NONE;
    memset(_callback_event.buff, 0, MAX_USER_UPLINK_LENGTH_BYTES);

    // Initialize Callbacks.
    _callbacks.lte_tx_queue_cleared = _lte_tx_queue_cleared;
    _callbacks.lte_message_failed   = _lte_message_failed;
    _callbacks.lte_received_message = _lte_received_message;
    _callbacks.context              = &_callback_event;

    // Initialize LTE-M Driver.
    ll_bh_hal_gettime(&start_time);
    lte_return_t lte_ret = lte_init_driver(&_callbacks);
    ll_bh_hal_gettime(&end_time);
    TEST_ASSERT_EQUAL(LTE_OK, lte_ret);

    // Validate time to execute (LTE_COMMS_TIMEOUT + 3s).
    TEST_ASSERT_LESS_THAN(47, ll_difftime(&start_time, &end_time));

    // Validate there is nothing pending transmit.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());
}

void tear_down_LTE_M_NON_RTOS(void)
{
    // Initialize Callbacks.
    _callbacks.lte_tx_queue_cleared = NULL;
    _callbacks.lte_message_failed   = NULL;
    _callbacks.lte_received_message = NULL;
    _callbacks.context              = NULL;
}

void test_LTE_M_NON_RTOS_SendOneMessage_TooSmall(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_SendOneMessage_TooSmall...");
    const int16_t LEN = 1;
    uint8_t buffer[LEN];
    _generate_random_data(buffer, LEN);

    // Size cannot be 0.
    TEST_ASSERT_EQUAL(LTE_INVALID_PARAM, lte_send_message(buffer, 0, 0));

    // Nothing should be pending transmit.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());

    // Buffer cannot be NULL.
    TEST_ASSERT_EQUAL(LTE_INVALID_PARAM, lte_send_message(NULL, LEN, 0));

    // Nothing should be pending transmit.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());
}

void test_LTE_M_NON_RTOS_SendOneMessage_Mailbox(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_SendOneMessage_Mailbox...");

    // Save the time the message was queued.
    ll_bh_hal_gettime(&start_time);

    // Message should be able to send.
    TEST_ASSERT_EQUAL(LTE_OK, lte_mailbox_req());

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(1, lte_get_tx_queue_size());

    LL_LOGF("TEST: Waiting for TX_DONE flag...");

    // Wait for the message to get sent.
    while(_callback_event.type == NONE)
    {
        // Check LTE-M Device for IRQ Flags.
        lte_service_irq();

        // Validate time to transmit is less than (LTE_MSG_TIMEOUT + 3s).
        TEST_ASSERT_LESS_THAN(47, ll_difftime_from_now(&start_time));
    }

    // Save time of completion.
    ll_bh_hal_gettime(&end_time);
    printf("Time to transmit mailbox req payload: %is", ll_difftime(&start_time, &end_time));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());

    // The TX queue cleared callback should have been triggered.
    TEST_ASSERT_EQUAL(TX_QUEUE_CLEARED, _callback_event.type);

    // Verify the number of messages txed.
    TEST_ASSERT_EQUAL(1, _callback_event.num_txed);

    // TODO: Check conductor.
}

void test_LTE_M_NON_RTOS_SendOneMessage_Small(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_SendOneMessage_Small...");
    const int16_t LEN = MAX_USER_UPLINK_LENGTH_BYTES / 8;
    uint8_t buffer[LEN];
    _generate_random_data(buffer, LEN);

    // Save the time the message was queued.
    ll_bh_hal_gettime(&start_time);

    // Message should be able to send.
    TEST_ASSERT_EQUAL(LTE_OK, lte_send_message(buffer, LEN, 0));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(1, lte_get_tx_queue_size());

    // Verify internal LTE-M driver state machine.
#ifdef INTERNAL_CHECK
    for (int16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
    {
        // Assuming there is a packet_id, there should be our message.
        if (_tx_msgs[i].packet_id)
        {
            // Verify the length was passed correctly.
            TEST_ASSERT_EQUAL(LEN, _tx_msgs[i].len);

            // Verify the port was passed correctly.
            TEST_ASSERT_EQUAL(0, _tx_msgs[i].port);

            // Verify the payload was passed correctly.
            for (int16_t j = 0; j < LEN; ++j)
            {
                TEST_ASSERT_EQUAL(_tx_msgs[i].buffer[j], buffer[j]);
            }
        }
    }
#endif // INTERNAL_CHECK


    // Wait for the message to get sent.
    while(_callback_event.type == NONE)
    {
        // Check LTE-M Device for IRQ Flags.
        lte_service_irq();

        // Validate time to transmit is less than (LTE_MSG_TIMEOUT + 3s).
        TEST_ASSERT_LESS_THAN(47, ll_difftime_from_now(&start_time));
    }

    // Save time of completion.
    ll_bh_hal_gettime(&end_time);
    printf("Time to transmit %i byte payload: %is", LEN, ll_difftime(&start_time, &end_time));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());

    // The TX queue cleared callback should have been triggered.
    TEST_ASSERT_EQUAL(TX_QUEUE_CLEARED, _callback_event.type);

    // Verify the number of messages txed.
    TEST_ASSERT_EQUAL(1, _callback_event.num_txed);

    // TODO: Check conductor.
}

void test_LTE_M_NON_RTOS_SendOneMessage_Medium(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_SendOneMessage_Medium...");

    const int16_t LEN = MAX_USER_UPLINK_LENGTH_BYTES / 2;
    uint8_t buffer[LEN];
    _generate_random_data(buffer, LEN);

    // Save the time the message was queued.
    ll_bh_hal_gettime(&start_time);

    // Message should be able to send.
    TEST_ASSERT_EQUAL(LTE_OK, lte_send_message(buffer, LEN, 0));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(1, lte_get_tx_queue_size());

#ifdef INTERNAL_CHECK
    // Verify internal LTE-M driver state machine.
    for (int16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
    {
        // Assuming there is a packet_id, there should be our message.
        if (_tx_msgs[i].packet_id)
        {
            // Verify the length was passed correctly.
            TEST_ASSERT_EQUAL(LEN, _tx_msgs[i].len);

            // Verify the port was passed correctly.
            TEST_ASSERT_EQUAL(0, _tx_msgs[i].port);

            // Verify the payload was passed correctly.
            for (int16_t j = 0; j < LEN; ++j)
            {
                TEST_ASSERT_EQUAL(_tx_msgs[i].buffer[j], buffer[j]);
            }
        }
    }
#endif // INTERNAL_CHECK

    // Wait for the message to get sent.
    while(_callback_event.type == NONE)
    {
        // Check LTE-M Device for IRQ Flags.
        lte_service_irq();

#ifdef INTERNAL_CHECK
        // Validate time to transmit is less than (LTE_MSG_TIMEOUT + 3s).
        TEST_ASSERT_EQUAL(_tx_msgs[i].buffer[j], buffer[j]);
#endif // INTERNAL_CHECK
    }

    // Save time of completion.
    ll_bh_hal_gettime(&end_time);
    printf("Time to transmit %i byte payload: %is", LEN, ll_difftime(&start_time, &end_time));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());

    // The TX queue cleared callback should have been triggered.
    TEST_ASSERT_EQUAL(TX_QUEUE_CLEARED, _callback_event.type);

    // Verify the number of messages txed.
    TEST_ASSERT_EQUAL(1, _callback_event.num_txed);

    // TODO: Check conductor.
}

void test_LTE_M_NON_RTOS_SendOneMessage_Large(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_SendOneMessage_Large...");

    const int16_t LEN = MAX_USER_UPLINK_LENGTH_BYTES;
    uint8_t buffer[LEN];
    _generate_random_data(buffer, LEN);

    // Save the time the message was queued.
    ll_bh_hal_gettime(&start_time);

    // Message should be able to send.
    TEST_ASSERT_EQUAL(LTE_OK, lte_send_message(buffer, LEN, 0));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(1, lte_get_tx_queue_size());

#ifdef INTERNAL_CHECK
    // Verify internal LTE-M driver state machine.
    for (int16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
    {
        // Assuming there is a packet_id, there should be our message.
        if (_tx_msgs[i].packet_id)
        {
            // Verify the length was passed correctly.
            TEST_ASSERT_EQUAL(LEN, _tx_msgs[i].len);

            // Verify the port was passed correctly.
            TEST_ASSERT_EQUAL(0, _tx_msgs[i].port);

            // Verify the payload was passed correctly.
            for (int16_t j = 0; j < LEN; ++j)
            {
                // Check LTE-M Device for IRQ Flags.
                lte_service_irq();

                // Validate time to transmit is less than (LTE_MSG_TIMEOUT + 3s).
                TEST_ASSERT_EQUAL(_tx_msgs[i].buffer[j], buffer[j]);
            }
        }
    }
#endif // INTERNAL_CHECK

    // Wait for the message to get sent.
    while(_callback_event.type == NONE)
    {
        // Check LTE-M Device for IRQ Flags.
        lte_service_irq();

        // Validate time to transmit is less than (LTE_MSG_TIMEOUT + 3s).
        TEST_ASSERT_LESS_THAN(47, ll_difftime_from_now(&start_time));
    }

    // Save time of completion.
    ll_bh_hal_gettime(&end_time);
    printf("Time to transmit %i byte payload: %is", LEN, ll_difftime(&start_time, &end_time));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());

    // The TX queue cleared callback should have been triggered.
    TEST_ASSERT_EQUAL(TX_QUEUE_CLEARED, _callback_event.type);

    // Verify the number of messages txed.
    TEST_ASSERT_EQUAL(1, _callback_event.num_txed);

    // TODO: Check conductor.
}

void test_LTE_M_NON_RTOS_SendOneMessage_TooLarge(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_SendOneMessage_TooLarge...");

    const int16_t LEN = MAX_USER_UPLINK_LENGTH_BYTES + 1;
    uint8_t buffer[LEN];
    _generate_random_data(buffer, LEN);

    // Size cannot be larger than the MAX_USER_UPLINK_LENGTH_BYTES.
    TEST_ASSERT_EQUAL(LTE_INVALID_PARAM, lte_send_message(buffer, LEN, 0));

    // Nothing should be pending transmit.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());
}

void test_LTE_M_NON_RTOS_SendHalfQueue(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_SendHalfQueue...");
    const uint8_t NUM_TO_TX = LL_LTE_MAX_TX_MSGS / 2;
    uint8_t buffer[NUM_TO_TX][LL_LTE_MAX_TX_MSGS];

    for (uint8_t i = 0; i < NUM_TO_TX; ++i)
    {
        // Make the messages.
        _generate_random_data(buffer[i], MAX_USER_UPLINK_LENGTH_BYTES);

        // Message should be able to send.
        TEST_ASSERT_EQUAL(LTE_OK, lte_send_message(buffer[i], MAX_USER_UPLINK_LENGTH_BYTES, 0));

        // There should now be one message pending.
        TEST_ASSERT_EQUAL(i + 1, lte_get_tx_queue_size());
    }

    // Save the time the message was queued.
    ll_bh_hal_gettime(&start_time);

    // Wait for the message to get sent.
    while(_callback_event.type == NONE)
    {
        // Check LTE-M Device for IRQ Flags.
        lte_service_irq();

        // Validate time to transmit is less than (LTE_MSG_TIMEOUT + 15s).
        TEST_ASSERT_LESS_THAN(60, ll_difftime_from_now(&start_time));
    }

    // Save time of completion.
    ll_bh_hal_gettime(&end_time);
    printf("Time to transmit %i byte payload: %is",
            MAX_USER_UPLINK_LENGTH_BYTES, ll_difftime(&start_time, &end_time));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());

    // The TX queue cleared callback should have been triggered.
    TEST_ASSERT_EQUAL(TX_QUEUE_CLEARED, _callback_event.type);

    // Verify the number of messages txed.
    TEST_ASSERT_EQUAL(NUM_TO_TX, _callback_event.num_txed);

    // TODO: Check conductor.
}

void test_LTE_M_NON_RTOS_SendFullQueue(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_SendFullQueue...");

    const uint8_t NUM_TO_TX = LL_LTE_MAX_TX_MSGS;
    uint8_t buffer[NUM_TO_TX][LL_LTE_MAX_TX_MSGS];

    for (uint8_t i = 0; i < NUM_TO_TX; ++i)
    {
        // Make the messages.
        _generate_random_data(buffer[i], MAX_USER_UPLINK_LENGTH_BYTES);

        // Message should be able to send.
        TEST_ASSERT_EQUAL(LTE_OK, lte_send_message(buffer[i], MAX_USER_UPLINK_LENGTH_BYTES, 0));

        // There should now be one message pending.
        TEST_ASSERT_EQUAL(i + 1, lte_get_tx_queue_size());
    }

    // Save the time the message was queued.
    ll_bh_hal_gettime(&start_time);

    // Wait for the message to get sent.
    while(_callback_event.type == NONE)
    {
        // Check LTE-M Device for IRQ Flags.
        lte_service_irq();

        // Validate time to transmit is less than (LTE_MSG_TIMEOUT + 15s).
        TEST_ASSERT_LESS_THAN(60, ll_difftime_from_now(&start_time));
    }

    // Save time of completion.
    ll_bh_hal_gettime(&end_time);
    printf("Time to transmit %i byte payload: %is", MAX_USER_UPLINK_LENGTH_BYTES, ll_difftime(&start_time, &end_time));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());

    // The TX queue cleared callback should have been triggered.
    TEST_ASSERT_EQUAL(TX_QUEUE_CLEARED, _callback_event.type);

    // Verify the number of messages txed.
    TEST_ASSERT_EQUAL(NUM_TO_TX, _callback_event.num_txed);

    // TODO: Check conductor.
}

void test_LTE_M_NON_RTOS_SendMoreThanFullQueue(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_SendMoreThanFullQueue...");

    const uint8_t NUM_TO_TX = LL_LTE_MAX_TX_MSGS + 1;
    uint8_t buffer[NUM_TO_TX][LL_LTE_MAX_TX_MSGS];

    for (uint8_t i = 0; i < NUM_TO_TX; ++i)
    {
        // Make the messages.
        _generate_random_data(buffer[i], MAX_USER_UPLINK_LENGTH_BYTES);

        // Message should be able to send.
        TEST_ASSERT_EQUAL(LTE_OK, lte_send_message(buffer[i], MAX_USER_UPLINK_LENGTH_BYTES, 0));

        // There should now be one message pending.
        TEST_ASSERT_EQUAL(i + 1, lte_get_tx_queue_size());
    }

    // Save the time the message was queued.
    ll_bh_hal_gettime(&start_time);

    // Wait for the message to get sent.
    while(_callback_event.type == NONE)
    {
        // Check LTE-M Device for IRQ Flags.
        lte_service_irq();

        // Validate time to transmit is less than (LTE_MSG_TIMEOUT + 15s).
        TEST_ASSERT_LESS_THAN(60, ll_difftime_from_now(&start_time));
    }

    // Save time of completion.
    ll_bh_hal_gettime(&end_time);
    printf("Time to transmit %i byte payload: %is", MAX_USER_UPLINK_LENGTH_BYTES, ll_difftime(&start_time, &end_time));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());

    // The TX queue cleared callback should have been triggered.
    TEST_ASSERT_EQUAL(TX_QUEUE_CLEARED, _callback_event.type);

    // Verify the number of messages txed.
    TEST_ASSERT_EQUAL(NUM_TO_TX, _callback_event.num_txed);

    // TODO: Check conductor.
}

void test_LTE_M_NON_RTOS_RecieveOneDownlinkMessage(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_RecieveOneDownlinkMessage...");

    const int16_t LEN = MAX_USER_UPLINK_LENGTH_BYTES;
    uint8_t buffer[LEN];
    _generate_random_data(buffer, LEN);

    // TODO: Send message to device via conductor!

    // Save the time the message was queued.
    ll_bh_hal_gettime(&start_time);

    // Message should be able to send.
    TEST_ASSERT_EQUAL(LTE_OK, lte_send_message(buffer, LEN, 0));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(1, lte_get_tx_queue_size());

    // Wait for the message to get sent.
    while(_callback_event.type == NONE)
    {
        // Check LTE-M Device for IRQ Flags.
        lte_service_irq();

        // TODO: reasonable timeout
        // Validate time to transmit is less than (LTE_MSG_TIMEOUT + 3s).
        // TEST_ASSERT_LESS_THAN(47, ll_difftime_from_now(&start_time));
    }

    // Save time of completion.
    ll_bh_hal_gettime(&end_time);
    printf("Time to transmit %i byte payload: %is", LEN, ll_difftime(&start_time, &end_time));

    // There should now be one message pending.
    TEST_ASSERT_EQUAL(0, lte_get_tx_queue_size());

    // The TX queue cleared callback should have been triggered.
    TEST_ASSERT_EQUAL(MSG_RXED, _callback_event.type);

    // TODO: Validate Data RXed from Data sent.
}

void test_LTE_M_NON_RTOS_RecieveTwoDownlinkMessages(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_RecieveTwoDownlinkMessages...");
}


void test_LTE_M_NON_RTOS_RecieveThreeDownlinkMessages(void)
{
    LL_LOGF("");
    LL_LOGF("TEST: LTE_M_NON_RTOS_RecieveThreeDownlinkMessages...");
}
