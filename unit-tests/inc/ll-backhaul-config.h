///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                     Thomas Steinholz  <thomas.steinholz@link-labs.com>, FEB 2018            ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef LL_BACKHAUL_CONFIG_H
#define LL_BACKHAUL_CONFIG_H

/// Whether to use this config (development) or an external config (release).
#define LL_BH_USE_CONFIG (1)
#if LL_BH_USE_CONFIG

#define DEBUG_PRINT // To have prints in the unit tests.

///////////////////////////////////////////////////////////////////////////////////////////////////
// BACKEND CONFIGURATIONS.
///////////////////////////////////////////////////////////////////////////////////////////////////

#define LL_BH_SYMPHONY_EN   (0) ///< Use the Symphony Link Backhaul.
#define LL_BH_LTE_M_EN      (1) ///< Use the LTE-M Backhaul.


///////////////////////////////////////////////////////////////////////////////////////////////////
// SYMPHONY LINK SPECIFIC CONFIGURATIONS.
///////////////////////////////////////////////////////////////////////////////////////////////////
#if LL_BH_SYMPHONY_EN
#define LL_SYM_FOTA_EN      (0) ///< Enable Symphony Link FOTA Support.
#define LL_SYM_USE_RTOS     (0) ///< Enable the RTOS version of the Symphony Task.
#endif // LL_BH_SYMPHONY_EN


///////////////////////////////////////////////////////////////////////////////////////////////////
// LTE-M SPECIFIC CONFIGURATIONS.
///////////////////////////////////////////////////////////////////////////////////////////////////
#if LL_BH_LTE_M_EN
// Enabled Features.
#define LL_LTE_FOTA_EN      (0) ///< Enable LTE-M FOTA Support.
#define LL_LTE_USE_RTOS     (0) ///< Enable the RTOS version of the LTE-M Task.
// Memory Allocation.
#define LL_LTE_MAX_TX_MSGS  (6) ///< The Maximum number of TX messages that can be queued at a time.
#define LL_LTE_MAX_ATTEMPTS (1) ///< The Maximum amount of attempts to send a message.
#endif // LL_BH_LTE_M_EN


#endif // LL_BH_USE_CONFIG
#endif // LL_BACKHAUL_CONFIG_H
