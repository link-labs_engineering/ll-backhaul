///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                  Thomas Steinholz  <thomas.steinholz@link-labs.com>,      March 2018        ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#include "ll-hal.h"
#include "ll-common.h"
#include "ll-lte_m-task.h"
#include "ll-backhaul-hal.h"
#include "ll-backhaul-config.h"
#include "ll-backhaul-hal-config.h"

#include <time.h>
#include <stdarg.h>

#include "nrf.h"
#include "nrf_drv_gpiote.h"
#include "nrf_delay.h"

#include "lte_ifc_hal.h"
#include "timer_hal.h"
#include "user_app.h"

#include "symble_debug.h"

#include "SEGGER_RTT.h"
#include "timer_hal.h"

//#include "lte_ifc_hal.h"
#include "app_error.h"
#include "app_uart.h"

// Configuration.
#if HW_REV == 0
#define GPS_SUPPORT
#endif // HW_REV == 0

#ifdef GPS_SUPPORT
#include "gps.h"
#endif // GPS_SUPPORT




static uint8_t s_last_backhaul = 0x0; // Keeps track of the last used backhaul.
static bool s_uart_active = false;

//void gps_set_reset_pin(bool status);
//void gps_set_on_off_pin(bool status);
//void gps_set_enable_pin(bool status);
//void gps_read_wakeup_pin(bool *status);

//------------------------------  LTE-M User Defined Configurations  ----------------------------//

#if LL_BH_LTE_M_EN
// TODO
#endif // LL_BH_LTE_M_EN



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                           PUBLIC PROTOTYPES FOR USER DEFINED HAL LAYER                        //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//------------------------------------  UART COMMUNICATION  -------------------------------------//
void uart_evt_handler(app_uart_evt_t *p_event)
{
    s_uart_active = false;
    if (p_event->evt_type == APP_UART_COMMUNICATION_ERROR)
    {
        //APP_ERROR_HANDLER(p_event->data.error_communication);
        // FIXME: This is happening too much, but not when we are actually using the UART and it
        // does not seem to affect overall operation.
        uint32_t error = (uint32_t) p_event->data.error_communication;
        if (error & UART_ERRORSRC_OVERRUN_Msk)
        {
            LL_LOGF("HAL: Received APP UART COMMUNICATION ERROR: OVERRUN ERROR");
        }
        if (error & UART_ERRORSRC_PARITY_Msk)
        {
            LL_LOGF("HAL: Received APP UART COMMUNICATION ERROR: PARITY ERROR");
        }
        if (error & UART_ERRORSRC_FRAMING_Msk)
        {
            LL_LOGF("HAL: Received APP UART COMMUNICATION ERROR: FRAMING ERROR");
        }
        if (error & UART_ERRORSRC_BREAK_Msk)
        {
            LL_LOGF("HAL: Received APP UART COMMUNICATION ERROR: BREAK CONDITION");
        }
    }
    else if (p_event->evt_type == APP_UART_FIFO_ERROR)
    {
        //APP_ERROR_HANDLER(p_event->data.error_code);
    }

    //char *event = (p_event->evt_type == APP_UART_TX_EMPTY ? ("TXed all Data!") : ("RXed Data"));
    //LL_DEBUG_LOG("UART EVENT %s", event);
}

void _on_lte_irq_notification(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    nrf_drv_gpiote_in_event_disable(HW_LTE_HOST_NOTIFY_PIN);
    lte_set_irq_high();
}

void set_up_gpio(uint8_t backhaul)
{
    if (backhaul & LL_SYM_BACKHAUL)
    {
        // TODO
    }
    else if (backhaul & LL_LTE_M_BACKHAUL)
    {
        // Turn on the LTE-M Device.
#if HW_REV == 1
//        nrf_gpio_cfg_output(HW_LTE_POWER_EN_PIN);
//        nrf_gpio_pin_set(HW_LTE_POWER_EN_PIN);
#endif // HW_REV == 1
        // Configure the HW_LTE_HOST_NOTIFY_PIN line as a low-to-high transition interrupt.
        nrf_drv_gpiote_in_config_t irq_config = GPIOTE_CONFIG_IN_SENSE_LOTOHI(true);
        irq_config.pull = NRF_GPIO_PIN_PULLDOWN;
        static bool init = false;
        if (nrf_drv_gpiote_is_init() && !init)
        {
            uint32_t err_code = nrf_drv_gpiote_in_init(HW_LTE_HOST_NOTIFY_PIN, &irq_config, _on_lte_irq_notification);
            APP_ERROR_CHECK(err_code);
        }
        nrf_drv_gpiote_in_event_enable(HW_LTE_HOST_NOTIFY_PIN, true);

        // Configure the HW_LTE_WAKE_REQUEST_LINE as an output (high).
        nrf_gpio_cfg_output(HW_LTE_WAKE_REQUEST_PIN);
        nrf_gpio_pin_set(HW_LTE_WAKE_REQUEST_PIN);

        // Configure the HW_LTE_WAKE_STATUS_PIN as an input with no PULL UP/DOWN.
        nrf_gpio_cfg_input(HW_LTE_WAKE_STATUS_PIN, NRF_GPIO_PIN_NOPULL);

        // Configure the HW_LTE_BOOT_PIN as an output (low).
        nrf_gpio_cfg_output(HW_LTE_BOOT_PIN);
        nrf_gpio_pin_clear(HW_LTE_BOOT_PIN);

        // Configure the HW_LTE_RESET_PIN as an output (low).
        nrf_gpio_cfg_output(HW_LTE_RESET_PIN);
        nrf_gpio_pin_clear(HW_LTE_RESET_PIN);

        init = true;
    }
#ifdef GPS_SUPPORT
    else if (backhaul != 0)
    {
        // Configure HW_GPS_WAKEUP_PIN as an input with a pull up.
        //nrf_gpio_cfg_input(HW_GPS_WAKEUP_PIN, NRF_GPIO_PIN_PULLUP);

        // Configure HW_GPS_ON_OFF_PIN as an output (low).
        //nrf_gpio_cfg_output(HW_GPS_ON_OFF_PIN);
        //nrf_gpio_pin_clear(HW_GPS_ON_OFF_PIN);

        // Configure HW_GPS_EN_PIN as an output (low).
        //nrf_gpio_cfg_output(HW_GPS_EN_PIN);
        //nrf_gpio_pin_clear(HW_GPS_EN_PIN);

        // Configure HW_GPS_RESET_PIN as an output (low).
        //nrf_gpio_cfg_output(HW_GPS_RESET_PIN);
        //nrf_gpio_pin_clear(HW_GPS_RESET_PIN);
    }
#endif // GPS_SUPPORT
}

void disable_gpio(uint8_t backhaul)
{
    if (backhaul & LL_SYM_BACKHAUL)
    {
        nrf_gpio_cfg_default(HW_LTE_OR_SYM_TXOUT_PIN);
        nrf_gpio_cfg_default(HW_LTE_OR_SYM_RXIN_PIN);
//#if HW_REV == 0
        nrf_gpio_cfg_default(HW_LTE_RTS_PIN);         // SYM RESET - YELLOW
        nrf_gpio_cfg_default(HW_LTE_CTS_PIN);         // SYM BOOT  - ORANGE
        nrf_gpio_cfg_default(HW_LTE_HOST_NOTIFY_PIN); // SYM IO0   - PURPLE
//#elif HW_REV == 1
#if HW_REV == 1
        nrf_gpio_cfg_default(HW_SYM_RESET_PIN);
        nrf_gpio_cfg_default(HW_SYM_BOOT_PIN);
        nrf_gpio_cfg_default(HW_SYM_IO0_PIN);
#endif // HW_REV
    }
    else if (backhaul & LL_LTE_M_BACKHAUL)
    {
        nrf_gpio_cfg_input(HW_LTE_OR_SYM_TXOUT_PIN, NRF_GPIO_PIN_PULLDOWN);
        nrf_gpio_cfg_input(HW_LTE_OR_SYM_RXIN_PIN, NRF_GPIO_PIN_PULLUP);
        nrf_gpio_cfg_input(HW_LTE_CTS_PIN, NRF_GPIO_PIN_PULLDOWN);
        nrf_gpio_cfg_input(HW_LTE_RTS_PIN, NRF_GPIO_PIN_PULLDOWN);
#if HW_REV == 1
//        nrf_gpio_cfg_output(HW_LTE_POWER_EN_PIN);
//        nrf_gpio_pin_clear(HW_LTE_POWER_EN_PIN);
#endif // HW_REV == 1

        nrf_gpio_pin_set(HW_LTE_WAKE_REQUEST_PIN);
#if HW_REV == 1
//        nrf_gpio_cfg_default(HW_LTE_POWER_EN_PIN);
#endif
    }
#ifdef GPS_SUPPORT
    else if (backhaul != 0)
    {
        //nrf_gpio_cfg_default(HW_GPS_WAKEUP_PIN);
        //nrf_gpio_cfg_default(HW_GPS_ON_OFF_PIN);
        //nrf_gpio_cfg_default(HW_GPS_TX_PIN);
        //nrf_gpio_cfg_default(HW_GPS_RX_PIN);
        //nrf_gpio_cfg_default(HW_GPS_RESET_PIN);
        //nrf_gpio_pin_clear(HW_GPS_EN_PIN);
    }
#endif // GPS_SUPPORT
}

void set_up_uart(uint8_t backhaul)
{
    uint32_t ret;
    app_uart_comm_params_t comm_params;

    // Reinitialize the correct UART configuration.
    if (s_last_backhaul != backhaul)
    {
        // DEINIT the UART.
        if (s_last_backhaul & LL_SYM_BACKHAUL)
        {
            LL_LOGF("HAL: Disable Symphony Link UART");

            ret = app_uart_close();

            //nRF52840 UART workaround
            *(volatile uint32_t *)0x40002FFC = 0;
            *(volatile uint32_t *)0x40002FFC;
            *(volatile uint32_t *)0x40002FFC = 1;

            //LL_ASSERT(ret);
        }
        else if (s_last_backhaul & LL_LTE_M_BACKHAUL)
        {
            LL_LOGF("HAL: Disable LTE-M UART");

            ret = app_uart_close();

            //nRF52840 UART workaround
            *(volatile uint32_t *)0x40002FFC = 0;
            *(volatile uint32_t *)0x40002FFC;
            *(volatile uint32_t *)0x40002FFC = 1;

            //LL_ASSERT(ret);
        }
#ifdef GPS_SUPPORT
        else if (s_last_backhaul != 0)
        {
            LL_LOGF("HAL: Disable GPS UART");
            gps_uart_deinit();
        }
#endif // GPS_SUPPORT

        disable_gpio(s_last_backhaul);
        s_last_backhaul = backhaul;
        set_up_gpio(backhaul);

        if (backhaul & LL_SYM_BACKHAUL) // SYM UART
        {
            LL_LOGF("HAL: Enable Symphony Link UART");

            comm_params.rx_pin_no    = HW_LTE_OR_SYM_TXOUT_PIN;
            comm_params.tx_pin_no    = HW_LTE_OR_SYM_RXIN_PIN;
            comm_params.flow_control = APP_UART_FLOW_CONTROL_DISABLED;
            comm_params.use_parity   = false;
            comm_params.baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200;
        }
        else if (backhaul & LL_LTE_M_BACKHAUL) // LTE-M UART
        {
             LL_LOGF("HAL: Enable LTE-M UART");

            comm_params.rx_pin_no    = HW_LTE_OR_SYM_TXOUT_PIN;
            comm_params.tx_pin_no    = HW_LTE_OR_SYM_RXIN_PIN;
            comm_params.rts_pin_no   = HW_LTE_CTS_PIN;
            comm_params.cts_pin_no   = HW_LTE_RTS_PIN;
            comm_params.flow_control = APP_UART_FLOW_CONTROL_ENABLED;
            comm_params.use_parity   = false;
            comm_params.baud_rate    = UART_BAUDRATE_BAUDRATE_Baud115200;
        }
#ifdef GPS_SUPPORT
        else if (backhaul != 0x0) // GPS UART
        {
            // Enable Pins
            LL_LOGF("HAL: Enable GPS UART");
            gps_uart_init();
//            gps_return_val_t gps_ret;
//
//            gps_hal_callbacks_t callbacks;
//            callbacks.set_reset_pin   = gps_set_reset_pin;
//            callbacks.set_on_off_pin  = gps_set_on_off_pin;
//            callbacks.read_wakeup_pin = gps_read_wakeup_pin;
//            callbacks.set_enable_pin  = gps_set_enable_pin;
//
//            gps_ret = init_gps(&callbacks);
//            LL_ASSERT(gps_ret == GPS_INFO_NO_ERROR);
            return; // Don't init app fifo uart.
        }
#endif // GPS_SUPPORT
        else
        {
            return; // Don't init app fifo uart.
        }

        // Safe method for initializing nrf UART.
        APP_UART_FIFO_INIT(&comm_params,
                           UART_RX_BUF_SIZE,
                           UART_TX_BUF_SIZE,
                           uart_evt_handler,
                           APP_IRQ_PRIORITY_LOWEST,
                           ret);

        APP_ERROR_CHECK(ret);
    }
}

void ll_bh_disable_uart(void)
{
    set_up_uart(0x0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// A UART Wrapper for Transmitting Messages to the desired backhaul.
///
/// param[in] backhaul Specifies the backhaul that the driver want's to communicate with using
//                      BitFlags defined in ll-backhaul.h.
/// param[in] buff The data buffer to transmit.
/// param[in] len The length of the data in the buffer to transmit.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_uart_write(uint8_t backhaul, uint8_t *buff, size_t len)
{
    set_up_uart(backhaul);

    //LL_LOGF("HAL: Start UART Write %i bytes", len);
    //LL_LOGF("HAL: Buffer:");
    //for (uint16_t i = 0; i < len; ++i)
    //{
        //LL_LOGF("HAL: %i: %x", i, buff[i]);
    //}

    if (!buff || len == 0)
    {
        return 0;
    }

    // Symphony Link needs Blocking UART
    if (backhaul & LL_SYM_BACKHAUL)
    {
        while (s_uart_active);
        s_uart_active = true;
    }

    // Tx out queued bytes.
    for (uint16_t i = 0; i < len; ++i)
    {
        uint32_t err = app_uart_put(buff[i]);
        //LL_LOGF("HAL: Writing %i", buff[i]);

        if (err != NRF_SUCCESS)
        {
            //LL_LOGF("HAL: UART Write Err: %ld\n", err);
            return -1; // Write fail
        }
    }

    // Symphony Link needs Blocking UART.
    if (backhaul & LL_SYM_BACKHAUL)
    {
        while (s_uart_active);
    }

    // Message transport complete.
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// A UART Wrapper for Receiving Messages to the desired backhaul.
///
/// param[in] backhaul Specifies the backhaul that the driver want's to communicate with using
//                      BitFlags defined in ll-backhaul.h.
/// param[in] buff The data buffer to receive.
/// param[in] len The length of the data in the buffer to transmit.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_uart_read(uint8_t backhaul, uint8_t *buff, size_t len)
{
    set_up_uart(backhaul);

    //LL_LOGF("HAL: Start UART Read");
// TODO: use length.
    uint32_t _uart_ret = app_uart_get(buff);

    if (_uart_ret == NRF_ERROR_NOT_FOUND)
    {
        //NRF_LOG_DEBUG("UART: Nothing to read\n");
        return -1; // Nothing to read/failed.
    }
    // Else if
    if(_uart_ret != NRF_SUCCESS)
    {
        //LL_LOGF("HAL: UART Read Err: %ld\n", _uart_ret);
        return _uart_ret;
    }

    //LL_LOGF("HAL: UART: read %c \n", *buff);

    return _uart_ret;
}



//-----------------------------  NON-RTOS REQUIRED HAL  -----------------------------------------//

#if (!LL_SYM_USE_RTOS || !LL_LTE_USE_RTOS)
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Sleep for a few milliseconds.
///
/// This comes predefined for RTOS!
///
/// param[in] millis The amount of milliseconds to sleep for.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_sleep_ms(int32_t millis)
{
    nrf_delay_ms(millis);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Populate the tp struct with the system time of the host.
///
/// This comes predefined for RTOS!
///
/// param[out] tp Time struct.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_gettime(struct time *tp)
{
    uint32_t time_ms = get_time_ms();
    uint32_t time_s;
    uint32_t time_remainder_ns;

    // sys_ms -> whole sec.
    time_s = (uint32_t) (time_ms / 1000.0);

    // nS between secs.
    time_remainder_ns = (time_ms - (time_s * 1000)) * (uint32_t) (1e6);

    tp->tv_sec = time_s;
    tp->tv_nsec = time_remainder_ns;

    // LL_LOGF("Time\nS:%ld\nnS:%ld\n\n", tp->tv_sec, tp->tv_nsec);

    return 0;
}
#endif // (!LL_SYM_USE_RTOS && !LL_LTE_USE_RTOS)


//-----------------------------------  GENERIC CALLBACKS  ---------------------------------------//

///////////////////////////////////////////////////////////////////////////////////////////////////
/// This function is called when a message is received by any Link Labs Backhaul.
///
/// param[in] backhaul Bit Flag that identifies which backhaul received the message.
/// param[in] buff The buffer containing the data from the message.
/// param[in] len The size of the buffer.
/// param[in] port The port the message was received from.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_message_rx(uint8_t backhaul, uint8_t *buff, size_t len, uint8_t port)
{

}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// User defined method of outputting log messages from the backhaul library.
///
/// Note: Log Function should handle printing new line characters as well as other formatting given
/// the log level of the message.
///
/// param[in] log_level The log level of the log message (Debug, Warning, etc).
/// param[in] fmt The va argument list of the formatted log string.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_log(uint8_t log_level, const char *fmt, ...)
{

}


//------------------------------  REQUIRED SYMPHONY LINK HAL ------------------------------------//

#if LL_BH_SYMPHONY_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Initialize the Symphony Module GPIO pins.
///
/// * IO Line    - GPIO input  [Rising Edge Trigger]
/// * RESET Line - GPIO output [Set Low]
///
/// returns LL_HAL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_init_sym_gpio(void)
{
    // PURPLE - HW_SYM_IO0_PIN
//    nrf_gpio_cfg_input(HW_LTE_HOST_NOTIFY_PIN, NRF_GPIO_PIN_NOPULL);

    // YELLOW - HW_SYM_RESET_PIN
//    nrf_gpio_cfg_output(HW_LTE_RTS_PIN);

    // ORANGE - HW_SYM_BOOT_PIN
//    nrf_gpio_cfg_output(HW_LTE_CTS_PIN);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Reset Pin.
///
/// The Reset pin is Active LOW.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_reset_set(ll_pin_state_t state)
{
    // YELLOW - HW_SYM_RESET_PIN
//    nrf_gpio_pin_write(HW_LTE_RTS_PIN, (uint32_t) state);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module IO Pin State.
///
/// The IO pin is HIGH when an IRQ event is triggered.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_sym_io_get(void)
{
    // PURPLE - HW_SYM_IO0_PIN
//    return nrf_gpio_pin_out_read(HW_LTE_HOST_NOTIFY_PIN) ? LL_PIN_HIGH : LL_PIN_LOW;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Power.
///
/// The Power Enable pin is Active HIGH.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_power_set(ll_pin_state_t state)
{
    // N/A
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Boot Line.
///
/// The Boot pin is Active LOW.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_boot_set(ll_pin_state_t state)
{
    // ORANGE - HW_SYM_BOOT_PIN
//    nrf_gpio_pin_write(HW_LTE_CTS_PIN, (uint32_t) state);
}


//--------------------  OPTIONAL SYMPHONY STATE MACHINE EVENT HANDLERS  -------------------------//

#if LL_SYM_USE_RTOS
///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine is about to go to sleep
/// to save power. This would be a good place to deactivate any other peripherals to save power.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_suspend_evnt(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine is about to resume power.
/// This is where all deactivated peripherals should be reactivated.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_resume_evnt(void);
#endif // LL_SYM_USE_RTOS


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine changes state.
///
/// param[in] new_state The new state that the symphony state machine is transitioning to.
/// param[in] old_state The old state that the symphony state machine is transitioning from.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_state_change_evnt(/*sym_module_state_t new_state, sym_module_state_t old_state*/)
{

}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine processes an error.
///
/// param[in] err The error the state machine encountered.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_error_evnt(/*sym_error_code_t err*/)
{

}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine changes state.
///
/// param[in] new_state The new state that the symphony state machine is transitioning to.
/// param[in] old_state The old state that the symphony state machine is transitioning from.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_tx_done(bool success)
{

}


//---------------------------  SYMPHONY LINK FOTA CALLBACKS  ------------------------------------//

#if LL_SYM_FOTA_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to open and allocate storage when a new file transfer begins.
/// This is called at the beginning of the file transfer.
///
/// @param[in] file_id ID of the incoming file
/// @param[in] file_version Version of the incoming file
/// @param[in] file_size Size (in bytes) of the incoming file
///
/// @return Return code to indicate success/error of operation
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_open_t(uint32_t file_id, uint32_t file_version,
        uint32_t file_size);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to read a chunk of file in memory
///
/// @param[in] file_id ID of the file
/// @param[in] file_version Version of the file
/// @param[in] offset Offset to begin reading file from
/// @param[out] payload Buffer to read requested bytes into
/// @param[in] len Number of bytes to read
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_read_t(uint32_t file_id, uint32_t file_version,
        uint32_t offset, uint8_t *payload, uint16_t len);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to write a chunk of file to memory
///
/// @param[in] file_id ID of the file
/// @param[in] file_version Version of the file
/// @param[in] offset Offset to begin writing file at
/// @param[in] payload Buffer with data to be written
/// @param[in] len Number of bytes to write
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_write_t(uint32_t file_id, uint32_t file_version,
        uint32_t offset, uint8_t *payload, uint16_t len);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when memory reading/writing is complete. This is called once all
/// segments have been received, when a file transfer is interrupted, or a file transfer is
/// canceled.
///
/// param[in] file_id The 32-bit unique ID of the file.
/// param[in] file_version The 32-bit unique file version.
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_close(uint32_t file_id, uint32_t file_version);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when FTP is complete.
///
/// @param[in] success 'true' if transfer was successful, 'false' otherwise
/// @param[in] file_id ID of the transferred file
/// @param[in] file_version Version of the transferred file
/// @param[in] file_size Size (in bytes) of the transferred file
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_apply(uint32_t file_id, uint32_t file_version,
       uint32_t file_size);
#endif // LL_SYM_FOTA_EN
#endif // LL_BH_SYMPHONY_EN


//---------------------------------  REQUIRED LTE-M HAL -----------------------------------------//

#if LL_BH_LTE_M_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Initialize the LTE-M GPIO pins.
///
/// * IO Line           - GPIO input  [Rising Edge Trigger]
/// * WAKE STATUS Line  - GPIO input  [Rising Edge Trigger]
/// * WAKE REQUEST Line - GPIO output [Set Low]
///
/// returns LL_HAL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_init_lte_gpio(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module IO Pin State.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_lte_io_pin_get(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module IO Pin State.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_lte_wake_status_pin_get(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Reset Pin.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_lte_wake_request_pin_set(ll_pin_state_t state);


void ll_bh_lte_reset_pin_set(ll_pin_state_t state)
{
    nrf_gpio_pin_write(HW_LTE_RESET_PIN, (int)state);
}


// TODO: Add Other LTE-M Pin HAL functions.

#endif // LL_BH_LTE_M_EN

//----------------------------------------  GPS HAL ---------------------------------------------//

//void gps_set_reset_pin(bool status)
//{
//    nrf_gpio_pin_write(HW_GPS_RESET_PIN, status);
//}
//
//void gps_set_on_off_pin(bool status)
//{
//    nrf_gpio_pin_write(HW_GPS_ON_OFF_PIN, status);
//}
//
//void gps_set_enable_pin(bool status)
//{
//    nrf_gpio_pin_write(HW_GPS_EN_PIN, status);
//}
//
//void gps_read_wakeup_pin(bool *status)
//{
//    *status = nrf_gpio_pin_read(HW_GPS_WAKEUP_PIN);
//}

int32_t lte_ifc_hal_get_irq_flags_gpio(bool *p_level)
{
    if (p_level)
    {
        *p_level = nrf_gpio_pin_read(HW_LTE_HOST_NOTIFY_PIN);
        return 0;
    }
    return -1;
}

// Wake status goes HIGH to indicate IC is hot.
int32_t lte_ifc_hal_get_wake_status(bool *p_wake)
{
    uint32_t input;

    input = nrf_gpio_pin_read(HW_LTE_WAKE_STATUS_PIN);

    if (input > 0)
    {
        *p_wake = true;
        // NRF_LOG_DEBUG("Module awake\n");
    }
    else
        *p_wake = false;

    return 0;
}

// Wake request active low.
int32_t lte_ifc_hal_set_wake_request(bool wake)
{
    if (wake)
    {
        // NRF_LOG_INFO("SET WAKE REQUEST PIN LOW");
        nrf_gpio_pin_clear(HW_LTE_WAKE_REQUEST_PIN); // Clear to low.
    }
    else
    {
        // NRF_LOG_INFO("SET WAKE REQUEST PIN HIGH");
        nrf_gpio_pin_set(HW_LTE_WAKE_REQUEST_PIN); // Set high.
    }

    return 0;
}

int32_t lte_ifc_hal_gettime(struct lte_ifc_time *tp)
{
    uint32_t time_ms = get_time_ms();
    uint32_t time_s;
    uint32_t time_remainder_ns;

    // sys_ms -> whole sec.
    time_s = (uint32_t)(time_ms / 1000.0);

    // nS between secs.
    time_remainder_ns = (time_ms - (time_s * 1000)) * (uint32_t)(1e6);

    tp->tv_sec = time_s;
    tp->tv_nsec = time_remainder_ns;

    // NRF_LOG_INFO("Time\nS:%ld\nnS:%ld\n\n", tp->tv_sec, tp->tv_nsec);

    return 0;
}

// NRF UART Read. Operated in non blocking mode.
int32_t lte_ifc_hal_read_byte(uint8_t *buff)
{
    uint32_t _uart_ret = ll_bh_hal_uart_read(LL_LTE_M_BACKHAUL, buff, 1);

    if (_uart_ret == NRF_ERROR_NOT_FOUND)
    {
        // NRF_LOG_DEBUG("UART: Nothing to read\n");
        return -1; // Nothing to read/failed.
    }
    // Else if
    if (_uart_ret != NRF_SUCCESS)
    {
        // LL_LOGF("UART Read Err: %ld\n", _uart_ret);
        return _uart_ret;
    }

    // NRF_LOG_INFO("UART: read %c \n", *buff);

    return _uart_ret;
}

/*
 * Put bytes message to UART. Continue for length of array.
   As noted in the HAL header, buffer must be copied locally.
   It is not guaranteed to be held by lte_ifc for output
   for use in non blocking mode.
 */
int32_t lte_ifc_hal_write(uint8_t *buff, uint16_t len)
{
    uint32_t err = ll_bh_hal_uart_write(LL_LTE_M_BACKHAUL, buff, len);

    if (err != NRF_SUCCESS)
    {
        // LL_LOGF("UART Write Err: %ld\n", err);
        return -1; // Write fail
    }

    // Message transport complete.
    return 0;
}