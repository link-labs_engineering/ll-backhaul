///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                     Thomas Steinholz  <thomas.steinholz@link-labs.com>, FEB 2018            ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef LL_BACKHAUL_PC_HAL_H
#define LL_BACKHAUL_PC_HAL_H

#include "ll-hal.h"
#include <stdio.h>
#include <assert.h>

#define LL_TTY_DEFAULT_BAUDRATE 115200

#ifdef __APPLE__
/* On OS X, Opening /dev/cu* doesn't require DCD to be asserted and succeeds immediately. */
#define LL_TTY_DEFAULT_DEVICE "/dev/cu.SLAB_USBtoUART"
#else
#if defined(WIN32) || defined (__MINGW32__)
#define LL_TTY_DEFAULT_DEVICE "\\\\.\\COM3"
#else
#define LL_TTY_DEFAULT_DEVICE "/dev/ttyUSB0"
#endif
#endif

#define UNIT_TESTS

#ifdef UNIT_TESTS
#include "unity.h"
#ifdef DEBUG_PRINT
#define LL_LOGF(...)                                                          \
{                                                                             \
    printf(__VA_ARGS__);                                                      \
    UNITY_PRINT_EOL();                                                        \
    UNITY_FLUSH_CALL();                                                       \
}
#else
#define LL_LOGF(...)
#endif // DEBUG_PRINT
#else
#define LL_LOGF(...) printf(__VA_ARGS__)
#endif // UNIT_TESTS

#define LL_ASSERT(x) assert(x)

/**
 * @brief Open the serial port to the LinkLabs module.
 *
 * @param dev_name The device name string.  NULL uses the default.
 * @param baudrate The desired baud rate.  0 uses the default and is
 *      recommended for normal use.
 * @return 0 or error code.
 */
int ll_tty_open(const char * dev_name, int baudrate, uint8_t backhaul);

/**
 * @brief Close the serial port to the LinkLabs module.
 *
 * @return 0 or error code.
 */
int ll_tty_close(void);

#endif // LL_BACKHAUL_PC_HAL_H
