///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                     Thomas Steinholz  <thomas.steinholz@link-labs.com>, FEB 2018            ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                            INCLUDES                                           //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

#include "ll-backhaul-config.h"
#include "ll-hal.h"
#include "ll-backhaul-hal.h"
#include "ll_ifc.h"
#include "ll_ifc_utils.h"

#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#if defined(WIN32) || defined(__MINGW32__)
#include <windows.h>
#warning "LTEM Driver NOT supported on WINDOWS!"
#warning "Need to setup flow control"
#elif defined(__APPLE__) || defined(__linux__) || defined(__CYGWIN__)
#include <sys/time.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#endif



// Debug switch - printf's every byte Tx'd and Rx'd (to stdout)
#undef DEBUG_PRINT_EVERY_BYTE_TX_RX

#if defined(WIN32) || defined (__MINGW32__)
static HANDLE g_tty_fd_lte;
static HANDLE g_tty_fd_sym;
#else
static int g_tty_fd_lte = -1;
static int g_tty_fd_sym = -1;
#endif

static int ll_tty_flush(void);


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                           PUBLIC PROTOTYPES FOR USER DEFINED HAL LAYER                        //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//------------------------------------  UART COMMUNICATION  -------------------------------------//

int ll_tty_open(const char * dev_name, int baudrate, uint8_t backhaul)
{
#if defined(WIN32) || defined (__MINGW32__)
    DCB dcbSerialParams = {0};
    COMMTIMEOUTS timeouts = {0};

    if (dev_name == NULL)
    {
        dev_name = LL_TTY_DEFAULT_DEVICE;
    }
    printf("Open %s baud %d\n", dev_name, baudrate);
    if (backhaul == LL_LTE_M_BACKHAUL)
    {
        g_tty_fd_lte = CreateFile(dev_name, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if(g_tty_fd_lte == INVALID_HANDLE_VALUE)
        {
            fprintf(stderr, "Unable to open %s\n", dev_name);
            exit(EXIT_FAILURE);
        }

        /**
         * Set device parameters (115200 baud, 1 start bit,
         * 1 stop bit, no parity)
         */
        dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
        if (GetCommState(g_tty_fd_lte, &dcbSerialParams) == 0)
        {
            fprintf(stderr, "Error getting device state\n");
            CloseHandle(g_tty_fd_lte);
            exit(EXIT_FAILURE);
        }

        dcbSerialParams.BaudRate = baudrate;
        dcbSerialParams.ByteSize = 8;
        dcbSerialParams.StopBits = ONESTOPBIT;
        dcbSerialParams.Parity = NOPARITY;
        if(SetCommState(g_tty_fd_lte, &dcbSerialParams) == 0)
        {
            fprintf(stderr, "Error setting device parameters\n");
            CloseHandle(g_tty_fd_lte);
            exit(EXIT_FAILURE);
        }

        /* Set COM port timeout settings */
        timeouts.ReadIntervalTimeout = 50;          /* milliseconds */
        timeouts.ReadTotalTimeoutConstant = 50;     /* milliseconds */
        timeouts.ReadTotalTimeoutMultiplier = 10;   /* milliseconds */
        timeouts.WriteTotalTimeoutConstant = 50;
        timeouts.WriteTotalTimeoutMultiplier = 10;
        if(SetCommTimeouts(g_tty_fd_lte, &timeouts) == 0)
        {
            fprintf(stderr, "Error setting timeouts\n");
            CloseHandle(g_tty_fd_lte);
            return 1;
        }
    }
    else
    {
        g_tty_fd_sym = CreateFile(dev_name, GENERIC_READ|GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if(g_tty_fd_sym == INVALID_HANDLE_VALUE)
        {
            fprintf(stderr, "Unable to open %s\n", dev_name);
            exit(EXIT_FAILURE);
        }

        /**
         * Set device parameters (115200 baud, 1 start bit,
         * 1 stop bit, no parity)
         */
        dcbSerialParams.DCBlength = sizeof(dcbSerialParams);
        if (GetCommState(g_tty_fd_sym, &dcbSerialParams) == 0)
        {
            fprintf(stderr, "Error getting device state\n");
            CloseHandle(g_tty_fd_sym);
            exit(EXIT_FAILURE);
        }

        dcbSerialParams.BaudRate = baudrate;
        dcbSerialParams.ByteSize = 8;
        dcbSerialParams.StopBits = ONESTOPBIT;
        dcbSerialParams.Parity = NOPARITY;
        if(SetCommState(g_tty_fd_sym, &dcbSerialParams) == 0)
        {
            fprintf(stderr, "Error setting device parameters\n");
            CloseHandle(g_tty_fd_sym);
            exit(EXIT_FAILURE);
        }

        /* Set COM port timeout settings */
        timeouts.ReadIntervalTimeout = 50;          /* milliseconds */
        timeouts.ReadTotalTimeoutConstant = 50;     /* milliseconds */
        timeouts.ReadTotalTimeoutMultiplier = 10;   /* milliseconds */
        timeouts.WriteTotalTimeoutConstant = 50;
        timeouts.WriteTotalTimeoutMultiplier = 10;
        if(SetCommTimeouts(g_tty_fd_sym, &timeouts) == 0)
        {
            fprintf(stderr, "Error setting timeouts\n");
            CloseHandle(g_tty_fd_sym);
            return 1;
        }
    }

    return 0;
#elif __APPLE__
    struct termios term_io_settings;

    if (backhaul == LL_LTE_M_BACKHAUL)
    {
        if (dev_name == NULL)
        {
            printf("Open %s baud %d\n", LL_TTY_DEFAULT_DEVICE, baudrate);
            g_tty_fd_lte = open(LL_TTY_DEFAULT_DEVICE, O_RDWR);
        }
        else
        {
            printf("Open %s baud %d\n", dev_name, baudrate);
            g_tty_fd_lte = open(dev_name, O_RDWR);
        }
        if(g_tty_fd_lte == -1)
        {
            fprintf(stderr, "Unable to open %s\n", dev_name);
            exit(EXIT_FAILURE);
        }

        memset(&term_io_settings, 0, sizeof(struct termios));
        cfmakeraw(&term_io_settings);
        cfsetspeed(&term_io_settings, baudrate);

        term_io_settings.c_cflag = CREAD | CLOCAL;  /* turn on READ */
        term_io_settings.c_cflag |= CS8;
        term_io_settings.c_cc[VMIN] = 0;
        term_io_settings.c_cc[VTIME] = 10;          /* 1 sec timeout */
        ioctl(g_tty_fd_lte, TIOCSETA, &term_io_settings);

        /* Flush read buffer */
        int i = 0;
        while (1)
        {
            int i32_ret;
            uint8_t ret_byte;

            i32_ret = read(g_tty_fd_lte, &ret_byte, 1);
            if (i32_ret <= 0)
            {
                break;
            }
            else if (i32_ret > 0)
            {
                i++;
            }
        }
        printf("Flushed %d bytes\n", i);
    }
    else
    {
        if (dev_name == NULL)
        {
            printf("Open %s baud %d\n", LL_TTY_DEFAULT_DEVICE, baudrate);
            g_tty_fd_sym = open(LL_TTY_DEFAULT_DEVICE, O_RDWR);
        }
        else
        {
            printf("Open %s baud %d\n", dev_name, baudrate);
            g_tty_fd_sym = open(dev_name, O_RDWR);
        }
        if(g_tty_fd_sym == -1)
        {
            fprintf(stderr, "Unable to open %s\n", dev_name);
            exit(EXIT_FAILURE);
        }

        memset(&term_io_settings, 0, sizeof(struct termios));
        cfmakeraw(&term_io_settings);
        cfsetspeed(&term_io_settings, baudrate);

        term_io_settings.c_cflag = CREAD | CLOCAL;  /* turn on READ */
        term_io_settings.c_cflag |= CS8;
        term_io_settings.c_cc[VMIN] = 0;
        term_io_settings.c_cc[VTIME] = 10;          /* 1 sec timeout */
        ioctl(g_tty_fd_sym, TIOCSETA, &term_io_settings);

        /* Flush read buffer */
        int i = 0;
        while (1)
        {
            int i32_ret;
            uint8_t ret_byte;

            i32_ret = read(g_tty_fd_sym, &ret_byte, 1);
            if (i32_ret <= 0)
            {
                break;
            }
            else if (i32_ret > 0)
            {
                i++;
            }
        }
        printf("Flushed %d bytes\n", i);

    }

    return 0;
#else
    if (baudrate == 0)
    {
        baudrate = LL_TTY_DEFAULT_BAUDRATE;
    }
    else if (baudrate != LL_TTY_DEFAULT_BAUDRATE)
    {
        return(-1);
    }
    if (dev_name == NULL)
    {
        dev_name = LL_TTY_DEFAULT_DEVICE;
    }


    if (backhaul == LL_LTE_M_BACKHAUL)
    {
        if (g_tty_fd_lte >= 0)
        {
            printf("ll_tty_open: already open.");
            ll_tty_close();
        }
        g_tty_fd_lte = open(dev_name, O_RDWR | O_NOCTTY | O_SYNC | O_NDELAY);
        if(g_tty_fd_lte < 0)
        {
            perror("ll_tty_open");
            return(-1);
        }
        printf("Open %s baud %d\n", dev_name, baudrate);

        /* Turn off blocking for reads, use (g_tty_fd, F_SETFL, FNDELAY) if you want that */
        struct termios tc;
        tcgetattr(g_tty_fd_lte, &tc);

        /* input flags */
        tc.c_iflag &= ~ IGNBRK;           /* enable ignoring break */
        tc.c_iflag &= ~(IGNPAR | PARMRK); /* disable parity checks */
        tc.c_iflag &= ~ INPCK;            /* disable parity checking */
        tc.c_iflag &= ~ ISTRIP;           /* disable stripping 8th bit */
        tc.c_iflag &= ~(INLCR | ICRNL);   /* disable translating NL <-> CR */
        tc.c_iflag &= ~ IGNCR;            /* disable ignoring CR */
        tc.c_iflag &= ~(IXON | IXOFF);    /* disable XON/XOFF flow control */

        /* output flags */
        tc.c_oflag &= ~ OPOST;            /* disable output processing */
        tc.c_oflag &= ~(ONLCR | OCRNL);   /* disable translating NL <-> CR */

        /* not for FreeBSD */
        tc.c_oflag &= ~ OFILL;            /* disable fill characters */

        /* control flags */
        tc.c_cflag |= CLOCAL;             /* prevent changing ownership */
        tc.c_cflag |= CREAD;              /* enable reciever */
        tc.c_cflag &= ~ PARENB;           /* disable parity */
        tc.c_cflag &= ~ CSTOPB;           /* disable 2 stop bits */
        tc.c_cflag &= ~ CSIZE;            /* remove size flag... */
        tc.c_cflag |= CS8;                /* ...enable 8 bit characters */
        tc.c_cflag |= HUPCL;              /* enable lower control lines on close - hang up */
        tc.c_cflag &= CRTSCTS;            /* enable hardware CTS/RTS flow control */

        /* local flags */
        tc.c_lflag &= ~ ISIG;             /* disable generating signals */
        tc.c_lflag &= ~ ICANON;           /* disable canonical mode - line by line */
        tc.c_lflag &= ~ ECHO;             /* disable echoing characters */
        tc.c_lflag &= ~ ECHONL;           /* ??? */
        tc.c_lflag &= ~ NOFLSH;           /* disable flushing on SIGINT */
        tc.c_lflag &= ~ IEXTEN;           /* disable input processing */

        /* control characters */
        memset(tc.c_cc,0,sizeof(tc.c_cc));

        /* set i/o baud rate */
        cfsetspeed(&tc, B115200);
        tcsetattr(g_tty_fd_lte, TCSAFLUSH, &tc);

        /* enable input & output transmission */
        tcflow(g_tty_fd_lte, TCOON | TCION);

        // Set RTS line LOW. Device Asleep.
        int ioArgs = TIOCM_RTS;
        int ret = ioctl(g_tty_fd_lte, TIOCMBIC, &ioArgs);
        if (ret < 0)
        {
            fprintf(stderr, "Error(%zi) clearing RTS line: %s\n", ret, strerror(errno));
            return ret;
        }
    }
    else
    {
        if (g_tty_fd_sym >= 0)
        {
            printf("ll_tty_open: already open.");
            ll_tty_close();
        }

        g_tty_fd_sym = open(dev_name, O_RDWR | O_NOCTTY | O_SYNC | O_NDELAY);
        if(g_tty_fd_sym < 0)
        {
            perror("ll_tty_open");
            return(-1);
        }
        printf("Open %s baud %d\n", dev_name, baudrate);

        /* Turn off blocking for reads, use (g_tty_fd, F_SETFL, FNDELAY) if you want that */
        struct termios tc;
        tcgetattr(g_tty_fd_sym, &tc);

        /* input flags */
        tc.c_iflag &= ~ IGNBRK; /* enable ignoring break */
        tc.c_iflag &= ~(IGNPAR | PARMRK); /* disable parity checks */
        tc.c_iflag &= ~ INPCK; /* disable parity checking */
        tc.c_iflag &= ~ ISTRIP; /* disable stripping 8th bit */
        tc.c_iflag &= ~(INLCR | ICRNL); /* disable translating NL <-> CR */
        tc.c_iflag &= ~ IGNCR; /* disable ignoring CR */
        tc.c_iflag &= ~(IXON | IXOFF); /* disable XON/XOFF flow control */
        /* output flags */
        tc.c_oflag &= ~ OPOST; /* disable output processing */
        tc.c_oflag &= ~(ONLCR | OCRNL); /* disable translating NL <-> CR */
        /* not for FreeBSD */
        tc.c_oflag &= ~ OFILL; /* disable fill characters */
        /* control flags */
        tc.c_cflag |= CLOCAL; /* prevent changing ownership */
        tc.c_cflag |= CREAD; /* enable reciever */
        tc.c_cflag &= ~ PARENB; /* disable parity */
        tc.c_cflag &= ~ CSTOPB; /* disable 2 stop bits */
        tc.c_cflag &= ~ CSIZE; /* remove size flag... */
        tc.c_cflag |= CS8; /* ...enable 8 bit characters */
        tc.c_cflag |= HUPCL; /* enable lower control lines on close - hang up */
        tc.c_cflag &= ~ CRTSCTS; /* disable hardware CTS/RTS flow control */
        /* local flags */
        tc.c_lflag &= ~ ISIG; /* disable generating signals */
        tc.c_lflag &= ~ ICANON; /* disable canonical mode - line by line */
        tc.c_lflag &= ~ ECHO; /* disable echoing characters */
        tc.c_lflag &= ~ ECHONL; /* ??? */
        tc.c_lflag &= ~ NOFLSH; /* disable flushing on SIGINT */
        tc.c_lflag &= ~ IEXTEN; /* disable input processing */

        /* control characters */
        memset(tc.c_cc,0,sizeof(tc.c_cc));

        /* set i/o baud rate */
        cfsetspeed(&tc, B115200);
        tcsetattr(g_tty_fd_sym, TCSAFLUSH, &tc);

        /* enable input & output transmission */
        tcflow(g_tty_fd_sym, TCOON | TCION);
    }

	ll_tty_flush();
    return(0);
#endif // Arch.
}


int ll_tty_close()
{
#if defined(WIN32) || defined (__MINGW32__)
    CloseHandle(g_tty_fd_lte);
    CloseHandle(g_tty_fd_sym);
    return 0;
#else
    if (g_tty_fd_lte < 0)
    {
		close(g_tty_fd_lte);
		g_tty_fd_lte = -1;
    }
    if (g_tty_fd_sym < 0)
    {
		close(g_tty_fd_sym);
		g_tty_fd_sym = -1;
    }

	return 0;
#endif // Arch.
}

#if defined(__APPLE__) || defined(__linux__) || defined(__CYGWIN__)
static int ll_tty_flush() {
    while (g_tty_fd_lte >= 0)
    {
        ssize_t sst_ret;
        uint8_t ret_byte;

        sst_ret = read(g_tty_fd_lte, &ret_byte, 1);
        if (sst_ret == 0)
		{
			return 0;  // no more data.
		}
		else if (sst_ret < 0)
        {
			perror("Error flushing CF UART");
			return -1;
		}
    }
    while (g_tty_fd_sym >= 0)
    {
        ssize_t sst_ret;
        uint8_t ret_byte;

        sst_ret = read(g_tty_fd_sym, &ret_byte, 1);
        if (sst_ret == 0)
		{
			return 0;  // no more data.
		}
		else if (sst_ret < 0)
        {
			perror("Error flushing CF UART");
			return -1;
		}
    }

	return 0;
}


static ssize_t read_with_timeout(int fd, void *bf, size_t len, time_t sec)
{
    fd_set set;
    struct timeval timeout;
    ssize_t ret;

    FD_ZERO(&set); // clear the set
    FD_SET(fd, &set); // add our file descriptor to the set

    timeout.tv_sec = sec;
    timeout.tv_usec = 0;

    ret = select(fd + 1, &set, NULL, NULL, &timeout);
    if(ret == -1)
    {
        perror("select"); // an error accured
        return ret;
    }
    else if(ret == 0)
    {
        // fprintf(stderr, "read_with_timeout: Timeout %zu, %zu\n", sec, len);
        return -1;
    }
    else
    {
        return read(fd, bf, len); // there was data to read
    }
}
#endif // APPLE, LINUX, or CYGWIN.

void ll_bh_enable_uart(void) { }
void ll_bh_disable_uart(void) { }

///////////////////////////////////////////////////////////////////////////////////////////////////
/// A UART Wrapper for Transmitting Messages to the desired backhaul.
///
/// param[in] backhaul Specifies the backhaul that the driver want's to communicate with using
//                      BitFlags defined in ll-backhaul.h.
/// param[in] buff The data buffer to transmit.
/// param[in] len The length of the data in the buffer to transmit.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_uart_write(uint8_t backhaul, uint8_t *buff, size_t len)
{
#if defined(WIN32) || defined (__MINGW32__)
    DWORD bytes_written;
    if (backhaul & LL_LTE_M_BACKHAUL)
    {
        if (!WriteFile(g_tty_fd_lte, buff, len, &bytes_written, NULL))
        {
            fprintf(stderr, "Error writing tty\n");
            return -1;
        }
        if (bytes_written < len)
        {
            return -1;
        }
    }
    if (backhaul & LL_SYM_BACKHAUL)
    {
        if (!WriteFile(g_tty_fd_sym, buff, len, &bytes_written, NULL))
        {
            fprintf(stderr, "Error writing tty\n");
            return -1;
        }
        if (bytes_written < len)
        {
            return -1;
        }
    }

#ifdef DEBUG_PRINT_EVERY_BYTE_TX_RX
    int i;
    for (i = 0; i < bytes_written; i++)
    {
        printf("W: 0x%02x\n", buff[i]);
    }
#endif // DEBUG
    return 0;
#else // *NIX
    ssize_t ret;
    if (backhaul == LL_LTE_M_BACKHAUL)
    {
        int ioArgs = TIOCM_DTR;

        // Set DTR line HIGH.
        ret = ioctl(g_tty_fd_lte, TIOCMBIS, &ioArgs);
        if (ret < 0)
        {
            fprintf(stderr, "Error(%zi) setting DTR line: %s\n", ret, strerror(errno));
            return ret;
        }

        // Write to UART.
        ret = write(g_tty_fd_lte, buff, len);
        if (ret != len)
        {
            printf("WARNING: Did not write all bytes!\n");
        }

        // Set DTR line LOW.
        ret = ioctl(g_tty_fd_lte, TIOCMBIC, &ioArgs);
        if (ret < 0)
        {
            fprintf(stderr, "Error(%zi) clearing DTR line: %s\n", ret, strerror(errno));
            return ret;
        }
    }
    else
    {
        ret = write(g_tty_fd_sym, buff, len);
    }
#ifdef DEBUG_PRINT_EVERY_BYTE_TX_RX
    int i;
    for (i = 0; i < len; i++)
    {
        printf("W: 0x%02x\n", buff[i]);
    }
#endif // DEBUG
    return ret;
#endif // ARCH
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// A UART Wrapper for Receiving Messages to the desired backhaul.
///
/// param[in] backhaul Specifies the backhaul that the driver want's to communicate with using
//                      BitFlags defined in ll-backhaul.h.
/// param[in] buff The data buffer to receive.
/// param[in] len The length of the data in the buffer to transmit.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_uart_read(uint8_t backhaul, uint8_t *buff, size_t len)
{
#if defined(WIN32) || defined (__MINGW32__)
    DWORD bytes_read;

    if (!ReadFile(
        backhaul == LL_LTE_M_BACKHAUL ? g_tty_fd_lte : g_tty_fd_sym,
        buff, len, &bytes_read, NULL))
    {
        // fprintf(stderr, "Error reading tty\n");
        return -1;
    }
    if (bytes_read < len)
    {
        return -1;
    }

#ifdef DEBUG_PRINT_EVERY_BYTE_TX_RX
    int i;
    for (i = 0; i < bytes_read; i++)
    {
        printf("\tR: 0x%02x\n", buff[i]);
    }
#endif // DEBUG PRINT
    return 0;
#else
    ssize_t ret;
    uint16_t bytes_read = 0;

    do
    {
        // Read with a timeout of 3.0 seconds
        ret = read_with_timeout(
                backhaul == LL_LTE_M_BACKHAUL ? g_tty_fd_lte : g_tty_fd_sym,
                buff + bytes_read, len - bytes_read, 3);
        if (ret < 0)
        {
            // fprintf(stderr, "Error(%zi) reading tty: %s\n", ret, strerror(errno));
            return ret;
        }
        else
        {
            bytes_read += ret;
        }
    }
    while (bytes_read < len);

#ifdef DEBUG_PRINT_EVERY_BYTE_TX_RX
    int i;
    for (i = 0; i < bytes_read; i++)
    {
        printf("\tR: 0x%02x\n", buff[i]);
    }
#endif // DEBUG PRINT
    return 0;
#endif // ARCHITECTURE
    return LL_HAL_NA;
}


//-----------------------------  NON-RTOS REQUIRED HAL  -----------------------------------------//

//TODO: #if !defined(LL_SYM_USE_RTOS) && !defined(LL_LTE_USE_RTOS)
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Sleep for a few milliseconds.
///
/// This comes predefined for RTOS!
///
/// param[in] millis The amount of milliseconds to sleep for.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_sleep_ms(int32_t millis)
{
#if defined(WIN32) || defined (__MINGW32__)
    Sleep(millis);
    return 0;
#elif defined(__APPLE__) || defined(__linux__) || defined(__CYGWIN__)
    struct timespec time_sleep;

    time_sleep.tv_sec = millis / 1000;
    time_sleep.tv_nsec = (millis % 1000) * 1000;
    return nanosleep(&time_sleep, 0);
#endif
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Populate the tp struct with the system time of the host.
///
/// This comes predefined for RTOS!
///
/// param[out] tp Time struct.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_gettime(struct time *tp)
{
#if defined(WIN32) || defined(__MINGW32__)
// Modified from http://stackoverflow.com/questions/5404277/porting-clock-gettime-to-windows
    __int64 wintime;

    GetSystemTimeAsFileTime((FILETIME*)&wintime);
    wintime -= 116444736000000000ll;  //1jan1601 to 1jan1970
    tp->tv_sec = wintime / 10000000ll;           //seconds
    tp->tv_nsec = wintime % 10000000ll * 100;      //nano-seconds
    return 0;
#elif defined(__APPLE__)
    struct timeval now;
    int ret = gettimeofday(&now, NULL);

    if (ret)
    {
        return ret;
    }
    tp->tv_sec = now.tv_sec;
    tp->tv_nsec = now.tv_usec * 1000;

    return 0;
}
#elif defined(__linux__) || defined(__CYGWIN__)
    // WARNING: gettimeofday is subject to clock jitter caused by NTP or other services
    // changing the wall-clock time.
    //
    // Using gettimeofday instead of clock_gettime to simplify the Makefile, so that
    // this code can run on system with glibc versions < 2.17 without having to
    // link with -lrt because -lrt is not available on OS-X or Windows.
#if 1
    struct timeval now;
    int ret = gettimeofday(&now, NULL);

    if (ret)
    {
        return ret;
    }
    tp->tv_sec = now.tv_sec;
    tp->tv_nsec = now.tv_usec * 1000;

    return 0;
}
#else // DEAD CODE
    struct timespec now;
    int32_t ret;

    ret = clock_gettime(CLOCK_MONOTONIC, &now);
    tp->tv_sec = now.tv_sec;
    tp->tv_nsec = now.tv_nsec;
    return ret;
#endif // DEAD CODE
//TODO: #endif // (!defined(LL_SYM_USE_RTOS)) && !defined(LL_LTE_USE_RTOS))
#endif // Arch.

//-----------------------------------  GENERIC CALLBACKS  ---------------------------------------//

///////////////////////////////////////////////////////////////////////////////////////////////////
/// This function is called when a message is received by any Link Labs Backhaul.
///
/// param[in] backhaul Bit Flag that identifies which backhaul received the message.
/// param[in] buff The buffer containing the data from the message.
/// param[in] len The size of the buffer.
/// param[in] port The port the message was received from.
///////////////////////////////////////////////////////////////////////////////////////////////////
//void ll_bh_message_rx(uint8_t backhaul, uint8_t *buff, size_t len, uint8_t port)
//{
//    return LL_HAL_NA;
//}


//------------------------------  REQUIRED SYMPHONY LINK HAL ------------------------------------//

#if LL_BH_SYMPHONY_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Initialize the Symphony Module GPIO pins.
///
/// * IO Line    - GPIO input  [Rising Edge Trigger]
/// * RESET Line - GPIO output [Set Low]
///
/// returns LL_HAL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_init_sym_gpio(void)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Reset Pin.
///
/// The Reset pin is Active LOW.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_reset_pin_set(ll_pin_state_t state)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module IO Pin State.
///
/// The IO pin is HIGH when an IRQ event is triggered.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_sym_io_pin_get(void)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Power.
///
/// The Power Enable pin is Active HIGH.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_power_set(ll_pin_state_t state)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Boot Line.
///
/// The Boot pin is Active LOW.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_boot_set(ll_pin_state_t state)
{
    return LL_HAL_NA;
}


//--------------------  OPTIONAL SYMPHONY STATE MACHINE EVENT HANDLERS  -------------------------//

#if LL_SYM_USE_RTOS
///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine is about to go to sleep
/// to save power. This would be a good place to deactivate any other peripherals to save power.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_suspend_evnt(void)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine is about to resume power.
/// This is where all deactivated peripherals should be reactivated.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_resume_evnt(void)
{
    return LL_HAL_NA;
}
#endif // LL_SYM_USE_RTOS


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine changes state.
///
/// param[in] new_state The new state that the symphony state machine is transitioning to.
/// param[in] old_state The old state that the symphony state machine is transitioning from.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_state_change_evnt(sym_module_state_t new_state, sym_module_state_t old_state)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine processes an error.
///
/// param[in] err The error the state machine encountered.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_error_evnt(sym_error_code_t err)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine changes state.
///
/// param[in] new_state The new state that the symphony state machine is transitioning to.
/// param[in] old_state The old state that the symphony state machine is transitioning from.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_tx_done(void)
{
    return LL_HAL_NA;
}


//---------------------------  SYMPHONY LINK FOTA CALLBACKS  ------------------------------------//

#if LL_SYM_FOTA_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to open and allocate storage when a new file transfer begins.
/// This is called at the beginning of the file transfer.
///
/// @param[in] file_id ID of the incoming file
/// @param[in] file_version Version of the incoming file
/// @param[in] file_size Size (in bytes) of the incoming file
///
/// @return Return code to indicate success/error of operation
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_open_t(uint32_t file_id, uint32_t file_version,
        uint32_t file_size)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to read a chunk of file in memory
///
/// @param[in] file_id ID of the file
/// @param[in] file_version Version of the file
/// @param[in] offset Offset to begin reading file from
/// @param[out] payload Buffer to read requested bytes into
/// @param[in] len Number of bytes to read
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_read_t(uint32_t file_id, uint32_t file_version,
        uint32_t offset, uint8_t *payload, uint16_t len)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to write a chunk of file to memory
///
/// @param[in] file_id ID of the file
/// @param[in] file_version Version of the file
/// @param[in] offset Offset to begin writing file at
/// @param[in] payload Buffer with data to be written
/// @param[in] len Number of bytes to write
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_write_t(uint32_t file_id, uint32_t file_version,
        uint32_t offset, uint8_t *payload, uint16_t len)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when memory reading/writing is complete. This is called once all
/// segments have been received, when a file transfer is interrupted, or a file transfer is
/// canceled.
///
/// param[in] file_id The 32-bit unique ID of the file.
/// param[in] file_version The 32-bit unique file version.
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_close(uint32_t file_id, uint32_t file_version)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when FTP is complete.
///
/// @param[in] success 'true' if transfer was successful, 'false' otherwise
/// @param[in] file_id ID of the transferred file
/// @param[in] file_version Version of the transferred file
/// @param[in] file_size Size (in bytes) of the transferred file
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_apply(uint32_t file_id, uint32_t file_version,
       uint32_t file_size)
{
    return LL_HAL_NA;
}
#endif // LL_SYM_FOTA_EN
#endif // LL_BH_SYMPHONY_EN


//---------------------------------  REQUIRED LTE-M HAL -----------------------------------------//

#if LL_BH_LTE_M_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Initialize the LTE-M GPIO pins.
///
/// * IO Line           - GPIO input  [Rising Edge Trigger]
/// * WAKE STATUS Line  - GPIO input  [Rising Edge Trigger]
/// * WAKE REQUEST Line - GPIO output [Set Low]
///
/// returns LL_HAL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_init_lte_gpio(void)
{
    return LL_HAL_NA;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module IO Pin State.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_lte_io_pin_get(void)
{
    return LL_PIN_LOW;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module IO Pin State.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_lte_wake_status_pin_get(void)
{
    return LL_PIN_HIGH;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Reset Pin.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_lte_wake_request_pin_set(ll_pin_state_t state)
{
    (void) state;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module Reset Pin.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_lte_irq_pin_get(void)
{
    return LL_PIN_LOW;
}

// TODO: Add Other LTE-M Pin HAL functions.

#endif // LL_BH_LTE_M_EN

#ifdef __cplusplus
}
#endif // __cplusplus

