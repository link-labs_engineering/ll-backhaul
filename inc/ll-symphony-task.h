///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                     Thomas Steinholz  <thomas.steinholz@link-labs.com>, FEB 2018            ///
///                       Mark Bloechl    <mark.bloechl@link-labs.com>,    APRIL 2017           ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef LL_SYMPHONY_TASK_H
#define LL_SYMPHONY_TASK_H

#include "ll-backhaul-config.h"

#if LL_BH_SYMPHONY_EN
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                            INCLUDES                                           //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

#include <stdbool.h>
#include <stdint.h>
#if LL_SYM_USE_RTOS
#include "FreeRTOS.h"
#include "task.h"
#endif // LL_SYM_USE_RTOS
#include "ll_ifc.h"
#include "ll_ifc_symphony.h"



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                    CONSTANTS AND DEFINES                                      //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

#define SYM_MANUAL_MAILBOX_CHECK (0xFFFFFFFF) // code to disable automatic mailbox checks

#ifdef __cplusplus
extern "C" {
#endif



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                      TYPEDEFS / STRUCTS                                       //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

// TODO
typedef struct
{
    /// The Network Token of the given Symphony Module.
    uint32_t sym_network_token;

    /// The Application Token of the given Symphony Module.
    uint8_t sym_application_token[10];

    /// The Downlink Mode of the given Symphony Module.
    enum ll_downlink_mode sym_dl_mode;

    /// The Quality of Service of the given Symphony Module. The higher the Quality of Service is,
    uint8_t sym_qos;

    /// Will set whether the transmitted Symphony Link messages will be ACKed or not.
    bool sym_acked;

    /// The minimum connection interval (in seconds). The task will attempt to reconnect
    /// to lost gateways at the interval specified by this setting.
    uint32_t sym_min_conn_int_s;

    /// The maximum connection interval (in seconds). After consecutive failures, the task
    /// will gradually increase the connection interval until this limit is reached.
    /// May be set equal to min_connect_interval_s if a constant interval is desired.
    uint32_t sym_max_conn_int_s;

    /// The mailbox check interval. Ignored if Downlink Always On mode
    /// is enabled. Set to SYM_MANUAL_MAILBOX_CHECK to disable automatic
    /// mailbox checking.
    uint32_t sym_mailbox_check_int_s;
} sym_config_t;

// TODO
// module task states
typedef enum
{
    SYM_RESET,
    SYM_INITIALIZING,
    SYM_NOT_READY,
    SYM_SCANNING,
    SYM_READY,
    SYM_OFF,
    SYM_FOTA,
    SYM_STATE_ERROR
} sym_module_state_t;

// module supervisor commands
typedef enum
{
    SYM_CMD_RST,           // resets module (and powers it up, if necessary)
    SYM_CMD_REINIT,        // resets module AND wipes it's flash variables
    SYM_CMD_RESYNC,        // placeholder, currently does same as reset command
    SYM_CMD_CHECK_MAILBOX, // issues a mailbox check, generally performed autonomously by symphony task
    SYM_CMD_POWERDOWN,     // cuts power to module (for applications needing extremely low sleep current)
} sym_cmd_t;

// send message return codes
typedef enum
{
    SYM_MSG_ENQUEUED,
    SYM_MSG_QUEUE_FULL,
    SYM_MOD_NOT_READY,
    SYM_NULL_MSG,
    SYM_MSG_TOO_LONG
} sym_send_msg_ret_t;

typedef enum
{
    FTP_OK,               // no error
    FTP_WRONG_FILE_ID,    // file ID is not as expected
    FTP_WRONG_VERSION,    // file version is not as expected
    FTP_DUPLICATE_VERSION,// file version is already in memory
    FTP_FLASH_ERROR,      // there was an error accessing flash
} sym_ftp_ret_t;

typedef enum
{
    SYM_TX_SUCCESS = 1,
    SYM_TX_FAILED = 2,
} sym_tx_state_t;

typedef enum
{
    // minor errors
    SYM_IFC_ERR,
    SYM_MAX_TX_FAILURES_ERR,
    SYM_MINOR_ERROR_LIMIT,
    // major errors
    SYM_IFC_ERR_RESET,
    SYM_MODULE_STATE_ERROR,
    SYM_TASK_STATE_ERROR
} sym_err_code_t;



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                  PUBLIC FUNCTION PROTOTYPES                                   //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Initializes the Symphony State Machine.
///
/// returns 0 - success, negative otherwise.
///////////////////////////////////////////////////////////////////////////////////////////////////
int32_t sym_init_task(const sym_config_t *config);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Send the Symphony Task a command.
///
/// param[in] cmd_data The cmd that the Symphony Task should perform.
///
/// returns 0 - success, negative otherwise.
///////////////////////////////////////////////////////////////////////////////////////////////////
int32_t sym_enqueue_cmd(sym_cmd_t cmd_data);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Requests to send a message using the Symphony Link Backhaul.
///
/// Adds message to the queue, the actual sending of the message will be processed later in the
/// state machine.
///
/// param[in] msg The message buffer to send through Symphony Link.
/// param[in] msg_len The length of the message being sent.
/// param[in] ack Whether the message should be acknowledged or not.
/// param[in] port The port the message should be sent on.
///
/// returns The status of the message that was requested to be sent.
///////////////////////////////////////////////////////////////////////////////////////////////////
sym_send_msg_ret_t sym_send_msg(uint8_t *msg, uint16_t msg_len, bool ack, uint8_t port);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Will allow the Symphony Link Module to continue searching for a Gateway if it has timed out
/// past the user defined connection timeout.
///////////////////////////////////////////////////////////////////////////////////////////////////
void sym_reset_connection_interval(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get's the current state of the Symphony Link State Machine.
///
/// returns the current state of the Symphony Link State Machine.
///////////////////////////////////////////////////////////////////////////////////////////////////
sym_module_state_t sym_get_state(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Will not allow any mailbox checks if applicable.
///////////////////////////////////////////////////////////////////////////////////////////////////
void sym_inhibit_mailbox_check(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Will allow mailbox checks if applicable.
///////////////////////////////////////////////////////////////////////////////////////////////////
void sym_allow_mailbox_check(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Gets the last time the Symphony Link Module was "Ready" or connected to a Gateway in IDLE.
///
/// param[out] last_conn_time The last time the Symphony Link Task was "Ready".
///////////////////////////////////////////////////////////////////////////////////////////////////
void sym_get_last_connection_time(struct time *last_conn_time);

#if LL_SYM_USE_RTOS
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Exposes the RTOS Symphony Link Handle.
///
/// returns the RTOS Symphony Link Handle.
///////////////////////////////////////////////////////////////////////////////////////////////////
xTaskHandle sym_task_get_handle(void);
#else
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Updates the Symphony Task for non-RTOS systems.
///
/// param[out] tx_state The TX State of the Symphony Link Module
/// param[out] rx_state The RX State of the Symphony Link Module
///
/// returns the current state of the module.
///////////////////////////////////////////////////////////////////////////////////////////////////
enum ll_state sym_update_task(enum ll_tx_state *tx_state, enum ll_rx_state *rx_state);
#endif // LL_SYM_USE_RTOS

#ifdef __cplusplus
}
#endif

#endif // LL_BH_SYMPHONY_EN
#endif // LL_SYMPHONY_TASK_H
