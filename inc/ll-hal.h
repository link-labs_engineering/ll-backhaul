///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                     Thomas Steinholz  <thomas.steinholz@link-labs.com>, FEB 2018            ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef LL_BACKHAUL_HAL_H
#define LL_BACKHAUL_HAL_H

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                            INCLUDES                                           //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "ll-backhaul-config.h"
#include "ll_ifc.h"

#ifdef __cplusplus
extern "C" {
#endif

/// The Link Labs Backhaul definition for pin states.
typedef enum
{
    LL_PIN_LOW = 0,                 ///< Pin is low (0).
    LL_PIN_HIGH = 1,                ///< Pin is high (1).
} ll_pin_state_t;

/// The backhaul return codes defined.
typedef enum
{
    LL_RET_OK               = 0,    ///< Success!
    LL_RET_NET_TOKEN_UNDEF  = 1,    ///< The Network Token is not defined in the HAL.
} ll_ret_t;

/// The return codes for the user defined HAL Calls.
typedef enum
{
    LL_HAL_RET_OK        = 0,       ///< Success!
    LL_HAL_ERROR         = 1,       ///< HAL Write failed.
    LL_HAL_NA            = 2,       ///< Not implemented.
} ll_hal_ret_t;


/// The supported Link Labs backhauls.
typedef enum
{
    LL_SYM_BACKHAUL    = 0x01,       ///< Symphony Link.
    LL_LTE_M_BACKHAUL = 0x02,        ///< LTE-M.
} ll_backhaul_masks_t;



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                           PUBLIC PROTOTYPES FOR USER DEFINED HAL LAYER                        //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//------------------------------------  UART COMMUNICATION  -------------------------------------//

///////////////////////////////////////////////////////////////////////////////////////////////////
/// A UART Wrapper for Transmitting Messages to the desired backhaul.
///
/// param[in] backhaul Specifies the backhaul that the driver want's to communicate with using
//                      BitFlags defined in ll-backhaul.h.
/// param[in] buff The data buffer to transmit.
/// param[in] len The length of the data in the buffer to transmit.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_uart_write(uint8_t backhaul, uint8_t *buff, size_t len);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// A UART Wrapper for Receiving Messages to the desired backhaul.
///
/// param[in] backhaul Specifies the backhaul that the driver want's to communicate with using
//                      BitFlags defined in ll-backhaul.h.
/// param[in] buff The data buffer to receive.
/// param[in] len The length of the data in the buffer to transmit.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_uart_read(uint8_t backhaul, uint8_t *buff, size_t len);


void ll_bh_disable_uart(void);

void ll_bh_enable_uart(void);


//-----------------------------  NON-RTOS REQUIRED HAL  -----------------------------------------//

#if (!LL_SYM_USE_RTOS || !LL_LTE_USE_RTOS)
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Sleep for a few milliseconds.
///
/// This comes predefined for RTOS!
///
/// param[in] millis The amount of milliseconds to sleep for.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_sleep_ms(int32_t millis);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Populate the tp struct with the system time of the host.
///
/// This comes predefined for RTOS!
///
/// param[out] tp Time struct.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_gettime(struct time *tp);
#endif // (!defined(LL_SYM_USE_RTOS)) && !defined(LL_LTE_USE_RTOS))


//-----------------------------------  GENERIC CALLBACKS  ---------------------------------------//

///////////////////////////////////////////////////////////////////////////////////////////////////
/// This function is called when a message is received by any Link Labs Backhaul.
///
/// param[in] backhaul Bit Flag that identifies which backhaul received the message.
/// param[in] buff The buffer containing the data from the message.
/// param[in] len The size of the buffer.
/// param[in] port The port the message was received from.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_message_rx(uint8_t backhaul, uint8_t *buff, size_t len, uint8_t port);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// User defined method of outputting log messages from the backhaul library.
///
/// Note: Log Function should handle printing new line characters as well as other formatting given
/// the log level of the message.
///
/// param[in] log_level The log level of the log message (Debug, Warning, etc).
/// param[in] fmt The va argument list of the formatted log string.
///
/// returns LL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_hal_log(uint8_t log_level, const char *fmt, ...);


//------------------------------  REQUIRED SYMPHONY LINK HAL ------------------------------------//

#if LL_BH_SYMPHONY_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Initialize the Symphony Module GPIO pins.
///
/// * IO Line    - GPIO input  [Rising Edge Trigger]
/// * RESET Line - GPIO output [Set Low]
///
/// returns LL_HAL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_init_sym_gpio(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Reset Pin.
///
/// The Reset pin is Active LOW.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_reset_set(ll_pin_state_t state);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module IO Pin State.
///
/// The IO pin is HIGH when an IRQ event is triggered.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_sym_io_get(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Power.
///
/// The Power Enable pin is Active HIGH.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_power_set(ll_pin_state_t state);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Boot Line.
///
/// The Boot pin is Active LOW.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_boot_set(ll_pin_state_t state);


//--------------------  OPTIONAL SYMPHONY STATE MACHINE EVENT HANDLERS  -------------------------//

#if LL_SYM_USE_RTOS
///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine is about to go to sleep
/// to save power. This would be a good place to deactivate any other peripherals to save power.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_suspend_evnt(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine is about to resume power.
/// This is where all deactivated peripherals should be reactivated.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_resume_evnt(void);
#endif // LL_SYM_USE_RTOS

///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine changes state.
///
/// param[in] new_state The new state that the symphony state machine is transitioning to.
/// param[in] old_state The old state that the symphony state machine is transitioning from.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_state_change_evnt(/*sym_module_state_t new_state, sym_module_state_t old_state*/);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine processes an error.
///
/// param[in] err The error the state machine encountered.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_error_evnt(/*sym_error_code_t err*/);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// This OPTIONAL function is called whenever the symphony state machine changes state.
///
/// param[in] new_state The new state that the symphony state machine is transitioning to.
/// param[in] old_state The old state that the symphony state machine is transitioning from.
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_sym_on_tx_done(bool success);


//---------------------------  SYMPHONY LINK FOTA CALLBACKS  ------------------------------------//

#if LL_SYM_FOTA_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to open and allocate storage when a new file transfer begins.
/// This is called at the beginning of the file transfer.
///
/// @param[in] file_id ID of the incoming file
/// @param[in] file_version Version of the incoming file
/// @param[in] file_size Size (in bytes) of the incoming file
///
/// @return Return code to indicate success/error of operation
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_open_t(uint32_t file_id, uint32_t file_version,
        uint32_t file_size);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to read a chunk of file in memory
///
/// @param[in] file_id ID of the file
/// @param[in] file_version Version of the file
/// @param[in] offset Offset to begin reading file from
/// @param[out] payload Buffer to read requested bytes into
/// @param[in] len Number of bytes to read
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_read_t(uint32_t file_id, uint32_t file_version,
        uint32_t offset, uint8_t *payload, uint16_t len);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to write a chunk of file to memory
///
/// @param[in] file_id ID of the file
/// @param[in] file_version Version of the file
/// @param[in] offset Offset to begin writing file at
/// @param[in] payload Buffer with data to be written
/// @param[in] len Number of bytes to write
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_write_t(uint32_t file_id, uint32_t file_version,
        uint32_t offset, uint8_t *payload, uint16_t len);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when memory reading/writing is complete. This is called once all
/// segments have been received, when a file transfer is interrupted, or a file transfer is
/// canceled.
///
/// param[in] file_id The 32-bit unique ID of the file.
/// param[in] file_version The 32-bit unique file version.
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_close(uint32_t file_id, uint32_t file_version);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when FTP is complete.
///
/// @param[in] success 'true' if transfer was successful, 'false' otherwise
/// @param[in] file_id ID of the transferred file
/// @param[in] file_version Version of the transferred file
/// @param[in] file_size Size (in bytes) of the transferred file
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_ftp_return_code_t ll_bh_hal_sym_ftp_apply(uint32_t file_id, uint32_t file_version,
       uint32_t file_size);
#endif // LL_SYM_FOTA_EN
#endif // LL_BH_SYMPHONY_EN


//---------------------------------  REQUIRED LTE-M HAL -----------------------------------------//

#if LL_BH_LTE_M_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Initialize the LTE-M GPIO pins.
///
/// * IO Line           - GPIO input  [Rising Edge Trigger]
/// * WAKE STATUS Line  - GPIO input  [Rising Edge Trigger]
/// * WAKE REQUEST Line - GPIO output [Set Low]
///
/// returns LL_HAL_RET_OK - Success.
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_hal_ret_t ll_bh_init_lte_gpio(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module IO Pin State.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_lte_io_pin_get(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module IO Pin State.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_lte_wake_status_pin_get(void);


///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the Symphony Link Module Reset Pin.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_lte_wake_request_pin_set(ll_pin_state_t state);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the Symphony Link Module Reset Pin.
///
/// returns The state of the IO pin (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
ll_pin_state_t ll_bh_lte_irq_pin_get(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Set the LTE-M Module Enable Pin.
///
/// param[in] state The pin state to set the module (HIGH or LOW).
///////////////////////////////////////////////////////////////////////////////////////////////////
void ll_bh_lte_reset_pin_set(ll_pin_state_t state);

// TODO: Add Other LTE-M Pin HAL functions.

#endif // LL_BH_LTE_M_EN


#ifdef __cplusplus
}
#endif // __cplusplus

#endif // LL_BACKHAUL_HAL_H

