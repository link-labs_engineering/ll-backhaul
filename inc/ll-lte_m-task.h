///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                  Thomas Steinholz  <thomas.steinholz@link-labs.com>,      March 2018        ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef LTE_M_DRIVER_H // LTE_M_DRIVER_H
#define LTE_M_DRIVER_H // LTE_M_DRIVER_H

#include "ll-backhaul-config.h"
#if LL_BH_LTE_M_EN

#include "ll-hal.h"
#include "ll-backhaul-hal.h"
#include "lte_ifc.h"
#include "lte_ifc_bytedefs.h"
#include "ll_ifc_utils.h"
#include <stdint.h>
#include <stdbool.h>

#undef MISS_PKT_DBG

///////////////////////////////////////////////////////////////////////////////////////////////////
/// The possible return types for the LTE Driver Calls.
///////////////////////////////////////////////////////////////////////////////////////////////////
typedef enum
{
    LTE_OK,                   ///< The command executed succesfully.
    LTE_FAILED_TO_RESET,      ///< The driver could not reset the LTE module.
    LTE_INVALID_PARAM,        ///< The passed parameters are invalid.
    LTE_ERROR_QUEUE_FULL,     ///< The Event Queue is already full!
    LTE_FAILED_TO_SEND,       ///< Could not send message.
} lte_return_t;

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Statistics the LTE-M driver keeps track of.
///////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct
{
    float avg_time_to_send_s; ///< Average amount of time it takes to send an LTE-M msg in seconds.
    uint32_t msgs_sent;       ///< Amount of messages successfully sent from the LTE-M module.
    uint32_t msgs_retried;    ///< Amount of messages that required a retry to be sent out.
    uint8_t msgs_failed;      ///< Amount of messages that fully failed to send out of the module.
    uint8_t msgs_received;    ///< Amount of messages that have been recieved.
    float avg_rsrq_db;        ///< Average RSRQ of the LTE-M module in db.
    float avg_rsrp_dbm;       ///< Average RSRP of the LTE-M module in dbm.
    uint32_t last_cell_id;    ///< The last Cell ID of the LTE tower.
    uint32_t last_cell_tac;   ///< The last Cell TAC of the LTE tower.
    struct time last_update;  ///< Last time the cell id/tac was updated.
} lte_stats_t;

///////////////////////////////////////////////////////////////////////////////////////////////////
/// User defined callbacks for the LTE Device.
///////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct
{
    void *context; ///< User defined context passed to all user defined functions.

    /// User defined method that gets called when an LTE module has emptied out its TX QUEUE,
    /// signifying that there is nothing left to transmit (do to all success or failure).
    ///
    /// param[in] buff The data buffer of the message, data only valid in scope.
    /// param[in] len The length of the data buffer.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    void (*lte_tx_queue_cleared)(uint8_t num_txed, void *context);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    /// User defined method that gets called when an LTE message fails to get sent out. The user is
    /// given the message back and can then decide what to do with it.
    ///
    /// param[in] buff The data buffer of the message, data only valid in scope.
    /// param[in] len The length of the data buffer.
    /// param[in] port The port the message was received on.
    /// param[in] context User defined context.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    void (*lte_message_failed)(uint8_t *buff, uint16_t len, int8_t port, void *context);

    ///////////////////////////////////////////////////////////////////////////////////////////////
    /// User defined method that gets called when an LTE downlink has been recieved from the
    /// driver.
    ///
    /// param[in] buff The data buffer of the message, data only valid in scope.
    /// param[in] len The length of the data buffer.
    /// param[in] port The port the message was received on.
    /// param[in] context User defined context.
    ///////////////////////////////////////////////////////////////////////////////////////////////
    void (*lte_received_message)(uint8_t *buff, uint16_t len, int8_t port, void *context);
} lte_callbacks_t;

#ifdef MISS_PKT_DBG
int16_t get_invalid_pkt_num(void); // DEBUG
#endif // MISS_PKT_DBG

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Inializes the LTE Driver and regisiters the user define callbacks.
///
/// param[in] callbacks Pointer to the user defined callbacks.
///
/// returns the status of the intialization.
///////////////////////////////////////////////////////////////////////////////////////////////////
lte_return_t lte_init_driver(lte_callbacks_t *callbacks);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Cache message, and attempt to send it throught the LTE-M module.
///
/// This function will make a local copy of the buffer provided in case of a failed message. In
/// that case, it will attempt to resend the cached message until the LL_LTE_MAX_ATTEMPTS is
/// reached. Then the message will be removed and counted as a failure.
///
/// param[in] buff The data buffer to send.
/// param[in] size The size of the data buffer.
/// param[in] port The port to transmit the message on.
///
/// returns LTE_OK - Success
///         LTE_QUEUE_FULL - Maximum amount of messages are queued.
///////////////////////////////////////////////////////////////////////////////////////////////////
lte_return_t lte_send_message(uint8_t *buff, uint16_t size, int8_t port);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Will attempt to recover the LTE-M module.
///
/// This function should be called at regular intervals when lte_module_operational() returns false.
////////////////////////////////////////////////////////////////////////////////////////////////////
void lte_recover_module(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Returns if the LTE-M module is operational.
////////////////////////////////////////////////////////////////////////////////////////////////////
bool lte_module_operational(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Will tell the user when the LTE-M module needs to be serviced.
///
/// This function should be called regularly to detect when the HOST INTERRUPT LINE goes high. Then
/// a call to "lte_service_irq()" should be made to service the IRQ.
////////////////////////////////////////////////////////////////////////////////////////////////////
bool lte_irq_requires_service(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// To be called by the HAL when the host interrupt line goes high.
////////////////////////////////////////////////////////////////////////////////////////////////////
void lte_set_irq_high(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Services the LTE-M module's IRQ flag.
///
/// This function should be called by the HAL whenever the HOST INTERRUPT LINE goes high, either
/// via callback. Or by manually checking the state of the line in a loop.
///
////////////////////////////////////////////////////////////////////////////////////////////////////
void lte_service_irq(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// This funciton will check the timeouts of each message in the TX queue.
///
/// It is only nessisary to call this function after a message is queued in the driver, once the
/// number of messages in the TX_QUEUE is 0, or the lte_tx_queue_cleared callback is called, there
/// is no longer a need to service this function until another message is called.
///////////////////////////////////////////////////////////////////////////////////////////////////
void lte_service_timeout(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the current stats of the LTE driver.
///
/// param[out] stats The LTE stats.
///////////////////////////////////////////////////////////////////////////////////////////////////
void lte_get_stats(lte_stats_t *stats);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the current amount of messages in the TX_QUEUE.
///
/// returns The amount of messages in the TX_QUEUE.
///////////////////////////////////////////////////////////////////////////////////////////////////
uint8_t lte_get_tx_queue_size(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Sends a mailbox request to the cloud to recieve downlink.
///
/// returns LTE_OK - Success
///         LTE_QUEUE_FULL - Maximum amount of messages are queued.
///////////////////////////////////////////////////////////////////////////////////////////////////
lte_return_t lte_mailbox_req(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the latest Cell Tower Information.
///
/// param[out] cell_id The last Cell ID of the tower used.
/// param[out] cell_tac The last Cell TAC of the tower used.
/// param[out] info_age_s The amount of time since that information has been updated in seconds.
///////////////////////////////////////////////////////////////////////////////////////////////////
void lte_get_cell_info(uint32_t *cell_id, uint16_t *cell_tac, int32_t *info_age_s);

#endif // LL_BH_LTE_M_EN
#endif // LTE_M_DRIVER_H

