///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                     Thomas Steinholz  <thomas.steinholz@link-labs.com>, FEB 2018            ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef LL_COMMON_H
#define LL_COMMON_H

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                         INCLUDES                                              //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

#include <stdint.h>
#include "ll-backhaul-config.h"
#include "ll-hal.h"

#ifdef __cplusplus
extern "C" {
#endif



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                    CONSTANTS AND DEFINES                                      //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//---------------------------------  Time Conversions  ------------------------------------------//
#define NS_TO_MS(x) (x / 1e6) ///< Convert NanoSeconds->MilliSeconds.
#define NS_TO_S(x)  (x / 1e9) ///< Convert NanoSeconds->Seconds.
#define MS_TO_S(x)  (x / 1e3) ///< Convert MilliSeconds->Seconds.
#define MS_TO_NS(x) (x * 1e6) ///< Convert MilliSeconds->NanoSeconds.
#define S_TO_MS(x)  (x * 1e3) ///< Convert Seconds->MilliSeconds.
#define S_TO_NS(x)  (x * 1e9) ///< Convert Seconds->NanoSeconds.

//--------------------------------  Logging Definitions  ----------------------------------------//
#ifdef LL_BH_LOGGING
#if LL_BH_LOG_LEVEL >= LL_LOG_LEVEL_ERROR
#define LL_ERROR_LOG(...) ll_bh_hal_log(LL_LOG_LEVEL_ERROR, __VA_ARGS__)
#else
#define LL_ERROR_LOG(...)
#endif // LL_BH_LOG_LEVEL >= 1
#if LL_BH_LOG_LEVEL >= LL_LOG_LEVEL_WARNING
#define LL_WARNING_LOG(...) ll_bh_hal_log(LL_LOG_LEVEL_WARNING, __VA_ARGS__)
#else
#define LL_WARNING_LOG(...)
#endif // LL_BH_LOG_LEVEL >= 2
#if LL_BH_LOG_LEVEL >= LL_LOG_LEVEL_INFO
#define LL_INFO_LOG(...) ll_bh_hal_log(LL_LOG_LEVEL_INFO, __VA_ARGS__)
#else
#define LL_INFO_LOG(...)
#endif // LL_BH_LOG_LEVEL >= 3
#if LL_BH_LOG_LEVEL >= LL_LOG_LEVEL_DEBUG
#define LL_DEBUG_LOG(...) ll_bh_hal_log(LL_LOG_LEVEL_DEBUG, __VA_ARGS__)
#else
#define LL_DEBUG_LOG(...)
#endif // LL_BH_LOG_LEVEL >= 4
#if LL_BH_LOG_LEVEL == LL_LOG_LEVEL_TRACE
#define LL_TRACE_LOG(...) ll_bh_hal_log(LL_LOG_LEVEL_TRACE, __VA_ARGS__)
#else
#define LL_TRACE_LOG(...)
#endif // LL_BH_LOG_LEVEL == 5
#endif //LL_BH_LOGGING

//#define LL_ASSERT(x) // TODO: ll_bh_assert(x)

#if (!defined(LL_SYM_USE_RTOS) || !defined(LL_LTE_USE_RTOS))
#define NUM_FIFO_BYTES (5) ///< The maximum amount of bytes allowed in the fifo.
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                         TYPEDEFS                                              //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

typedef struct
{
    uint8_t buf[NUM_FIFO_BYTES];
    uint16_t take;
    uint16_t put;
} byte_fifo_t;


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                               PUBLIC FUNCITON PROTOTYPES                                      //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Initialize the Byte FIFO.
///////////////////////////////////////////////////////////////////////////////////////////////////
void fifo_init(byte_fifo_t* f);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// @brief
///  Initiate a Tx operation over the Module USART.
///
/// @details
///
/// @param[in] uint8_t* buf
///  A buffer of uint8_t values to send out
///
/// @param[in] uint16_t  len
///  How many bytes to send out
///
/// @return None.
///////////////////////////////////////////////////////////////////////////////////////////////////
void fifo_push(byte_fifo_t* f, uint8_t* buf, uint16_t len);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// @brief
///   Check to see if there are any bytes to Tx over module ifc
///
/// @details
///
/// @param[out] uint8_t* p_byte
///  This pointer will be written with the byte to write if return value = 1
///
/// @return byte_to_tx:
///   1 = there is a byte that needs to be transmitted over host ifc
///   0 = no bytes to Tx at this time
///////////////////////////////////////////////////////////////////////////////////////////////////
uint8_t fifo_get(byte_fifo_t* f, uint8_t* p_byte);
#endif // !defined(LL_SYM_USE_RTOS) || !defined(LL_LTE_USE_RTOS)

#ifdef __cplusplus
}
#endif

#endif // LL_COMMON_H
