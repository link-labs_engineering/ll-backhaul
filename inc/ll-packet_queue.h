//
// \file    packet_queue.h
// \brief   Generic packet queue header.
// \details
//      Definitions and API related to a generic packet queue.
// \author  Scott Wohler
// \author  Thomas Steinholz
// \version 0.0.2
//
// \copyright LinkLabs, 2017
//
#ifndef PACKET_QUEUE_H
#define PACKET_QUEUE_H

// Includes ------------------------------------------------------------------
#include <stdint.h>
#include <stdlib.h>

#include <project.h>

// Defines -------------------------------------------------------------------
#ifdef USE_FREERTOS
#define PKT_QUEUE_MAX_TIME_TO_BLOCK_MS  (50u)
#endif

#define PKT_QUEUE_MAX_PAYLOAD_LENGTH    (256)

// Typedefs ------------------------------------------------------------------
typedef enum
{
    PKT_PENDING   = 0,
    PKT_REQUESTED = 1,
    PKT_SENT      = 2,
    PKT_RECIEVED  = 3,
} packet_state_t;

typedef struct __attribute__((packed)) packet_info
{
    uint16_t length;        // 1-256 Bytes
    uint8_t port;           // Port
    uint32_t id;            // Unique Identifier
    struct time timestamp;  // Timestamp
} packet_info_t;

typedef struct __attribute__((packed)) packet_queue
{
    uint8_t *buf;
    uint32_t len;
    uint32_t put_idx;
    uint32_t get_idx;
    uint32_t bytes_available;
    uint32_t num_packets;
#ifdef USE_FREERTOS
    void* mutex;
#endif
} packet_queue_t;

#ifdef USE_FREERTOS
// Usage Example:
//   packet_queue_t queue;
//   packet_queue_init(&queue, 500);
//  Returns EXIT_SUCCESS, EXIT_FAILURE
int32_t packet_queue_init(packet_queue_t* p, uint32_t size_bytes);
#else
// Usage Example:
//   uint8_t queue_buffer[500];
//   packet_queue_t queue;
//   packet_queue_init(&queue, queue_buffer, 500);
//  Returns EXIT_SUCCESS, EXIT_FAILURE
int32_t packet_queue_init(packet_queue_t* p, uint8_t* buffer, uint32_t size_bytes);
#endif

// Exported Functions --------------------------------------------------------
void packet_queue_flush(packet_queue_t* p);
uint32_t packet_queue_num_packets(packet_queue_t* p);
uint32_t packet_queue_usage_percentage(packet_queue_t* p);

int32_t packet_queue_push_back(packet_queue_t* p, const packet_info_t* d, const uint8_t* payload);
int32_t packet_queue_push_front(packet_queue_t* p, const packet_info_t* d, const uint8_t* payload);
int32_t packet_queue_pop_front(packet_queue_t* p, packet_info_t* d, uint8_t* payload);
int32_t packet_queue_peek_front(packet_queue_t* p, pkt_info_t* pkt_info);

int32_t packet_queue_peek(packet_queue_t *p, uint32_t index, packet_info_t *d);
int32_t packet_queue_pop_index(packet_queue_t *p, uint32_t index, packet_info_t *d, uint8_t *payload);
#endif // PACKET_QUEUE_H
