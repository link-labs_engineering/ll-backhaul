///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                     Thomas Steinholz  <thomas.steinholz@link-labs.com>, FEB 2018            ///
///                       Mark Bloechl    <mark.bloechl@link-labs.com>,    APRIL 2017           ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#include "ll-symphony-task.h"

#if LL_BH_SYMPHONY_EN
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                         INCLUDES                                              //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

#include <stdlib.h>
#include <string.h>
#if LL_SYM_USE_RTOS
#include "FreeRTOSConfig.h"
#include "queue.h"
#endif // LL_SYM_USE_RTOS
#if LL_SYM_FOTA_EN
#include "ll_ifc_ftp.h"
#endif // LL_SYM_FOTA_EN
#include "ll-hal.h"
#include "ll-common.h"
#include "ll_ifc_no_mac.h"



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                    CONSTANTS AND DEFINES                                      //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//-------------------------  Symphony Task Configuration  ---------------------------------------//
#define SYM_TX_QUEUE_SIZE             (1)      ///< The amount of TX messages that can be stored
                                               ///<     in the TX Queue or FIFO.
#define SYM_CMD_QUEUE_SIZE            (5)      ///< The amount of CMD messages that can be sroted
                                               ///<     in the CMD Queue or FIFO.
#define IFC_MAX_ERRS                  (5)      ///< Max number of sequential errors with ll_ifc
                                               ///<     before resetting module.
#define IFC_CLEAR_FLASH               (3)      ///< Max number of resets before completely
                                               ///<     clearing flash variables.
#define SYM_ACTIVE_TASK_RATE_MS       (1000)   ///< Rate symphony task runs at when activity is
                                               ///<     occurring (or expected).
#define SYM_FOTA_TASK_RATE_MS         (100)    ///< Rate symphony task runs at when FOTA is
                                               ///<     occurring.
#define SYM_POST_RESET_DELAY_MS       (2000)   ///< Delay after resetting module before attempting
                                               ///<     communications.
#define SYM_MIN_STATUS_UPDATE_RATE_MS (300000) ///< Max length of time to go without checking module
                                               ///<     status.
#define SYM_DISCONNECTED_TIMEOUT_MS   (100000) ///< Time to wait after scan before going to NOT_READY
                                               ///<     state when device reports DISCONNECTED.
#define SYM_INITIALIZING_TIMEOUT_MS   (180000) ///< Time to wait after scan before going to NOT_READY
                                               ///<     state when device reports INITIALIZING.
#define SYM_TX_TIMEOUT_MS             (120000) ///< Timeout until TX_SUCCESS or TX_ERROR is received
                                               ///<     when sending a message.
#define SYM_MAX_TX_FAILURES           (5)      ///< Max number of consecutive tx errors before going
                                               ///<     to not ready state.
#define SYM_DL_CHECK_TIMEOUT_S        (30)     ///< Length of time after mailbox check before
                                               ///<     assuming no messages will be received.
#define SYM_FTP_DONE_TIMEOUT_MS       (10000)  ///< Time after FTP apply before ftp_done called.
//------------------------------  Supported Symphony Versions  ----------------------------------//
#define SUPPORTED_SYM_MOD_VERSION_MAJOR (1)    ///< The supported major module version.
#define SUPPORTED_SYM_MOD_VERSION_MINOR (5)    ///< The supported minor module version.
#define SUPPORTED_SYM_MOD_VERSION_TAG   (0)    ///< The supported tag module version.

#define SUPPORTED_SYM_IFC_VERSION_MAJOR (0)    ///< The supported major module ifc version.
#define SUPPORTED_SYM_IFC_VERSION_MINOR (6)    ///< The supported minor module ifc version.
#define SUPPORTED_SYM_IFC_VERSION_TAG   (0)    ///< The supported tag module ifc version.
//-----------------------------------  FreeRTOS Task Config  ------------------------------------//
#if LL_SYM_USE_RTOS
#define SYM_TASK_PRIORITY (tskIDLE_PRIORITY + 3)
#endif // LL_SYM_USE_RTOS

//-------------------------------  Compiler Warning Detection  ----------------------------------//

#if LL_BH_SYMPHONY_EN
#if SUPPORTED_SYM_IFC_VERSION_MAJOR != IFC_VERSION_MAJOR \
 || SUPPORTED_SYM_IFC_VERSION_MINOR != IFC_VERSION_MINOR \
 || SUPPORTED_SYM_IFC_VERSION_TAG   != IFC_VERSION_TAG
#error "SYMPHONY IFC IS OUT OF SYNC!"
#endif // Symphony IFC Version Verification.
#endif // LL_BH_SYMPHONY_EN


//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                         TYPEDEFS                                              //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

// TODO
// error return codes
typedef enum { NO_ERR, ERR_NO_RESET, ERR_RESET } err_check_ret_t;
// tx message struct
typedef struct
{
    uint16_t msg_len; // length of message to be sent
    uint8_t *msg;     // pointer to message
    bool ack; // true if ACK requested (note: xTaskToNotify will be notified when message is sent to
              // module if ack = false)
    uint8_t port;                             // transmit port
} sym_tx_msg_struct_t;
// FTP variables
typedef struct
{
    bool success;
    uint32_t file_id;
    uint32_t file_ver;
    uint32_t file_sz;
} sym_ftp_var_t;



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                                    LOCAL VARIABLES                                            //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//-----------------------------------  RTOS Variables  ------------------------------------------//
#if LL_SYM_USE_RTOS
static xTaskHandle         s_LL_symphonyHandle;     ///< RTOS Task Handle.
static xQueueHandle        s_sym_cmd_queue;         ///< The RTOS Command Queue.
static xQueueHandle        s_sym_tx_queue;          ///< The RTOS TX MSG Queue.
#else
//---------------------------------  Non-RTOS Variables  ----------------------------------------//
//static byte_fifo_t         s_sym_cmd_queue;        ///< Command Queue.
//static byte_fifo_t         s_sym_tx_queue;         ///< TX MSG Queue.
static uint8_t             s_sym_tx_queue_index;   ///< Current TX Queue index.
static uint8_t             s_sym_tx_queue_cap;     ///< Current TX Queue capacity.
static sym_tx_msg_struct_t s_tx_queue_data[SYM_TX_QUEUE_SIZE];   ///< Storage for the cmd queue.
#endif // LL_SYM_USE_RTOS
//----------------------------------  Symphony Variables  ---------------------------------------//
static enum ll_state       s_sym_state;             ///< The current state of the Symphony Task.
static enum ll_tx_state    s_tx_state;              ///< The TX state of the module.
static enum ll_rx_state    s_rx_state;              ///< The RX state of the module.
static uint32_t            s_irq_flags;             ///< Current IRQ Flags from the Module.
//------------------------------------  FTP Variables  ------------------------------------------//
#if LL_SYM_FOTA_EN
static ll_ftp_t            s_ftp;                   ///< The FOTA struct that manages it's state.
static ll_ftp_callbacks_t  s_ftp_callbacks;         ///< The user supplied FOTA callbacks.
static sym_ftp_var_t       s_ftp_vars;              ///< The current file transfer info.
static struct time         s_fota_cycle_time;       ///< The last time we cycled FOTA.
#endif // LL_SYM_FOTA_EN
//--------------------------------  Symphony Task Variables  ------------------------------------//
static sym_config_t        s_sym_config;            ///< The Symphony Configuration.
static uint32_t            s_err_count;             ///< The running total amount of errors.
static uint32_t            s_clear_flash_count;     ///< Module flash erase count (resets device).
static uint32_t            s_connect_int_modifier;  ///< Current time spent connecting.
static uint32_t            s_tx_fail_count;         ///< The amount of times a TX has failed.
static bool                s_tx_in_progress;        ///< Whether a TX is in progress or not.
static bool                s_mailbox_check_inhibit; ///< Whether to inhibit a mailbox check.
static sym_module_state_t  s_current_state;         ///< The current state of the module.
static struct time         s_state_entry_time;      ///< The time the state was entered.
static struct time         s_last_dl_check_time;    ///< The last time we did a DL check.
static struct time         s_msg_send_time;         ///< The last time we sent a message.
static struct time         s_status_update_time;    ///< The last time we did a status update.
static struct time         s_last_ready_time;       ///< The last time we were ready.
static struct time         s_dl_check_timeout_time; ///< The last time we checked the timeout.
static uint8_t             s_dl_msg_bfr[256];       ///< The downlink message buffer.
static uint16_t            s_dl_msg_len;            ///< The downlink message length.



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                              PRIVATE FUNCITON PROTOTYPES                                      //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//-------------------------------------  RTOS Tasks  --------------------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////
/// The Symphony RTOS Task, Handles the State Machine; Also works for the Non-RTOS version.
///
/// param[in] argument Non-Used RTOS Feature.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _symphony_task(void const *argument);

//---------------------------------  State Processing Functions  --------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Reset State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_reset_state(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Initializing State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_initializing(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Scanning State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_module_scanning(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Not Ready State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_not_ready(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Ready State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_ready(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the OFF State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_off(void);

#if LL_SYM_FOTA_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handles the FOTA State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_fota(void);
#endif // LL_SYM_FOTA_EN

//----------------------------------  Utility Functions  ----------------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Checks for Symphony Downlink if applicable in the user configuration.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_downlink_check(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Checks the return of ll_ifc calls to verify success and track any errors.
///
/// param[in] ifc_ret The return from the ifc call.
///
/// returns The Symphony Task action based on the amount of errors recieved.
///////////////////////////////////////////////////////////////////////////////////////////////////
static err_check_ret_t _sym_ll_ifc_error_check(int32_t ifc_ret);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Checks the return of ll_ifc calls to verify success and track any errors.
///
/// param[in] ifc_ret The return from the ifc call.
///
/// returns The Symphony Task action based on the amount of errors recieved.
///////////////////////////////////////////////////////////////////////////////////////////////////
static err_check_ret_t _sym_get_module_status(bool force);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Send the message to the module.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_process_tx_state(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Recieve the message from the module.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_process_rx_state(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Check and process any user submitted commands.
///
/// param[in] state_in The state the module is currently in.
///
/// returns The next module state.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_process_cmd_queue(sym_module_state_t state_in);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Calculates the difference in time between a given time and the current time.
///
/// param[in] t0 The previous time that we want to find the diffence of.
///
/// returns the difference of time between t0 and when the function is called.
///////////////////////////////////////////////////////////////////////////////////////////////////
static int32_t _sym_time_to_now_ms(struct time *t0);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Returns the user defined connection interval in milliseconds.
///
/// returns the user defeined connection interval in milliseconds.
///////////////////////////////////////////////////////////////////////////////////////////////////
static uint32_t _sym_get_connection_int_ms(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Increments the connection modifier to keep track of the amount of connection attempts.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_inc_connection_modifier(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Clears the TX Queue of any pending messages.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_flush_tx_queue(void);

#if LL_SYM_USE_RTOS
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Calculates the amount of time the RTOS Symphony Task can delay between updates.
///
/// returns the amount of ticks the task can sleep.
///////////////////////////////////////////////////////////////////////////////////////////////////
static portTickType _sym_get_task_delay(void);
#endif // LL_SYM_USE_RTOS

//-------------------------------  LL FTP Functions & Callbacks ---------------------------------//
#if LL_SYM_FOTA_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Updates the FOTA state machine to processes the file transfer.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_cycle_ftp(void);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to open and allocate storage when a new file transfer begins.
/// This is called at the beginning of the file transfer.
///
/// @param[in] file_id ID of the incoming file.
/// @param[in] file_version Version of the incoming file.
/// @param[in] file_size Size (in bytes) of the incoming file.
///
/// @return Return code to indicate success/error of operation.
///////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_open_callback(uint32_t file_id, uint32_t file_version,
                                               uint32_t file_size);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to read a chunk of file in memory.
///
/// @param[in] file_id ID of the file.
/// @param[in] file_version Version of the file.
/// @param[in] offset Offset to begin reading file from.
/// @param[out] payload Buffer to read requested bytes into.
/// @param[in] len Number of bytes to read.
///
/// @return Return code to indicate success/error of operation.
////////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_read_callback(uint32_t file_id, uint32_t file_version,
                                               uint32_t offset, uint8_t *payload, uint16_t len);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to write a chunk of file to memory.
///
/// @param[in] file_id ID of the file.
/// @param[in] file_version Version of the file.
/// @param[in] offset Offset to begin writing file at.
/// @param[in] payload Buffer with data to be written.
/// @param[in] len Number of bytes to write.
///
/// @return Return code to indicate success/error of operation.
////////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_write_callback(uint32_t file_id, uint32_t file_version,
                                                uint32_t offset, uint8_t *payload, uint16_t len);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when memory reading/writing is complete. This is called once all
/// segments have been received, when a file transfer is interrupted, or a file transfer is
/// canceled.
///
/// param[in] file_id The 32-bit unique ID of the file.
/// param[in] file_version The 32-bit unique file version.
///
/// @return Return code to indicate success/error of operation.
////////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_close_callback(uint32_t file_id, uint32_t file_version);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when FTP is complete.
///
/// @param[in] success 'true' if transfer was successful, 'false' otherwise.
/// @param[in] file_id ID of the transferred file.
/// @param[in] file_version Version of the transferred file.
/// @param[in] file_size Size (in bytes) of the transferred file.
///
/// @return Return code to indicate success/error of operation.
///////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_apply_callback(uint32_t file_id, uint32_t file_version,
                                                uint32_t file_size);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when an uplink message should be sent.
///
/// @param[in] buf The message data.
/// @param[in] len The message length.
/// @param[in] acked If the message should be acked.
/// @param[in] port The port of the message.
///
/// @return Return code to indicate success/error of operation.
///////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_send_uplink_callback(uint8_t *buf, uint8_t len, bool acked,
                                                      uint8_t port);

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when Downlink Always ON is required.
///
/// @param[in] downlink_on 'true' if Downlink Mode should be Always On, else it doesn't matter.
///////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_dl_config_callback(bool downlink_on);
#endif // LL_SYM_FOTA_EN



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                              PUBLIC FUNCTION IMPLEMENTATIONS                                  //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Initializes the Symphony State Machine.
///
/// returns 0 - success, negative otherwise.
///////////////////////////////////////////////////////////////////////////////////////////////////
int32_t sym_init_task(const sym_config_t *config)
{
    int32_t ret             = 0;

    if (!config)
    {
        LL_ERROR_LOG("Symphony: Invalid Config!");
        return -1;
    }
    
    // Save config.
    s_sym_config.sym_network_token       = config->sym_network_token;
    s_sym_config.sym_dl_mode             = config->sym_dl_mode;
    s_sym_config.sym_qos                 = config->sym_qos;
    s_sym_config.sym_acked               = config->sym_acked;
    s_sym_config.sym_min_conn_int_s      = config->sym_min_conn_int_s;
    s_sym_config.sym_max_conn_int_s      = config->sym_max_conn_int_s;
    s_sym_config.sym_mailbox_check_int_s = config->sym_mailbox_check_int_s;
    memcpy(s_sym_config.sym_application_token, config->sym_application_token, sizeof(config->sym_application_token));

    LL_TRACE_LOG("Symphony: Initializing Symphony Task...");

    // Initialize Static Global Variables.
    s_err_count             = 0;
    s_clear_flash_count     = 0;
    s_connect_int_modifier  = 0;
    s_tx_fail_count         = 0;
    s_tx_in_progress        = false;
    s_mailbox_check_inhibit = false;
    // Reset the module in init for non-RTOS systems.
#if LL_SYM_USE_RTOS // TODO: Might not be necessary
    s_current_state         = SYM_RESET;
#else
    s_current_state         = _sym_reset_state();
#endif // LL_SYM_USE_RTOS

    // Initialize Start Times.
    ll_bh_hal_gettime(&s_state_entry_time);
    ll_bh_hal_gettime(&s_last_dl_check_time);
    ll_bh_hal_gettime(&s_dl_check_timeout_time);

    // Initialize FTP.
#if LL_SYM_FOTA_EN
    LL_TRACE_LOG("Symphony: Initialize FTP.");
    gettime(&s_fota_cycle_tick);

    s_ftp_callbacks.apply  = _ftp_apply_callback;
    s_ftp_callbacks.close  = _ftp_close_callback;
    s_ftp_callbacks.config = _ftp_dl_config_callback;
    s_ftp_callbacks.open   = _ftp_open_callback;
    s_ftp_callbacks.read   = _ftp_read_callback;
    s_ftp_callbacks.uplink = _ftp_send_uplink_callback;
    s_ftp_callbacks.write  = _ftp_write_callback;

    memset(&s_ftp_vars, 0, sizeof(sym_ftp_var_t));

    ret = ll_ftp_init(&s_ftp, &s_ftp_callbacks);
    if (ret < 0)
    {
        LL_ERROR_LOG("Symphony: FTP Initialization FAILED! (%i)", ret);
        return ret;
    }

    LL_TRACE_LOG("Symphony: FTP Initialized Successfully.");
#endif // LL_SYM_FOTA_EN

#if LL_SYM_USE_RTOS
    // Initialize RTOS Task.
    LL_TRACE_LOG("Symphony: Initialize RTOS Task.");
    // Create RTOS Task.
    xTaskCreate((TaskFunction_t)(_symphony_task), (const char *) "sym_task",
                configMINIMAL_STACK_SIZE, NULL, SYM_TASK_PRIORITY, &s_LL_symphonyHandle);

    // Create RTOS Queues.
    s_sym_cmd_queue = xQueueCreate(SYM_CMD_QUEUE_SIZE, sizeof(sym_cmd_t));
    s_sym_tx_queue  = xQueueCreate(SYM_TX_QUEUE_SIZE,  sizeof(sym_tx_msg_struct_t));

    LL_TRACE_LOG("Symphony: Initialized RTOS Task.");
#else
    // Initialize Non-RTOS Task/Queues.
    //fifo_init(&s_sym_tx_queue);
    //fifo_init(&s_sym_cmd_queue);

    s_sym_tx_queue_index = 0;
#endif // LL_SYM_USE_RTOS

    // Configure Module Pins.
    LL_TRACE_LOG("Symphony: Initialize Module GPIO.");
    ll_bh_init_sym_gpio();
    ll_bh_sym_power_set(LL_PIN_HIGH); // _set_power(SYM_POWER_ON);
    ll_bh_sym_boot_set(LL_PIN_HIGH);  // _set_boot(SYM_NORMAL_MODE);
    ll_bh_sym_reset_set(LL_PIN_HIGH); // _set_reset(SYM_NOT_IN_RESET);

    LL_TRACE_LOG("Symphony: Initialization Complete.");

    return (ret);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Send the Symphony Task a command.
///
/// param[in] cmd_data The cmd that the Symphony Task should perform.
///
/// returns 0 - success, negative otherwise.
///////////////////////////////////////////////////////////////////////////////////////////////////
int32_t sym_enqueue_cmd(sym_cmd_t cmd_data)
{
#if LL_SYM_USE_RTOS
    if (xQueueSendToBack(s_sym_cmd_queue, &cmd_data, 0) == errQUEUE_FULL)
    {
        LL_WARNING_LOG("Symphony: Can't enqueue command (%i), RTOS QUEUE FULL!", cmd_data);
        return (-1);
    }
    else
    {
        xTaskNotifyGive(s_LL_symphonyHandle);
        LL_TRACE_LOG("Symphony: RTOS Message (%i) Enqueded successfully!", cmd_data);
        return (0);
    }
#else
    // Using the pointer field to store the command data.
    //fifo_push(&s_sym_cmd_queue, (uint8_t*) cmd_data, 1);
#endif // LL_SYM_USE_RTOS
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Requests to send a message using the Symphony Link Backhaul.
///
/// Adds message to the queue, the actual sending of the message will be processed later in the
/// state machine.
///
/// param[in] msg The message buffer to send through Symphony Link.
/// param[in] msg_len The length of the message being sent.
/// param[in] ack Whether the message should be acknowledged or not.
/// param[in] port The port the message should be sent on.
///
/// returns The status of the message that was requested to be sent.
///////////////////////////////////////////////////////////////////////////////////////////////////
sym_send_msg_ret_t sym_send_msg(uint8_t *msg, uint16_t msg_len, bool ack, uint8_t port)
{
    LL_TRACE_LOG("Symphony: Send Message...");

    // TODO: check that there is not a message in progress.

    // TODO: print message data (trace log)

    if (msg_len == 0)
    {
        LL_ERROR_LOG("Symphony: Can't Send Message with 0 length!");
        return (SYM_NULL_MSG);
    }
    else if (msg_len > 255)
    {
        LL_ERROR_LOG("Symphony: Message length is too long! (must be < 256 bytes)");
        return (SYM_MSG_TOO_LONG);
    }

    LL_TRACE_LOG("Symphony: Message is valid.");

    if (s_current_state == SYM_READY)
    {
#if LL_SYM_USE_RTOS
        sym_tx_msg_struct_t cmd_data;

        cmd_data.msg     = msg;
        cmd_data.msg_len = msg_len;
        cmd_data.ack     = ack;
        cmd_data.port    = port;

        if (xQueueSendToBack(s_sym_tx_queue, &cmd_data, 0) == errQUEUE_FULL)
        {
            LL_WARNING_LOG("Symphony: Can't enqueue uplink, RTOS QUEUE FULL!");
            return (SYM_MSG_QUEUE_FULL);
        }
        else
        {
            xTaskNotifyGive(s_LL_symphonyHandle);
            LL_TRACE_LOG("Symphony: RTOS uplink enqueded successfully!");
            return (SYM_MSG_ENQUEUED);
        }
#else
        // Non-RTOS Enqueue.
        if (s_sym_tx_queue_cap + 1 > SYM_TX_QUEUE_SIZE)
        {
            LL_WARNING_LOG("Symphony: Can't enqueue uplink, RTOS QUEUE FULL!");
            return (SYM_MSG_QUEUE_FULL);
        }

        s_sym_tx_queue_cap++;

        s_sym_tx_queue_index = s_sym_tx_queue_index >= SYM_TX_QUEUE_SIZE - 1 ?
                                   0 : s_sym_tx_queue_index + 1;

        s_tx_queue_data[s_sym_tx_queue_index].msg     = msg;
        s_tx_queue_data[s_sym_tx_queue_index].msg_len = msg_len;
        s_tx_queue_data[s_sym_tx_queue_index].ack     = ack;
        s_tx_queue_data[s_sym_tx_queue_index].port    = port;

        // Push the index of the data onto the queue.
        //fifo_push(&s_sym_tx_queue, &s_sym_tx_queue_index, 1);
#endif // LL_SYM_USE_RTOS
    }
    else
    {
        LL_WARNING_LOG("Symphony: Can't send message, No Gateway Connection!");
        return SYM_MOD_NOT_READY;
    }
}

enum ll_state sym_update_task(enum ll_tx_state *tx_state, enum ll_rx_state *rx_state)
{
    if (tx_state)
    {
        *tx_state = s_tx_state;
    }

    if (rx_state)
    {
        *rx_state = s_rx_state;
    }

    _symphony_task(NULL);
    return s_sym_state;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Will allow the Symphony Link Module to continue searching for a Gateway if it has timed out
/// past the user defined connection timeout.
///////////////////////////////////////////////////////////////////////////////////////////////////
inline void sym_reset_connection_interval(void)
{
    s_connect_int_modifier = 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get's the current state of the Symphony Link State Machine.
///
/// returns the current state of the Symphony Link State Machine.
///////////////////////////////////////////////////////////////////////////////////////////////////
inline sym_module_state_t sym_get_state(void)
{
    return (s_current_state);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Will not allow any mailbox checks if applicable.
///////////////////////////////////////////////////////////////////////////////////////////////////
inline void sym_inhibit_mailbox_check(void)
{
    LL_INFO_LOG("Symphony: Mailbox Checks INHIBITED");
    s_mailbox_check_inhibit = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Will allow mailbox checks if applicable.
///////////////////////////////////////////////////////////////////////////////////////////////////
inline void sym_allow_mailbox_check(void)
{
    LL_INFO_LOG("Symphony: Mailbox Checks ALLOWED");
    s_mailbox_check_inhibit = false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Gets the last time the Symphony Link Module was "Ready" or connected to a Gateway in IDLE.
///
/// param[out] last_conn_time The last time the Symphony Link Task was "Ready".
///////////////////////////////////////////////////////////////////////////////////////////////////
inline void sym_get_last_connection_time(struct time *last_conn_time)
{
    last_conn_time->tv_sec  = s_last_ready_time.tv_sec;
    last_conn_time->tv_nsec = s_last_ready_time.tv_nsec;
}

#if LL_SYM_USE_RTOS
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Exposes the RTOS Symphony Link Handle.
///
/// returns the RTOS Symphony Link Handle.
///////////////////////////////////////////////////////////////////////////////////////////////////
inline xTaskHandle sym_task_get_handle(void)
{
    return (s_LL_symphonyHandle);
}
#endif // LL_SYM_USE_RTOS



//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//
//                               PRIVATE FUNCTION IMPLEMENTATIONS                                //
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!//

//-------------------------------------  RTOS Tasks  --------------------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////
/// The Symphony RTOS Task, Handles the State Machine; Also works for the Non-RTOS version.
///
/// param[in] argument Non-Used RTOS Feature.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _symphony_task(void const *argument)
{
    sym_module_state_t new_state;
#if LL_SYM_USE_RTOS
    LL_TRACE_LOG("Symphony: Task starting...");
    ll_bh_sym_on_state_change_evnt(s_current_state, s_current_state);
    for (;;)
    {
#endif // LL_SYM_USE_RTOS
        // Process current State
        switch (s_current_state)
        {
            case SYM_RESET:
                LL_TRACE_LOG("Symphony: RESET STATE UPDATE");
                new_state = _sym_reset_state();
                break;
            case SYM_INITIALIZING:
                LL_TRACE_LOG("Symphony: INITIALIZING STATE UPDATE");
                new_state = _sym_initializing();
                break;
            case SYM_SCANNING:
                LL_TRACE_LOG("Symphony: SCANNING STATE UPDATE");
                new_state = _sym_module_scanning();
                break;
            case SYM_NOT_READY:
                LL_TRACE_LOG("Symphony: NOT READY STATE UPDATE");
                new_state = _sym_not_ready();
                break;
            case SYM_READY:
                LL_TRACE_LOG("Symphony: READY STATE UPDATE");
                new_state = _sym_ready();
                break;
            case SYM_OFF:
                LL_TRACE_LOG("Symphony: OFF STATE UPDATE");
                new_state = _sym_off();
                break;
#if LL_SYM_FOTA_EN
            case SYM_FOTA:
                LL_TRACE_LOG("Symphony: FOTA STATE UPDATE");
                new_state = _sym_fota();
                break;
#endif // LL_SYM_FOTA_EN
            case SYM_STATE_ERROR:
            default:
                ll_bh_sym_on_error_evnt(SYM_TASK_STATE_ERROR);
                LL_TRACE_LOG("Symphony: ERROR STATE UPDATE");
                new_state = _sym_reset_state();
                break;
#if LL_SYM_USE_RTOS
        }
#endif // LL_SYM_USE_RTOS

        // Handle Downlink.
        _sym_downlink_check();

        // Check if we should reset the module.
        if (s_err_count > IFC_MAX_ERRS)
        {
            LL_WARNING_LOG("Symphony: Recieved MAX IFC Errors... Reseting.");
            new_state = SYM_RESET;
        }
        // time-tag transitions (for error checking later)
        if (s_current_state != new_state)
        {
            ll_bh_sym_on_state_change_evnt(new_state, s_current_state);
            gettime(&s_state_entry_time);
        }
        s_current_state = new_state;

#if LL_SYM_FOTA_EN
        // Process FOTA State Machine.
        _sym_cycle_ftp();
#endif // LL_SYM_FOTA_EN

#if LL_SYM_USE_RTOS
        ll_bh_sym_on_suspend_evnt();
        ulTaskNotifyTake(pdTRUE, _sym_get_task_delay());
        ll_bh_sym_on_resume_evnt();
#endif // LL_SYM_USE_RTOS
    }
}

//---------------------------------  State Processing Functions  --------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Reset State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_reset_state(void)
{
    LL_INFO_LOG("Symphony: Reseting Module.");

    // Config Module Pins
    ll_bh_sym_power_set(LL_PIN_HIGH);  // _set_power(SYM_POWER_ON);
    ll_bh_sym_boot_set(LL_PIN_HIGH);   // _set_boot(SYM_NORMAL_MODE);
    ll_bh_sym_reset_set(LL_PIN_LOW);   // _set_reset(SYM_IN_RESET);

    ll_bh_hal_sleep_ms(1000);

    ll_bh_sym_reset_set(LL_PIN_HIGH); // _set_reset(SYM_NOT_IN_RESET);

    ll_bh_hal_sleep_ms(SYM_POST_RESET_DELAY_MS);

    if (++s_clear_flash_count >= IFC_CLEAR_FLASH)
    {
        ll_settings_delete();
        ll_bh_hal_sleep_ms(6000);
        s_clear_flash_count = 0;
    }

    s_err_count = 0;
    s_tx_in_progress = false;

    LL_INFO_LOG("Symphony: Reset Complete.");

    return SYM_INITIALIZING;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Initializing State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_initializing(void)
{
    sym_module_state_t next_state = SYM_INITIALIZING;
    int32_t ifc_ret;
    err_check_ret_t err;

    LL_DEBUG_LOG("Symphony: Initializing...");

    // Verify the Module is in Symphony Link, NOT NOMAC.
    ifc_ret = ll_mac_mode_set(SYMPHONY_LINK);
    err     = _sym_ll_ifc_error_check(ifc_ret); // This will reset the module if it fails 3x.

    if (err != NO_ERR)
    {
        LL_ERROR_LOG("Symphony: Setting Mac Mode to Symphony Link Failed! (%i)", ifc_ret);
        return next_state;
    }

    // TODO: Log Symphony Configuration.

    // Verify the module has our user defined configuration.
    ifc_ret = ll_config_set(
            s_sym_config.sym_network_token,
            s_sym_config.sym_application_token,
            s_sym_config.sym_dl_mode,
            s_sym_config.sym_qos);
    err     = _sym_ll_ifc_error_check(ifc_ret); // This will reset the module if it fails 3x.

    if (err != NO_ERR)
    {
        LL_ERROR_LOG("Symphony: Setting Symphony Configuration Failed! (%i)", ifc_ret);
        return next_state;
    }

    // Transition to the Scanning State.
    next_state = SYM_SCANNING;

    LL_INFO_LOG("Symphony: Module Initialized Successfully.");

    return next_state;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Scanning State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_module_scanning(void)
{
    sym_module_state_t next_state = SYM_SCANNING;

    if (NO_ERR == _sym_get_module_status(false))
    {
        switch (s_sym_state)
        {
            case LL_STATE_IDLE_CONNECTED:
                LL_INFO_LOG("Symphony: Connected to Gateway!");
                s_tx_fail_count = 0;
                next_state = SYM_READY;
                break;
            case LL_STATE_INITIALIZING:
                LL_TRACE_LOG("Symphony: Initializing Gateway Connection...");
                if (_sym_time_to_now_ms(&s_state_entry_time) > SYM_INITIALIZING_TIMEOUT_MS)
                {
                    LL_WARNING_LOG("Symphony: Gateway Initialization Timed Out!");
                    next_state = SYM_NOT_READY;
                }
                break;
            case LL_STATE_IDLE_DISCONNECTED:
                if (_sym_time_to_now_ms(&s_state_entry_time) > SYM_DISCONNECTED_TIMEOUT_MS)
                {
                    LL_WARNING_LOG("Symphony: Gateway Connection FAILED!");
                    next_state = SYM_NOT_READY;
                }
                break;
            default:
                LL_ERROR_LOG("Symphony: Module State Error in Scanning State!");
                ll_bh_sym_on_error_evnt(SYM_MODULE_STATE_ERROR);
                next_state = SYM_RESET;
                break;
        }

        // Process other pending commands.
        next_state = _sym_process_cmd_queue(next_state);
    }

    return (next_state);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Not Ready State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_not_ready(void)
{
    sym_module_state_t next_state = SYM_NOT_READY;

    if (NO_ERR == _sym_get_module_status(false))
    {
        if (s_sym_state == LL_STATE_IDLE_CONNECTED)
        {
            LL_INFO_LOG("Symphony: Connected to Gateway!");
            s_tx_fail_count = 0;
            next_state = SYM_READY;
        }
        else if (_sym_time_to_now_ms(&s_state_entry_time) > _sym_get_connection_int_ms())
        {
            LL_WARNING_LOG("Symphony: Timed out waiting for Gateway Connection! Reseting...");
            // haven't connected in a long time, try again
            _sym_inc_connection_modifier();
            next_state = SYM_RESET;
        }
        else
        {
            LL_ERROR_LOG("Symphony: Timed out waiting for Gateway Connection! User must submit RESET or REINIT cmd.");
            // module hasn't successfully connected, wait for user to issue reset/reinit command
            next_state = _sym_process_cmd_queue(SYM_NOT_READY);
        }
    }
    return (next_state);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the Ready State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_ready(void)
{
    sym_module_state_t next_state = SYM_READY;

    // Get status of module.
    if (NO_ERR == _sym_get_module_status(s_tx_in_progress)) // Force poll if tx is in progress.
    {
        if (s_sym_state != LL_STATE_IDLE_CONNECTED)
        {
            LL_ERROR_LOG("Symphony: TX Failed, (Gateway Disconnected)!");

            _sym_flush_tx_queue(); // Cancel tx messages/flush tx msg queue.

            next_state = SYM_NOT_READY; // Transition to not ready state.
        }
        else
        {
            LL_DEBUG_LOG("Symphony: Check TX and RX status");
            _sym_process_tx_state(); // Check TX status.
            _sym_process_rx_state(); // check RX status.

            // Determine next state.
            if (s_tx_fail_count > SYM_MAX_TX_FAILURES)
            {
                LL_ERROR_LOG("Symphony: Too many failed TX Attempts!");
                ll_bh_sym_on_error_evnt(SYM_MAX_TX_FAILURES_ERR);
                _sym_flush_tx_queue(); // Flush TX queue.
                next_state = SYM_NOT_READY; // Transition to not ready state.
            }
            else
            {
                next_state = _sym_process_cmd_queue(next_state); // Check for commands.
            }
        }
    }

    // Bookkeeping.
    s_connect_int_modifier = 0;
    gettime(&s_last_ready_time);

    return next_state;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handle the OFF State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_off(void)
{
    return (_sym_process_cmd_queue(SYM_OFF));
}

#if LL_SYM_FOTA_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Handles the FOTA State.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_fota(void)
{
    _sym_process_rx_state();
    _sym_process_tx_state();

    return SYM_FOTA;
}
#endif // LL_SYM_FOTA_EN

//----------------------------------  Utility Functions  ----------------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Checks for Symphony Downlink if applicable in the user configuration.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_downlink_check(void)
{
    if (s_sym_config.sym_dl_mode == (enum ll_downlink_mode)(DOWNLINK_MODE_MAILBOX))
    {
        if ((s_mailbox_check_inhibit == false) && (s_current_state == SYM_READY) &&
            (s_sym_config.sym_mailbox_check_int_s != SYM_MANUAL_MAILBOX_CHECK))
        {
            if (_sym_time_to_now_ms(&s_last_dl_check_time) >
                (s_sym_config.sym_mailbox_check_int_s * 1000))
            {
                sym_enqueue_cmd(SYM_CMD_CHECK_MAILBOX);
                gettime(&s_last_dl_check_time);
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Checks the return of ll_ifc calls to verify success and track any errors.
///
/// param[in] ifc_ret The return from the ifc call.
///
/// returns The Symphony Task action based on the amount of errors recieved.
///////////////////////////////////////////////////////////////////////////////////////////////////
static err_check_ret_t _sym_ll_ifc_error_check(int32_t ifc_ret)
{
    err_check_ret_t rst;

    if (ifc_ret < 0)
    {
        s_err_count++;
        rst = ERR_NO_RESET;
        ll_bh_sym_on_error_evnt(SYM_IFC_ERR);
    }
    else
    {
        s_err_count = 0;
        rst = NO_ERR;
    }

    if (s_err_count > IFC_MAX_ERRS)
    {
        ll_bh_sym_on_error_evnt(SYM_IFC_ERR_RESET);
        rst = ERR_RESET;
    }

    return (rst);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Checks the return of ll_ifc calls to verify success and track any errors.
///
/// param[in] ifc_ret The return from the ifc call.
///
/// returns The Symphony Task action based on the amount of errors recieved.
///////////////////////////////////////////////////////////////////////////////////////////////////
static err_check_ret_t _sym_get_module_status(bool force)
{
    err_check_ret_t err = NO_ERR;
    uint32_t irq_flags_tmp;

    if (_sym_time_to_now_ms(&s_status_update_time) > SYM_MIN_STATUS_UPDATE_RATE_MS)
    {
        force = true;
    }
    // read module state & IRQ flags

    if (ll_bh_sym_io_get() || force)
    {
        LL_DEBUG_LOG("Symphony: Polling module status (%s)", force ? "forced" : "io0");

        gettime(&s_status_update_time);

        err = _sym_ll_ifc_error_check(ll_get_state(&s_sym_state, &s_tx_state, &s_rx_state));
        if (NO_ERR != err)
        {
            return err;
        }

        err = _sym_ll_ifc_error_check(ll_irq_flags(0xFFFFFFFF, &irq_flags_tmp));
        if (NO_ERR != err)
        {
            return err;
        }
        s_irq_flags |= irq_flags_tmp;
    }

    return (err);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Send the message to the module.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_process_tx_state(void)
{
    int32_t ifc_ret;
    err_check_ret_t err;
    sym_tx_msg_struct_t tx_msg_data;

    // Check for messages to transmit (if not waiting on any messages).
    if (s_tx_in_progress == false)
    {
#if LL_SYM_USE_RTOS
        if (xQueueReceive(s_sym_tx_queue, &tx_msg_data, 0) == pdTRUE)
        {
#else
        uint8_t index;

        //if (fifo_get(&s_sym_tx_queue, &index))
        {
           s_sym_tx_queue_cap--;

           tx_msg_data.msg     = s_tx_queue_data[index].msg;
           tx_msg_data.msg_len = s_tx_queue_data[index].msg_len;
           tx_msg_data.ack     = s_tx_queue_data[index].ack;
           tx_msg_data.port    = s_tx_queue_data[index].port;
#endif // LL_SYM_USE_RTOS
            LL_DEBUG_LOG("Symphony: Sending message to module...");
            ifc_ret = ll_message_send(tx_msg_data.msg,
                    tx_msg_data.msg_len,
                    tx_msg_data.ack,
                    tx_msg_data.port);

            err = _sym_ll_ifc_error_check(ifc_ret);

            if (NO_ERR != err)
            {
                // Notify task that message failed.
                LL_ERROR_LOG("Symphony: Failed to send message!");
                ll_bh_sym_on_tx_done(false);
            }
            else
            {
                LL_INFO_LOG("Symphony: Message sent!");
                gettime(&s_msg_send_time);
                s_tx_in_progress = true;
            }
        }
    }
    else
    {
        // check transmit state
        switch (s_tx_state)
        {
            case LL_TX_STATE_ERROR:
                // notify task that message failed
                ll_bh_sym_on_tx_done(false);
                LL_DEBUG_LOG("Symphony: TX failed!");
                s_tx_in_progress = false;
                s_tx_fail_count++;
                break;
            case LL_TX_STATE_SUCCESS:
                ll_bh_sym_on_tx_done(true);
                LL_DEBUG_LOG("Symphony: TX success!");
                s_tx_in_progress = false;
                s_tx_fail_count = 0;
                break;
            case LL_TX_STATE_TRANSMITTING:
            default:
                LL_DEBUG_LOG("Symphony: Waiting for tx done...");
                // no action--wait until done
                if (_sym_time_to_now_ms(&s_msg_send_time) > SYM_TX_TIMEOUT_MS)
                {
                    // timeout
                    LL_DEBUG_LOG("Symphony: TX timeout!");
                    ll_bh_sym_on_tx_done(false);
                    s_tx_in_progress = false;
                    s_tx_fail_count++;
                }
                break;
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Recieve the message from the module.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_process_rx_state(void)
{
    uint8_t port;
    switch (s_rx_state)
    {
        case LL_RX_STATE_RECEIVED_MSG:
            s_dl_msg_len = sizeof(s_dl_msg_bfr);
            while (s_rx_state == LL_RX_STATE_RECEIVED_MSG)
            {
                LL_INFO_LOG("Symphony: Recieved a message!");
                if (ll_retrieve_message(s_dl_msg_bfr, &s_dl_msg_len, &port, NULL, NULL) >= 0)
                {
#if LL_SYM_FOTA_EN
                    if (128 == port)
                    {
                        LL_TRACE_LOG("Symphony: Processing FTP Message.");
                        ll_ftp_msg_process(&s_ftp, s_dl_msg_bfr, s_dl_msg_len);
                    }
                    else
                    {
#endif // LL_SYM_FOTA_EN
                        LL_TRACE_LOG("Symphony: Passing message to the user.");
                        ll_bh_message_rx(LL_SYM_BACKHAUL, s_dl_msg_bfr, s_dl_msg_len, port);
#if LL_SYM_FOTA_EN
                    }
#endif // LL_SYM_FOTA_EN
                    sleep_ms(1); // wait one tick for module to load next msg (if any)
                    _sym_get_module_status(true); // update rx state
                }
                else
                {
                    break;
                }
            }
            break;
        default:
            break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Check and process any user submitted commands.
///
/// param[in] state_in The state the module is currently in.
///
/// returns The next module state.
///////////////////////////////////////////////////////////////////////////////////////////////////
static sym_module_state_t _sym_process_cmd_queue(sym_module_state_t state_in)
{
    sym_module_state_t next_state = state_in;
    sym_cmd_t cmd;

#if LL_SYM_USE_RTOS
    if (xQueueReceive(s_sym_cmd_queue, &cmd, 0) == pdTRUE)
#else
    //if (fifo_get(&s_sym_cmd_queue, (uint8_t*) &cmd))
#endif // LL_SYM_USE_RTOS
    {
        switch (cmd)
        {
            case SYM_CMD_REINIT:
                LL_INFO_LOG("Symphony: Reinitialize Module.");
                s_clear_flash_count = IFC_CLEAR_FLASH;
                _sym_flush_tx_queue();
                next_state = SYM_RESET;
                break;
            case SYM_CMD_RESYNC: // placeholder for resync command
            case SYM_CMD_RST:
                _sym_flush_tx_queue();
                next_state = SYM_RESET;
                break;
            case SYM_CMD_CHECK_MAILBOX:
                if (s_current_state == SYM_READY)
                {
                    LL_INFO_LOG("Symphony: Downlink Check.");
                    if (NO_ERR == _sym_ll_ifc_error_check(ll_mailbox_request()))
                    {
                        LL_DEBUG_LOG("Symphony: Mailbox check issued");
                        gettime(&s_dl_check_timeout_time);
                        s_dl_check_timeout_time.tv_sec += SYM_DL_CHECK_TIMEOUT_S;
                    }
                }
                break;
            case SYM_CMD_POWERDOWN:
                LL_INFO_LOG("Symphony: Power Down.");
                ll_bh_sym_power_set(LL_PIN_LOW); // _set_power(SYM_POWER_OFF);
                ll_bh_sym_boot_set(LL_PIN_HIGH); // _set_boot(SYM_NORMAL_MODE);
                ll_bh_sym_power_set(LL_PIN_LOW); // _set_reset(SYM_NOT_IN_RESET);
                _sym_flush_tx_queue();
                next_state = SYM_OFF;
                break;
            default:
                break;
        }
    }
    return (next_state);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Calculates the difference in time between a given time and the current time.
///
/// param[in] t0 The previous time that we want to find the diffence of.
///
/// returns the difference of time between t0 and when the function is called.
///////////////////////////////////////////////////////////////////////////////////////////////////
static int32_t _sym_time_to_now_ms(struct time *t0)
{
    // Get the time "now".
    struct time now;
    gettime(&now);

    // Convert times to absolute milliseconds.
    int32_t now_ms   = S_TO_MS(now.tv_sec) + NS_TO_MS(now.tv_nsec);
    int32_t time_ms  = S_TO_MS(t0->tv_sec) + NS_TO_MS(t0->tv_nsec);

    // Calculate the amount of time spend in the state (in milliseconds).
    return now_ms - time_ms;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Returns the user defined connection interval in milliseconds.
///
/// returns the user defeined connection interval in milliseconds.
///////////////////////////////////////////////////////////////////////////////////////////////////
static uint32_t _sym_get_connection_int_ms(void)
{
    uint32_t ret;

    if (s_sym_config.sym_max_conn_int_s < s_sym_config.sym_min_conn_int_s)
    {
        // user mis-configured task, just use min_connect_interval_s
        ret = s_sym_config.sym_min_conn_int_s;
    }
    else
    {
        ret = s_sym_config.sym_min_conn_int_s;
        ret += s_connect_int_modifier;
        if (ret > s_sym_config.sym_max_conn_int_s)
        {
            ret = s_sym_config.sym_max_conn_int_s;
        }
    }

    // convert to ms
    return (ret * 1000);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Increments the connection modifier to keep track of the amount of connection attempts.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_inc_connection_modifier(void)
{
    // confirm modifier is not already over limit before incrementing
    if ((s_sym_config.sym_min_conn_int_s + s_connect_int_modifier) <
        s_sym_config.sym_max_conn_int_s)
    {
        s_connect_int_modifier = (s_connect_int_modifier * 2) + 1;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Clears the TX Queue of any pending messages.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_flush_tx_queue(void)
{
    LL_DEBUG_LOG("Symphony: Flushing TX QUEUE!");

    // Cancel TX messages/flush TX msg queue.
    if (s_tx_in_progress == true)
    {
        ll_bh_sym_on_tx_done(false);
        s_tx_in_progress = false;
    }

    // Flush TX Queue.
#if LL_SYM_USE_RTOS
    while (xQueueReceive(s_sym_tx_queue, &s_tx_msg_data, 0) == pdTRUE)
    {
        ll_bh_sym_on_tx_done(false);
    }
#else
    uint8_t byte;
    //while (fifo_get(&s_sym_tx_queue, &byte))
    {
        s_sym_tx_queue_cap--;
        ll_bh_sym_on_tx_done(false);
    }
#endif // LL_SYM_USE_RTOS
}

#if LL_SYM_USE_RTOS
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Calculates the amount of time the RTOS Symphony Task can delay between updates.
///
/// returns the amount of ticks the task can sleep.
///////////////////////////////////////////////////////////////////////////////////////////////////
static portTickType _sym_get_task_delay(void)
{
    portTickType ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
    uint32_t ms_temp;
    // determine appropriate length of time to sleep
    switch (s_current_state)
    {
        case SYM_READY:
            if (s_tx_in_progress)
            {
                ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
            }
            else
            {
                switch (s_sym_net_config.dl_mode)
                {
                    case DOWNLINK_MODE_ALWAYS_ON:
                        ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        break;
                    default:
                        if (uxQueueMessagesWaiting(s_sym_tx_queue) > 0)
                        {
                            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        }
                        else if (uxQueueMessagesWaiting(s_sym_cmd_queue) > 0)
                        {
                            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        }
                        else if (s_sym_task_config.mailbox_check_int_s == SYM_MANUAL_MAILBOX_CHECK)
                        {
                            ret = portMAX_DELAY;
                        }
                        else if (_sym_time_to_now_ms(s_last_dl_check_time) >
                               S_TO_MS(s_sym_task_config.mailbox_check_int_s))
                        {
                            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        }
                        else if (_sym_time_to_now_ms(s_dl_check_timeout_time) >= 0)
                        {
                            // just initiated a mailbox check, poll more frequently until timeout
                            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
                        }
                        else
                        {
                            ms_temp = S_TO_MS(s_sym_task_config.mailbox_check_int_s) -
                                      _sym_time_to_now_ms(s_last_dl_check_time);
                            ret = (ms_temp / portTICK_PERIOD_MS);
                        }
                        break;
                }
            }
            break;
        case SYM_NOT_READY:
            if (_sym_time_to_now_ms(&s_state_entry_time) > _sym_get_connection_int_ms())
            {
                ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
            }
            else if ((downlink_mode_t)(s_sym_net_config.dl_mode) == DOWNLINK_MODE_ALWAYS_ON)
            {
                ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
            }
            else
            {
                ms_temp = _sym_get_connection_int_ms() - _sym_time_to_now_ms(&s_state_entry_time);
                ret = (ms_temp / portTICK_PERIOD_MS);
            }
            break;
        case SYM_OFF:
            ret = portMAX_DELAY;
            break;
        case SYM_FOTA:
            ret = (SYM_FOTA_TASK_RATE_MS / portTICK_PERIOD_MS);
            break;
        case SYM_RESET:
        case SYM_INITIALIZING:
        case SYM_SCANNING:
        default:
            ret = (SYM_ACTIVE_TASK_RATE_MS / portTICK_PERIOD_MS);
            break;
    }

    return (ret);
}
#endif // LL_SYM_USE_RTOS

//------------------------------  Standard Symphony Link HAL  -----------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Get the current time.
///
/// param[out] tp Current value of the clock.
///
/// returns 0 - success, negative otherwise.
///////////////////////////////////////////////////////////////////////////////////////////////////
int32_t gettime(struct time *tp)
{
#if LL_SYM_USE_RTOS
    TickType_t systime_ms = xTaskGetTickCount() * portTICK_RATE_MS;

    tp->tv_sec = MS_TO_S(systime_ms);
    tp->tv_nsec = MS_TO_NS(systime_ms) % S_TO_NS(tp->tv_sec);
    LL_TRACE_LOG("Symphony Get Time | %i:%i", tp->tv_sec, tp->tv_nsec);
    return 0;
#else
    ll_hal_ret_t ret = ll_bh_hal_gettime(tp);
    LL_TRACE_LOG("Symphony Get Time | %i:%i", tp->tv_sec, tp->tv_nsec);
    return ret == LL_HAL_RET_OK ? 0 : -100;
#endif // LL_SYM_USE_RTOS
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Sleep for a number of milliseconds.
///
/// param[in] millis number of milliseconds.
///
/// returns 0 - success, negative otherwise.
///////////////////////////////////////////////////////////////////////////////////////////////////
int32_t sleep_ms(int32_t millis)
{
    LL_TRACE_LOG("Sleep for %i milliseconds", millis);
#if LL_SYM_USE_RTOS
    vTaskDelay(millis / portTICK_PERIOD_MS);
    LL_TRACE_LOG("Done sleeping.");
    return 0;
#else
    ll_hal_ret_t ret = sleep_ms(millis);
    LL_TRACE_LOG("Done sleeping.");
    return ret == LL_HAL_RET_OK ? 0 : -200;
#endif // LL_SYM_USE_RTOS
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Read data from the Link Lab's module.
///
/// This function is usually a simple UART wrapper.
///
/// param[in,out] buff The buffer that will be modified with the data read from the module. The
///                    size of buff must be at least len bytes.
/// param[in] len The number of bytes to read.
///
/// returns 0 - success, negative otherwise.
///////////////////////////////////////////////////////////////////////////////////////////////////
int32_t transport_read(uint8_t *buff, uint16_t len)
{
    ll_hal_ret_t ret = ll_bh_hal_uart_write(LL_SYM_BACKHAUL, buff, len);
    return ret == LL_HAL_RET_OK ? 0 : -300;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Write data to the Link Lab's module.
///
/// This function is usually a simple UART wrapper.
///
/// param[in] buff The buffer containing the data to write to the module. The size of buff must be
///                at least len bytes.
/// param[in] len The number of bytes to write.
///
/// returns 0 - success, negative otherwise.
///////////////////////////////////////////////////////////////////////////////////////////////////
int32_t transport_write(uint8_t *buff, uint16_t len)
{
    ll_hal_ret_t ret = ll_bh_hal_uart_read(LL_SYM_BACKHAUL, buff, len);
    return ret == LL_HAL_RET_OK ? 0 : -400;
}

//----------------------------------------  FOTA HAL  -------------------------------------------//
#if LL_SYM_FOTA_EN
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Updates the FOTA state machine to processes the file transfer.
///
/// returns Next Symphony Module State.
///////////////////////////////////////////////////////////////////////////////////////////////////
static void _sym_cycle_ftp(void)
{
    if (_sym_time_to_now_ms(s_fota_cycle_time) > 1000)
    {
        ll_ftp_msg_process(&s_ftp, NULL, 0);
        gettime(s_fota_cycle_time);
    }

    if ((s_ftp.state != IDLE) && (s_current_state != SYM_FOTA))
    {
        LL_INFO_LOG("Symphony: FTP State Machine is ACTIVE, switching to FOTA STATE!");
        _sym_flush_tx_queue();
        ll_bh_sym_on_state_change_evnt(SYM_FOTA, s_current_state);
        s_current_state = SYM_FOTA;
        memset(&s_ftp_vars, 0, sizeof(sym_ftp_var_t));
    }
    else if ((s_ftp.state == IDLE) && (s_current_state == SYM_FOTA))
    {
        LL_INFO_LOG("Symphony: FTP Transfer has completed!");
        sleep_ms(SYM_FTP_DONE_TIMEOUT_MS);

        _sym_get_module_status(true);
        if (s_sym_state == LL_STATE_IDLE_CONNECTED)
        {
            s_current_state = SYM_READY;
            LL_INFO_LOG("Symphony: Connected to Gateway, Ready-to-Go!");
        }
        else
        {
            s_current_state = SYM_NOT_READY;
            LL_INFO_LOG("Symphony: NOT Connected to Gateway, NOT Ready-to-Go!");
        }
        ll_bh_sym_on_state_change_evnt(s_current_state, SYM_FOTA);
        s_ftp_config.ftp_done(s_ftp_vars.success, s_ftp_vars.file_id, s_ftp_vars.file_ver,
                              s_ftp_vars.file_sz);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to open and allocate storage when a new file transfer begins.
/// This is called at the beginning of the file transfer.
///
/// @param[in] file_id ID of the incoming file
/// @param[in] file_version Version of the incoming file
/// @param[in] file_size Size (in bytes) of the incoming file
///
/// @return Return code to indicate success/error of operation
///////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_open_callback(uint32_t file_id, uint32_t file_version,
                                               uint32_t file_size)
{
    sym_ftp_ret_t ret;

    s_ftp_vars.file_id = file_id;
    s_ftp_vars.file_id = file_version;
    s_ftp_vars.file_sz = file_size;
    s_ftp_vars.success = false;
    ret = s_ftp_config.ftp_file_open(file_id, file_version, file_size); // TODO

    switch (ret)
    {
        case FTP_OK:
            return (LL_FTP_OK);
            break;
        case FTP_WRONG_FILE_ID:
        case FTP_DUPLICATE_VERSION:
            return (LL_FTP_NO_ACTION);
            break;
        case FTP_FLASH_ERROR:
        default:
            return (LL_FTP_ERROR);
            break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to read a chunk of file in memory
///
/// @param[in] file_id ID of the file
/// @param[in] file_version Version of the file
/// @param[in] offset Offset to begin reading file from
/// @param[out] payload Buffer to read requested bytes into
/// @param[in] len Number of bytes to read
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_read_callback(uint32_t file_id, uint32_t file_version,
                                               uint32_t offset, uint8_t *payload, uint16_t len)
{
    sym_ftp_ret_t ret;

    ret = s_ftp_config.ftp_file_read(file_id, file_version, offset, payload, len); // TODO

    switch (ret)
    {
        case FTP_OK:
            return (LL_FTP_OK);
            break;
        case FTP_WRONG_FILE_ID:
        case FTP_DUPLICATE_VERSION:
            return (LL_FTP_NO_ACTION);
            break;
        case FTP_FLASH_ERROR:
        default:
            return (LL_FTP_ERROR);
            break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine to write a chunk of file to memory
///
/// @param[in] file_id ID of the file
/// @param[in] file_version Version of the file
/// @param[in] offset Offset to begin writing file at
/// @param[in] payload Buffer with data to be written
/// @param[in] len Number of bytes to write
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_write_callback(uint32_t file_id, uint32_t file_version,
                                                uint32_t offset, uint8_t *payload, uint16_t len)
{
    sym_ftp_ret_t ret;

    ret = s_ftp_config.ftp_file_write(file_id, file_version, offset, payload, len); // TODO

    switch (ret)
    {
        case FTP_OK:
            return (LL_FTP_OK);
            break;
        case FTP_WRONG_FILE_ID:
        case FTP_DUPLICATE_VERSION:
        case FTP_WRONG_VERSION:
            return (LL_FTP_NO_ACTION);
            break;
        case FTP_FLASH_ERROR:
        default:
            return (LL_FTP_ERROR);
            break;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when memory reading/writing is complete. This is called once all
/// segments have been received, when a file transfer is interrupted, or a file transfer is
/// canceled.
///
/// param[in] file_id The 32-bit unique ID of the file.
/// param[in] file_version The 32-bit unique file version.
///
/// @return Return code to indicate success/error of operation
////////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_close_callback(uint32_t file_id, uint32_t file_version)
{
    if (s_ftp_config.ftp_file_close == NULL)
    {
        return (LL_FTP_OK);
    }
    else if (s_ftp_config.ftp_file_close(file_id, file_version) == FTP_OK) // TODO
    {
        return (LL_FTP_OK);
    }
    else
    {
        return (LL_FTP_ERROR);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when FTP is complete.
///
/// @param[in] success 'true' if transfer was successful, 'false' otherwise
/// @param[in] file_id ID of the transferred file
/// @param[in] file_version Version of the transferred file
/// @param[in] file_size Size (in bytes) of the transferred file
///////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_apply_callback(uint32_t file_id, uint32_t file_version,
                                                uint32_t file_size)
{
    s_ftp_vars.success = true;
    return (LL_FTP_OK);
}

// TODO: Setup predefined FTP Calls.
#if 0
///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when an uplink message should be sent.
///
/// @param[in] buf The message data.
/// @param[in] len The message length.
/// @param[in] acked If the message should be acked.
/// @param[in] port The port of the message.
///////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_send_uplink_callback(uint8_t *buf, uint8_t len, bool acked,
                                                      uint8_t port)
{
    int32_t ret;

    ret = ll_message_send(buf, len, acked, port);

    if (ret < 0)
    {
        return (LL_FTP_ERROR);
    }

    return (LL_FTP_OK);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
/// Called by the LL FTP engine when Downlink Always ON is required.
///
/// @param[in] downlink_on 'true' if Downlink Mode should be Always On, else it doesn't matter.
///////////////////////////////////////////////////////////////////////////////////////////////////
static ll_ftp_return_code_t _ftp_dl_config_callback(bool downlink_on)
{
    ll_ftp_return_code_t ret = LL_FTP_OK;
    enum ll_downlink_mode dl_mode_tmp;

    dl_mode_tmp = (downlink_on) ? LL_DL_ALWAYS_ON : LL_DL_MAILBOX;
    if (ll_config_set(s_sym_net_config.net_token, s_sym_net_config.app_token, dl_mode_tmp,
                      s_sym_net_config.qos) < 0)
    {
        ret = LL_FTP_ERROR;
    }

    return (ret);
}

ll_ftp_return_code_t ll_bh_hal_sym_ftp_dl_config_t(bool downlink_on)
{
	if (sym_dl_mode != LL_DL_ALWAYS_ON)
	{
        static ll_downlink_mode last_dl_mode = sym_dl_mode;
        sym_dl_mode = downlink_on ? LL_DL_ALWAYS_ON : last_dl_mode;
        ll_bh_update_config(LL_SYM_BACKHAUL);
	}
    return LL_FTP_OK;
}

ll_ftp_return_code_t ll_bh_hal_sym_ftp_send_uplink_t(const uint8_t *buf, uint8_t len, bool acked,
        uint8_t port)
{
    sym_acked = acked;
    ll_ret_t = ll_backhaul_tx(LL_SYM_BACKHAUL, buf, len, port);
    return ll_ret_t == LL_RET_OK ? LL_FTP_OK : LL_FTP_ERROR;
}
#endif


#endif // LL_SYM_FOTA_EN
#endif // LL_BH_SYMPHONY_EN
