//
// \file    packet_queue.c
// \brief   Generic packet queue functions.
// \details
//      Functions used for initializing, pushing and popping to/from a generic packet
//      queue.
// \author  Scott Wohler
// \author  Thomas Steinholz
// \version 0.0.2
//
// \copyright LinkLabs, 2017
//

// Includes ------------------------------------------------------------------
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#ifdef USE_FREERTOS
#include "FreeRTOS.h"
#include "semphr.h"
#endif

#include "bsp.h"
#include "debug_print.h"

#include "packet_queue.h"

/*
 * Returns EXIT_SUCCESS/EXIT_FAILURE if things are good/bad
 */
#ifdef USE_FREERTOS
int32_t packet_queue_init(packet_queue_t* p, uint32_t size_bytes)
#else
int32_t packet_queue_init(packet_queue_t* p, uint8_t* buffer, uint32_t size_bytes)
#endif
{
    LL_ASSERT(p!=NULL);
    LL_ASSERT(size_bytes > 0);

    if (p == NULL)
    {
        return EXIT_FAILURE;
    }

    LOGF_DEBUG("packet_queue_init: %ld", size_bytes);

#ifdef USE_FREERTOS
    p->buf = (uint8_t*) pvPortMalloc(size_bytes);
    if(p->buf == NULL)
    {
        return EXIT_FAILURE;
    }

    p->mutex = xSemaphoreCreateMutex();
    if( p->mutex == NULL )
    {
        return EXIT_FAILURE;
    }
#else
    LL_ASSERT(buffer != NULL);
    p->buf = buffer;
#endif

    p->len = size_bytes;
    p->put_idx = 0;
    p->get_idx = 0;
    p->bytes_available = size_bytes;
    p->num_packets = 0;

    memset(p->buf, 0x00, size_bytes);

    return EXIT_SUCCESS;
}

/*
 *  Returns 0 if all is good, negative if errors:
 *  -1 Not enough room to push
 *  -2 Invalid arguments
 */
int32_t packet_queue_push_back(packet_queue_t* p, const packet_info_t* d, const uint8_t* payload)
{
    LL_ASSERT(p!=NULL);
    LL_ASSERT(d!=NULL);
    LL_ASSERT(payload!=NULL);

    if (p == NULL || d == NULL || (payload == NULL && d->length != 0))
    {
        return -2;
    }
    LL_ASSERT(d->length > 0);

    if (d->length > PKT_QUEUE_MAX_PAYLOAD_LENGTH)
    {
        return -2;
    }

    uint32_t num_bytes_needed = sizeof(packet_info_t) + (uint32_t)d->length + 1;

#ifdef USE_FREERTOS
    if( xSemaphoreTake( p->mutex, ( TickType_t ) (PKT_QUEUE_MAX_TIME_TO_BLOCK_MS / portTICK_PERIOD_MS )) != pdTRUE )
    {
        LL_ASSERT(false);
    }
#endif

    if (p->bytes_available < num_bytes_needed)
    {
#ifdef USE_FREERTOS
        if (pdTRUE != xSemaphoreGive( p->mutex ))
        {
            LL_ASSERT(false);
        }
#endif
        return(-1);
    }

    LOGF_DEBUG(
        "packet_queue_push_back, PKT:%ld, N:%ld L:%d A:%ld, P:%ld G:%ld",
        p->num_packets,
        num_bytes_needed,
        d->length,
        p->bytes_available,
        p->put_idx,
        p->get_idx
    );

    // If here then we have enough room. Start copying...
    // First save the packet descriptor
    uint32_t i;
    for (i = 0; i < sizeof(packet_info_t); i++)
    {
        p->buf[p->put_idx] = ((uint8_t*)(d))[i];

        // Increment and rollover put index
        p->put_idx = (p->put_idx + 1) % p->len;

        // Update number of unused bytes availables in the queue
        LL_ASSERT(p->bytes_available > 0);
        p->bytes_available--;
    }

    // Then add the payload
    for (i = 0; i < d->length; i++)
    {
        p->buf[p->put_idx] = payload[i];

        // Increment and rollover put index
        p->put_idx = (p->put_idx + 1) % p->len;

        // Update number of unused bytes availables in the queue
        LL_ASSERT(p->bytes_available > 0);
        p->bytes_available--;
    }

    // Another full packet on the queue
    p->num_packets++;

#ifdef USE_FREERTOS
    if (pdTRUE != xSemaphoreGive( p->mutex ))
    {
        LL_ASSERT(false);
    }
#endif

    return(0);
}

/*
 *  Returns 0 if all is good, negative if errors:
 *  -1 Not enough room to push
 *  -2 Invalid arguments
 */
int32_t packet_queue_push_front(packet_queue_t* p, const packet_info_t* d, const uint8_t* payload)
{
    LL_ASSERT(p!=NULL);
    LL_ASSERT(d!=NULL);
    LL_ASSERT(payload!=NULL);

    if (p == NULL || d == NULL || payload == NULL)
    {
        return -2;
    }

    LL_ASSERT(d->length > 0);
    LL_ASSERT(d->length > 6);
    if (d->length > PKT_QUEUE_MAX_PAYLOAD_LENGTH)
    {
        return -3;
    }

    uint32_t num_bytes_needed = sizeof(packet_info_t) + (uint32_t)d->length + 1;

#ifdef USE_FREERTOS
    if( xSemaphoreTake( p->mutex, ( TickType_t ) (PKT_QUEUE_MAX_TIME_TO_BLOCK_MS / portTICK_PERIOD_MS )) != pdTRUE )
    {
        LL_ASSERT(false);
    }
#endif

    if (p->bytes_available < num_bytes_needed)
    {
#ifdef USE_FREERTOS
        if (pdTRUE != xSemaphoreGive( p->mutex ))
        {
            LL_ASSERT(false);
        }
#endif
        return -1;
    }

    // We're pushing to the front of the queue/FIFO, push bytes in reverse order
    // If here then we have enough room. Start copying...
    LOGF_DEBUG(
        "packet_queue_push_front, PKT:%ld, N:%ld L:%d A:%ld, P:%ld G:%ld",
        p->num_packets,
        num_bytes_needed,
        d->length,
        p->bytes_available,
        p->put_idx,
        p->get_idx
    );

    // First add the payload in reverse order
    uint32_t i;
    for (i = d->length; i > 0; i--)
    {
        // Decrement and roll under the get_idx
        if (p->get_idx == 0)
        {
            p->get_idx = (p->len - 1);
        }
        else
        {
            p->get_idx = (p->get_idx - 1);
        }

        p->buf[p->get_idx] = payload[i-1];

        // Update number of unused bytes available in the queue
        LL_ASSERT(p->bytes_available > 0);
        p->bytes_available--;
    }

    // Then save the packet descriptor
    for (i = sizeof(packet_info_t); i > 0; i--)
    {
        // Decrement and roll under the get_idx
        if (p->get_idx == 0)
        {
            p->get_idx = p->len;
        }
        else
        {
            p->get_idx = (p->get_idx - 1);
        }

        p->buf[p->get_idx] = ((uint8_t*)(d))[i-1];

        // Update number of unused bytes availables in the queue
        LL_ASSERT(p->bytes_available > 0);
        p->bytes_available--;
    }

    // Another full packet on the queue
    p->num_packets++;

#ifdef USE_FREERTOS
    if (pdTRUE != xSemaphoreGive( p->mutex ))
    {
        LL_ASSERT(false);
    }
#endif
    return(0);
}

/*
 *  Returns 0 if all is good, negative if errors:
 *  -1 No packets to pop
 *  -2 Invalid arguments
 */
int32_t packet_queue_pop_front(packet_queue_t* p, packet_info_t* d, uint8_t* payload)
{
    LL_ASSERT(p!=NULL);
    LL_ASSERT(d!=NULL);
    LL_ASSERT(payload!=NULL);

    if (p == NULL || d == NULL || payload == NULL)
    {
        return -2;
    }

#ifdef USE_FREERTOS
    if( xSemaphoreTake( p->mutex, ( TickType_t ) (PKT_QUEUE_MAX_TIME_TO_BLOCK_MS / portTICK_PERIOD_MS )) != pdTRUE )
    {
        LL_ASSERT(false);
    }
#endif

    if (p->num_packets == 0)
    {
#ifdef USE_FREERTOS
        if (pdTRUE != xSemaphoreGive( p->mutex ))
        {
            LL_ASSERT(false);
        }
#endif
        return -1;
    }

    // If here then we have a packet. Start copying out...

    // First copy out the packet descriptor
    uint32_t i;
    for (i = 0; i < sizeof(packet_info_t); i++)
    {
        ((uint8_t*)(d))[i] = p->buf[p->get_idx];

        // Increment and rollover get index
        p->get_idx = (p->get_idx + 1) % p->len;

        // Update number of unused bytes available in the queue
        p->bytes_available++;
    }

    // Verify that we're not about to clobber the payload buffer
    if (d->length > PKT_QUEUE_MAX_PAYLOAD_LENGTH)
    {
#ifdef USE_FREERTOS
        if (pdTRUE != xSemaphoreGive( p->mutex ))
        {
            LL_ASSERT(false);
        }
#endif
        return -3;
    }

    // Then copy out the payload
    for (i = 0; i < d->length; i++)
    {
        payload[i] = p->buf[p->get_idx];

        // Increment and rollover get index
        p->get_idx = (p->get_idx + 1) % p->len;

        // Update number of unused bytes available in the queue
        p->bytes_available++;
    }

    // One less full packet on the queue
    p->num_packets--;

    LOGF_DEBUG(
        "packet_queue_pop_front, PKT:%ld, L:%d A:%ld, P:%ld G:%ld",
        p->num_packets,
        d->length,
        p->bytes_available,
        p->put_idx,
        p->get_idx
    );

#ifdef USE_FREERTOS
    if (pdTRUE != xSemaphoreGive( p->mutex ))
    {
        LL_ASSERT(false);
    }
#endif

    return(0);
}

// Take a peek at the info of the packet at the front of the queue
// Returns 0 if a valid peek, -1 if nothing to peek

int32_t packet_queue_peek_front(packet_queue_t* p, pkt_info_t* pkt_info)
{
    int32_t result = 0;

    LL_ASSERT (p != NULL);
    LL_ASSERT (pkt_info != NULL);

#ifdef USE_FREERTOS
    if( xSemaphoreTake( p->mutex, ( TickType_t ) (PKT_QUEUE_MAX_TIME_TO_BLOCK_MS / portTICK_PERIOD_MS )) != pdTRUE )
    {
        LL_ASSERT(false);
    }
#endif

    if (0 == packet_queue_num_packets(p))
    {
        result = -1;
    }
    else
    {
        *pkt_info = p->buf[p->get_idx];
    }

#ifdef USE_FREERTOS
    if (pdTRUE != xSemaphoreGive( p->mutex ))
    {
        LL_ASSERT(false);
    }
#endif

    return result;
}

void packet_queue_flush(packet_queue_t* p)
{
    LL_ASSERT(p!=NULL);

    if (p == NULL)
    {
        return;
    }
#ifdef USE_FREERTOS
    if( xSemaphoreTake( p->mutex, ( TickType_t ) (PKT_QUEUE_MAX_TIME_TO_BLOCK_MS / portTICK_PERIOD_MS )) != pdTRUE )
    {
        LL_ASSERT(false);
    }
#endif

    p->get_idx = p->put_idx;
    p->bytes_available = p->len;
    p->num_packets = 0;

#ifdef USE_FREERTOS
    if (pdTRUE != xSemaphoreGive( p->mutex ))
    {
        LL_ASSERT(false);
    }
#endif
}

/*
 *  Returns 0 if all is good, negative if errors:
 *  -1 No packets to pop
 *  -2 Invalid arguments
 *  -3 Index out of range
 *  -4 Could not find index
 */
int32_t packet_queue_peek(packet_queue_t *p, uint32_t index, packet_info_t *d)
{
    LL_ASSERT(p!=NULL);
    LL_ASSERT(d!=NULL);

    if (p == NULL || d == NULL)
    {
        return -2; // Invalid arguments.
    }

#ifdef USE_FREERTOS
    if( xSemaphoreTake( p->mutex, ( TickType_t ) (PKT_QUEUE_MAX_TIME_TO_BLOCK_MS / portTICK_PERIOD_MS )) != pdTRUE )
    {
        LL_ASSERT(false);
    }
#endif

    if (p->num_packets == 0)
    {
#ifdef USE_FREERTOS
        if (pdTRUE != xSemaphoreGive( p->mutex ))
        {
            LL_ASSERT(false);
        }
#endif
        return -1; // No packets to pop.
    }

    if (index > p->num_packets)
    {
#ifdef USE_FREERTOS
        if (pdTRUE != xSemaphoreGive( p->mutex ))
        {
            LL_ASSERT(false);
        }
#endif
        return -3; // Index out of range.
    }

    // Go through the entire packet queue buffer
    for (uint32_t pkt_idx = 0; pkt_idx < p->num_packets; pkt_idx += d->length)
    {
        // Copy out the packet descriptor
        for (uint32_t i = 0; i < sizeof(packet_info_t); i++)
        {
            ((uint8_t*)(d))[i] = p->buf[p->pkt_idx];

            // Increment and rollover get index
            p->pkt_idx = (p->pkt_idx + 1) % p->len;
        }

        if (pkt_idx == index)
        {
            return 0;
        }
    }

#ifdef USE_FREERTOS
    if (pdTRUE != xSemaphoreGive( p->mutex ))
    {
        LL_ASSERT(false);
    }
#endif

    // Clear packet_info if execution made it to this point.
    d->length = 0;
    d->port   = 0;
    d->id     = 0;
    d->status = 0;
    d->state  = 0;

    return -4; // Could not find index.
}

int32_t packet_queue_pop_index(packet_queue_t *p, uint32_t index, packet_info_t *d, uint8_t *payload)
{
    LL_ASSERT(p!=NULL);
    LL_ASSERT(d!=NULL);

    if (p == NULL || d == NULL)
    {
        return -2; // Invalid arguments.
    }

#ifdef USE_FREERTOS
    if( xSemaphoreTake( p->mutex, ( TickType_t ) (PKT_QUEUE_MAX_TIME_TO_BLOCK_MS / portTICK_PERIOD_MS )) != pdTRUE )
    {
        LL_ASSERT(false);
    }
#endif

    if (p->num_packets == 0)
    {
#ifdef USE_FREERTOS
        if (pdTRUE != xSemaphoreGive( p->mutex ))
        {
            LL_ASSERT(false);
        }
#endif
        return -1; // No packets to pop.
    }

    if (index > p->num_packets)
    {
#ifdef USE_FREERTOS
        if (pdTRUE != xSemaphoreGive( p->mutex ))
        {
            LL_ASSERT(false);
        }
#endif
        return -3; // Index out of range.
    }

    uint32_t bytes_removed = 0;
    uint32_t iterations = 0;

    // Go through the entire packet queue buffer
    for (uint32_t pkt_idx = 0; pkt_idx < p->num_packets; ++pkt_idx)
    {
        // Copy out the packet descriptor
        for (uint32_t i = 0; i < sizeof(packet_info_t); i++)
        {
            ((uint8_t*)(d))[i] = p->buf[p->pkt_idx];

            // Increment and rollover get index
            p->pkt_idx = (p->pkt_idx + 1) % p->len;

            // Update number of unused bytes available in the queue
            bytes_removed++;
        }

        // Verify that we're not about to clobber the payload buffer
        if (d->length > PKT_QUEUE_MAX_PAYLOAD_LENGTH)
        {
#ifdef USE_FREERTOS
            if (pdTRUE != xSemaphoreGive( p->mutex ))
            {
                LL_ASSERT(false);
            }
#endif
            return -4;
        }

        // Then copy out the payload
        for (i = 0; i < d->length; i++)
        {
            payload[i] = p->buf[p->pkt_idx];

            // Increment and rollover get index
            p->pkt_idx = (p->pkt_idx + 1) % p->len;

            // Update number of unused bytes available in the queue
            bytes_removed++;
        }

        if (iterations == index)
        {
            // One less full packet on the queue
            p->num_packets--;

            // Account for all the bytes we removed
            p->bytes_available += bytes_removed;

            LOGF_DEBUG(
                "packet_queue_pop_index, PKT:%ld, I:%d L:%d A:%ld, P:%ld G:%ld",
                p->num_packets,
                index,
                d->length,
                p->bytes_available,
                p->put_idx,
                p->get_idx);

#ifdef USE_FREERTOS
            if (pdTRUE != xSemaphoreGive( p->mutex ))
            {
                LL_ASSERT(false);
            }
#endif
            return 0;
        }

        ++iterations;
    }

#ifdef USE_FREERTOS
    if (pdTRUE != xSemaphoreGive( p->mutex ))
    {
        LL_ASSERT(false);
    }
#endif

    // Clear packet_info if execution made it to this point.
    d->length = 0;
    d->port   = 0;
    d->id     = 0;
    d->status = 0;
    d->state  = 0;

    return -4; // Could not find index.

}

uint32_t packet_queue_num_packets(packet_queue_t* p)
{
    LL_ASSERT(p!=NULL);

    if (p == NULL)
    {
        return 0;
    }
    return p->num_packets;
}

uint32_t packet_queue_usage_percentage(packet_queue_t* p)
{
    uint32_t ret;

    LL_ASSERT(p!=NULL);
    if (p == NULL)
    {
        return 0;
    }

    ret = 100 - ((100 * p->bytes_available) / p->len);

    return ret;
}
