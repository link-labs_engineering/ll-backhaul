///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                  Thomas Steinholz  <thomas.steinholz@link-labs.com>,     August 2018        ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////


#include "ll-lte_m-task.h"
#if LL_BH_LTE_M_EN

#include "string.h"
#include <inttypes.h>



///////////////////////////////////////////////////////////////////////////////////////////////////
// ---------------------------------  Driver Configuration  -------------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////

#define LTE_MSG_TIMEOUT       (45) ///< The amount of seconds before a msg is considered failed.
#define LTE_COMMS_TIMEOUT     (45) ///< Maximum amount of seconds to wait for LTE comms to work.
#define LTE_DEBUG_PRINTS           ///< Show additional debug prints.
#undef MISS_PKT_DBG


///////////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------- Private Variable Declarations --------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////

typedef enum
{
    LTE_RET_SUCCESS   = 0,                       ///< Success
    LTE_RET_NON_FATAL = 1,                       ///< Failure is recoverable.
    LTE_RET_FATAL     = 2,                       ///< Failure is not recoverable.
} lte_ret_type_t; ///< Simplified LTE_IFC return type.

typedef struct
{
    uint32_t packet_id;                           ///< UUID for every LTE message.
    uint8_t buffer[MAX_USER_UPLINK_LENGTH_BYTES]; ///< Message Buffer.
    uint16_t len;                                 ///< Length of the message buffer.
    uint8_t port;                                 ///< Port to send the message on.
    uint8_t attempt;                              ///< Current attempt number to send this message.
    struct time timestamp;                        ///< The time the message was last attempted to
                                                  ///< get sent.
} lte_msg_t; ///< Data Structure to manage any LTE message.

lte_msg_t _tx_msgs[LL_LTE_MAX_TX_MSGS];           ///< Storage for all TX messages.
static lte_callbacks_t _callbacks;                ///< User defined callbacks.
static lte_stats_t _stats;                        ///< Track stats on the LTE driver.
static bool _host_irq_high;                       ///< Is there a pending IRQ flag?
static bool _lte_operational;                     ///< Is the module currently operational?


///////////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------- Private Function Declarations --------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////

static lte_return_t _push_message(uint8_t *buff, uint16_t len, uint8_t port);
static void _retrieve_message(void);
static void _handle_lte_assert(void);
static bool _non_fatal_error(int32_t ret);
static void _verify_tx(void);
static lte_ret_type_t _verify_ret(int32_t ret_code);
static void _resend_msg(lte_msg_t *msg);
static void _delete_msg(lte_msg_t *msg);
static void _debug_show_tx_queue(void);

static void _cb_tx_done(uint8_t num_txed);
static void _cb_msg_failed(uint8_t *buff, uint16_t len, int8_t port);
static void _cb_rx(uint8_t *buff, uint16_t len, int8_t port);

// DEBUG
#ifdef MISS_PKT_DBG
static uint16_t s_num_invalid_pkt_ids;

int16_t get_invalid_pkt_num(void)
{
    return s_num_invalid_pkt_ids;
}
#endif //MISS_PKT_DBG

///////////////////////////////////////////////////////////////////////////////////////////////////
//--------------------------------- Public Function Definitions ---------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////

lte_return_t lte_init_driver(lte_callbacks_t *callbacks)
{
    // Register Callbacks
    if (callbacks)
    {
        _callbacks.context              = callbacks->context;
        _callbacks.lte_message_failed   = callbacks->lte_message_failed;
        _callbacks.lte_tx_queue_cleared = callbacks->lte_tx_queue_cleared;
        _callbacks.lte_received_message = callbacks->lte_received_message;
    }

    _host_irq_high = false;

    // Initialize TX MSGS Buffer
    for (uint16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
    {
        _delete_msg(&_tx_msgs[i]);
    }

    // Reset Module
    //LL_LOGF("LTE: Reset LTE-M device..."); // HAL should reset LTE on init.
    _lte_operational = false;

    // Disable UART
    ll_bh_disable_uart();
    return LTE_OK;
}

lte_return_t lte_send_message(uint8_t *buff, uint16_t size, int8_t port)
{
    return (!buff || !size) ? LTE_INVALID_PARAM : _push_message(buff, size, port);
}

lte_return_t lte_mailbox_req(void)
{
    return _push_message(NULL, 0, 0);
}

bool lte_irq_requires_service(void)
{
    return _host_irq_high;
}

void lte_set_irq_high(void)
{
    _host_irq_high = true;
}

void lte_service_irq(void)
{
    uint32_t irq_flags = 0x0;
    uint32_t flags_to_clear = 0x0;
    lte_ret_type_t ret = LTE_RET_SUCCESS;

    do // Handle any IRQ Flags.
    {
        do // Get the IRQ Flags.
        {
            ret = lte_ifc_irq_flags(0x0, &irq_flags);

            if (ret == LTE_RET_NON_FATAL)
            {
                ll_hal_ret_t hr = ll_bh_hal_sleep_ms(25);
                LL_ASSERT(hr == LL_RET_OK);
            }
            else if (ret == LTE_RET_FATAL)
            {
                LL_LOGF("LTE: Unable to retrieve IRQ, LTEM non-operational");
                ll_bh_disable_uart();
                return;
            }
        } while (_verify_ret(ret) != LTE_RET_SUCCESS);

        // Parse IRQ Flags.
        if (LTE_IRQ_FLAGS_ASSERT & irq_flags)
        {
            flags_to_clear |= LTE_IRQ_FLAGS_ASSERT;
            LL_LOGF("LTE: Received %s IRQ Flag!",
                   lte_ifc_irq_flag_desc(LTE_IRQ_FLAGS_ASSERT));
            _handle_lte_assert();
        }
        if (LTE_IRQ_FLAGS_RX_DONE & irq_flags || LTE_IRQ_FLAGS_RX_QUEUE_FULL & irq_flags)
        {
            if (irq_flags & LTE_IRQ_FLAGS_RX_DONE)
            {
                flags_to_clear |= LTE_IRQ_FLAGS_RX_DONE;
                LL_LOGF("LTE: Received %s IRQ Flag!",
                        lte_ifc_irq_flag_desc(LTE_IRQ_FLAGS_RX_DONE));
            }
            if (irq_flags & LTE_IRQ_FLAGS_RX_QUEUE_FULL)
            {
                flags_to_clear |= LTE_IRQ_FLAGS_RX_QUEUE_FULL;
                LL_LOGF("LTE: Received %s IRQ Flag!",
                       lte_ifc_irq_flag_desc(LTE_IRQ_FLAGS_RX_QUEUE_FULL));
            }
            _retrieve_message();
        }
        if (LTE_IRQ_FLAGS_TX_DONE & irq_flags || LTE_IRQ_FLAGS_TX_STATUS_QUEUE_FULL & irq_flags)
        {
            if (irq_flags & LTE_IRQ_FLAGS_TX_DONE)
            {
                flags_to_clear |= LTE_IRQ_FLAGS_TX_DONE;
                LL_LOGF("LTE: Received %s IRQ Flag!",
                        lte_ifc_irq_flag_desc(LTE_IRQ_FLAGS_TX_DONE));
            }
            if (irq_flags & LTE_IRQ_FLAGS_TX_STATUS_QUEUE_FULL)
            {
                flags_to_clear |= LTE_IRQ_FLAGS_TX_STATUS_QUEUE_FULL;
                LL_LOGF("LTE: Received %s IRQ Flag!",
                        lte_ifc_irq_flag_desc(LTE_IRQ_FLAGS_TX_STATUS_QUEUE_FULL));
            }

            // When polling IRQ flags from the module quickly, the module will send a duplicate event.
            // Verify the driver has a message to verify before doing so...
            if (lte_get_tx_queue_size())
            {
                _verify_tx();
            }
            else
            {
                LL_LOGF("LTE: [Warning] Ignoring TX DONE Flag, no pending message!");
            }
        }
        if (LTE_IRQ_FLAGS_RX_ERROR & irq_flags)
        {
            flags_to_clear |= LTE_IRQ_FLAGS_RX_ERROR;
            LL_LOGF("LTE: Received %s IRQ Flag!",
                    lte_ifc_irq_flag_desc(LTE_IRQ_FLAGS_RX_ERROR));
            LL_LOGF("LTE: RX ERROR Reported!");
        }
        if (LTE_IRQ_FLAGS_WDOG & irq_flags)
        {
            flags_to_clear |= LTE_IRQ_FLAGS_WDOG;
            LL_LOGF("LTE: Received %s IRQ Flag!",
                    lte_ifc_irq_flag_desc(LTE_IRQ_FLAGS_WDOG));
            LL_LOGF("LTE: Attempting to recover the device...");
            _lte_operational = false;
        }

        // Clear processed IRQ Flags
        ret = lte_ifc_irq_flags(flags_to_clear, &irq_flags);
        flags_to_clear = 0x0;
    } while (irq_flags);

    _host_irq_high = false; // IRQ has been serviced.
    ll_bh_disable_uart();
}

void lte_get_cell_info(uint32_t *cell_id, uint16_t *cell_tac, int32_t *info_age_s)
{
    *info_age_s = ll_difftime_from_now(&_stats.last_update);
    *cell_id  = _stats.last_cell_id;
    *cell_tac = _stats.last_cell_tac;
}

void lte_service_timeout(void)
{
    // Find the message in our cached data.
    for (uint16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
    {
        if (_tx_msgs[i].packet_id)
        {
            uint32_t delta = ll_difftime_from_now(&_tx_msgs[i].timestamp);

            if (delta > LTE_MSG_TIMEOUT)
            {
                LL_LOGF("LTE: Message %d timed out! %u", _tx_msgs[i].packet_id, delta);

                // Attempt to resend the message.
                //_resend_msg(&_tx_msgs[i]);
                _debug_show_tx_queue();
            }
        }
    }
}

void lte_get_stats(lte_stats_t *stats)
{
    *stats = _stats;
}

uint8_t lte_get_tx_queue_size(void)
{
    uint8_t count = 0;

    for (uint16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
    {
        if (_tx_msgs[i].packet_id)
        {
            ++count;
        }
    }

    return count;
}

bool lte_module_operational(void)
{
    return _lte_operational;
}

void lte_recover_module(void)
{
    int32_t ret;
    static int32_t last_ret;
    ll_hal_ret_t hr;
    static struct time last_check_time = { 0, 0 };
    lte_version_t version = { 0, 0, 0 };
    static uint8_t attempt = 0;
    static bool reset = false;

    if (ll_difftime_from_now(&last_check_time) < 2)
    {
        return; // Do not spam the module.
    }

    // Update last Reset Time.
    hr = ll_bh_hal_gettime(&last_check_time);
    LL_ASSERT(hr == LL_RET_OK);

    // See if the module is operating...
    ret = lte_ifc_version_get(&version);
    if (ret == LTE_IFC_ACK)
    {
        _lte_operational = true;
        LL_LOGF("LTE: Device is Operating - v%d.%d.%d",
                version.major, version.minor, version.tag);

        // Resend the messages if the LTEM module required a reset.
        if (reset)
        {
            // Resend the TX Queue.
            for (uint16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
            {
                // See if there is a message in the slot.
                if (_tx_msgs[i].packet_id)
                {
                    // Resend the message.
                    _resend_msg(&_tx_msgs[i]);
                }
            }

            reset = false;
        }
    }
    else
    {
        _lte_operational = false;
        LL_LOGF("LTE: Version Get returned: %s", lte_ifc_ack_desc(ret));
    }

    // See if we can try another reset, based on time.
    if (last_ret != LTE_IFC_ACK)
    {
        if (attempt++ < 10)
        {
            // Hardware Reset...
            LL_LOGF("LTE: Hardware Reset!");
            ll_bh_lte_reset_pin_set(true);
            hr = ll_bh_hal_sleep_ms(500);
            LL_ASSERT(hr == LL_RET_OK);
            ll_bh_lte_reset_pin_set(false);
            ll_bh_hal_sleep_ms(250);
            LL_ASSERT(hr == LL_RET_OK);
            reset = true;
        }
        else
        {
            // Software Reset...
            last_ret = lte_ifc_reset_mcu();
            attempt = last_ret == LL_IFC_ACK ? 0 : attempt + 1;
            reset = true;
        }
    }

    ll_bh_disable_uart(); // Save power.
    return;
}



///////////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------- Private Function Definitions ---------------------------------//
///////////////////////////////////////////////////////////////////////////////////////////////////

static lte_return_t _push_message(uint8_t *buff, uint16_t len, uint8_t port)
{
    int32_t ret;

    for (uint16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
    {
        // When there is an avalible TX slot.
        if(!_tx_msgs[i].packet_id)
        {
            LL_LOGF("LTE: Queuing msg in slot %d", i);
            // Populate the message cache.
            _tx_msgs[i].len = len;
            _tx_msgs[i].port = port;
            memcpy(_tx_msgs[i].buffer, buff, _tx_msgs[i].len);
            ll_hal_ret_t hr = ll_bh_hal_gettime(&_tx_msgs[i].timestamp);
            LL_ASSERT(hr == LL_RET_OK);
            _tx_msgs[i].attempt++;

            // Try to send the message.
            LL_LOGF("LTE: Attempting to send message...");
            do
            {
                ret = buff && len ?
                    lte_ifc_message_send(
                        _tx_msgs[i].buffer,
                        _tx_msgs[i].len,
                        _tx_msgs[i].port,
                        &(_tx_msgs[i].packet_id)) :
                    lte_ifc_mailbox_request(&(_tx_msgs[i].packet_id));

                if (ret != 0)
                {
                    hr = ll_bh_hal_sleep_ms(50);
                    LL_ASSERT(hr == LL_RET_OK);
                }
            } while (_verify_ret(ret) == LTE_RET_NON_FATAL);

            if (ret != LTE_IFC_ACK)
            {
                ll_bh_disable_uart();
                return LTE_FAILED_TO_SEND;
            }

            _debug_show_tx_queue();
            ll_bh_disable_uart(); // Save power.
            return LTE_OK;
        }
    }
    _debug_show_tx_queue();
    ll_bh_disable_uart(); // Save power.
    return LTE_ERROR_QUEUE_FULL; // TX QUEUE is full.
}

static void _retrieve_message(void)
{
    int32_t ret;
    uint8_t payload[MAX_USER_UPLINK_LENGTH_BYTES];
    uint16_t length = MAX_USER_UPLINK_LENGTH_BYTES;
    uint8_t port;

    // Retrieve the message.
    ret = _verify_ret(lte_ifc_retrieve_message(payload, &length, &port));
    LL_ASSERT(ret == LTE_IFC_ACK);

    // Update stats.
    _stats.msgs_received++;

    // Notify User.
    _cb_rx(payload, length, port);
}

static void _verify_tx(void)
{
    int32_t ret;
    uint8_t new_status;
    uint32_t packet_id;
    int32_t packet_status;
    uint32_t packet_time_in_queue_ms;
    int16_t rsrq_db;
    int16_t rsrp_dbm;
    uint32_t cell_id;
    uint16_t cell_tac;
    uint8_t *payload;
    ll_hal_ret_t hr;
    bool msg_found = false;

    static uint8_t msgs_txed = 0;

    // LTE Workaround: Sometimes the LTE-M message queue is not populated by
    // the time the TX DONE flag is triggered, resulting in no new status for
    // the message. This code will poll the message status queue until a
    // message comes out because we know that there is a new status given the
    // TX Done IRQ Flag.
    do
    {
        ret = _verify_ret(lte_ifc_message_status_ext(&new_status, &packet_id,
            &packet_status, &packet_time_in_queue_ms, &rsrq_db, &rsrp_dbm,
            &cell_id, &cell_tac));

        if (ret == LTE_RET_NON_FATAL)
        {
            LL_LOGF("LTE: Poll Status Failed (%s) - Waiting 250ms...",
                    lte_ifc_ack_desc(ret));
            hr = ll_bh_hal_sleep_ms(250);
            LL_ASSERT(hr == LL_RET_OK);
        }
        else if (ret == LTE_RET_FATAL)
        {
            // Leave the function
            ll_bh_disable_uart();
            return;
        }
    } while (new_status != true && lte_get_tx_queue_size());

    // Update Stats.
    if (_stats.last_cell_id == cell_id && _stats.last_cell_tac == cell_tac)
    {
       // Find running average for the certain Cell ID/TAC.
        _stats.avg_rsrq_db += rsrq_db;
        _stats.avg_rsrq_db /= 2.0;
        _stats.avg_rsrp_dbm += rsrp_dbm;
        _stats.avg_rsrp_dbm /= 2.0;
    }
    else
    {
        // Reset the average when the Cell ID/TAC changes.
        _stats.avg_rsrq_db = rsrq_db;
        _stats.avg_rsrp_dbm = rsrp_dbm;
    }

    // Always update the last Cell ID/TAC last.
    _stats.last_cell_id = cell_id;
    _stats.last_cell_tac = cell_tac;

    hr = ll_bh_hal_gettime(&_stats.last_update);
    LL_ASSERT(hr == LL_RET_OK);

    // Find the message in our cached data.
    for (uint16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
    {
        // Identify the packet via id and verify the status is new.
        if (_tx_msgs[i].packet_id == packet_id)
        {
            msg_found = true;

            // Verify the transmission results.
            if (packet_status == LTE_IFC_ACK)
            {
                LL_LOGF("LTE: Message %d sent successfully!", packet_id);

                // Update stats.
                _stats.msgs_sent++;
                msgs_txed++;

                // Delete Message Cache.
                _delete_msg(&_tx_msgs[i]);
                _debug_show_tx_queue();

                break; // Get out of loop.
            }
            else
            {
                // Handle failed message tx.
                LL_LOGF("LTE: Message %d failed to send! (%s)", packet_id,
                    lte_ifc_ack_desc(packet_status));

                // Attempt to resend the message.
                _resend_msg(&_tx_msgs[i]);
                _debug_show_tx_queue();

                break; // Get out of loop.
            }
        }
    }

    if (!msg_found)
    {
        LL_LOGF("LTE: Could not find reference to sent message? %d %i", packet_id, (bool)new_status);
#ifdef MISS_PKT_DBG
        s_num_invalid_pkt_ids++;
#endif // MISS_PKT_DBG
    }

    // We transmitted everything in the queue.
    if (!lte_get_tx_queue_size())
    {
        // Notify user.
        _cb_tx_done(msgs_txed);
    }
}

static void _resend_msg(lte_msg_t *msg)
{
    LL_LOGF("LTE: Attempting to resend message!");
    _stats.msgs_failed++; // Update stats.

    // See if we can resend the message.
    if (msg->attempt <= LL_LTE_MAX_ATTEMPTS)
    {
        // Try to send the message.
        int32_t ret;
        ll_hal_ret_t hr;
        do
        {
            ret = msg->buffer && msg->len ?
                lte_ifc_message_send(msg->buffer, msg->len, msg->port, &(msg->packet_id)) :
                lte_ifc_mailbox_request(&(msg->packet_id));

            if (ret == LTE_RET_NON_FATAL)
            {
                LL_LOGF("LTE: Send Message Failed (%s) - Waiting 500ms...",
                    lte_ifc_ack_desc(ret));
                hr = ll_bh_hal_sleep_ms(500);
                LL_ASSERT(hr == LL_RET_OK);
            }
            else if (ret == LTE_RET_FATAL)
            {
                // Message has used all attempts.
                LL_LOGF("LTE: Message ID %d could not be sent, LTE-M is not operational", msg->packet_id);

                // Notify User.
                _cb_msg_failed(msg->buffer, msg->len, msg->port);

                // Delete Message Cache.
                _delete_msg(msg);

                ll_bh_disable_uart();
                return;
            }
        } while (_verify_ret(ret) != LTE_RET_SUCCESS);

        LL_LOGF("LTE: Resent with id: %d", msg->packet_id);
        _stats.msgs_retried++;
        msg->attempt++;
        hr = ll_bh_hal_gettime(&msg->timestamp);
        LL_ASSERT(hr == LL_RET_OK);
    }
    else
    {
        // Message has used all attempts.
        LL_LOGF("LTE: Used all attempts for %d", msg->packet_id);

        // Notify User.
        _cb_msg_failed(msg->buffer, msg->len, msg->port);

        // Delete Message Cache.
        _delete_msg(msg);
    }
}

static void _delete_msg(lte_msg_t *msg)
{
    memset(msg->buffer, 0xFF, MAX_USER_UPLINK_LENGTH_BYTES);
    msg->packet_id = 0;
    msg->len = 0;
    msg->port = 0;
    msg->attempt = 0;
    msg->timestamp.tv_sec = 0;
    msg->timestamp.tv_nsec = 0;
}

static void _debug_show_tx_queue(void)
{
#ifdef LTE_DEBUG_PRINTS
    LL_LOGF("");
    LL_LOGF("================== LTE TX QUEUE ==================");
    for (uint16_t i = 0; i < LL_LTE_MAX_TX_MSGS; ++i)
    {
        if (_tx_msgs[i].packet_id)
        {
            LL_LOGF("LTE: Slot %d | Packet ID: %d Length: %d Attempt: %d",
                i, _tx_msgs[i].packet_id, _tx_msgs[i].len, _tx_msgs[i].attempt);
        }
        else
        {
            LL_LOGF("LTE: Slot %d | No Message in Slot.", i);
        }
    }
    LL_LOGF("");
#endif // LTE_DEBUG_PRINTS
}

static void _handle_lte_assert(void)
{
    int32_t ret;
    char *filename;
    uint32_t line;
    uint32_t uptime;

    ret = lte_ifc_get_assert_info(filename, &line, &uptime);
    LL_ASSERT(ret == LTE_IFC_ACK);

    LL_LOGF("LTE: Device asserted in %s on line %d! Uptime: %d",
            filename, line, uptime);

    LL_LOGF("LTE: Attempting to recover the device...");
    _lte_operational = false;
}

static bool _non_fatal_error(int32_t ret)
{
    // This defines all of the LTE return codes that will not
    // result in a reset of the LTE-M device.

    // Note: These Macros are not in scope of the LTE IFC...
    return (ret == -103 ||  // SQN_ERR_QUEUE_FULL_BYTES
            ret == -104 ||  // SQN_ERR_QUEUE_FULL_NUM
            ret == -123 ||  // SQN_FOTA_IN_PROGRESS
            ret == -130 ||  // SQN_ERR_NOT_REGISTERED
            ret == -131);   // SQN_ERR_INIT_IN_PROGRESS
}

static lte_ret_type_t _verify_ret(int32_t ret_code)
{
    if (_non_fatal_error(ret_code))
    {
        LL_LOGF("LTE: Recieved the following error: %s", lte_ifc_ack_desc(ret_code));
        return LTE_RET_NON_FATAL;
    }
    else if (ret_code == 0)
    {
        return LTE_RET_SUCCESS;
    }
    else
    {
        LL_LOGF("LTE: Recieved the FATAL following error: %s", lte_ifc_ack_desc(ret_code));
        LL_LOGF("LTE: Attempting to recover the device...");
        
        _lte_operational = false;
        return LTE_RET_FATAL;
    }
}

static void _cb_tx_done(uint8_t num_txed)
{
    ll_bh_disable_uart(); // Save power.
    if (_callbacks.lte_tx_queue_cleared)
    {
        _callbacks.lte_tx_queue_cleared(num_txed, _callbacks.context);
    }
    else
    {
        LL_LOGF("LTE: WARNING: No tx_queue_cleared callback defined!");
    }
}

static void _cb_msg_failed(uint8_t *buff, uint16_t len, int8_t port)
{
    if (_callbacks.lte_message_failed)
    {
        _callbacks.lte_message_failed(buff, len, port, _callbacks.context);
    }
    else
    {
        LL_LOGF("LTE: WARNING: No message_failed callback defined!");
    }
}

static void _cb_rx(uint8_t *buff, uint16_t len, int8_t port)
{
    if (_callbacks.lte_received_message)
    {
        _callbacks.lte_received_message(buff, len, port, _callbacks.context);
    }
    else
    {
        LL_LOGF("LTE: WARNING: No received_message callback defined!");
    }
}



#endif // LL_BH_LTE_M_EN

