///////////////////////////////////////////////////////////////////////////////////////////////////
///                                                                                             ///
///                     ▓▓  ▒▒  ░░  _     _       _      _          _                           ///
///                     ▓▓  ▒▒     | |   (_)_ __ | | __ | |    __ _| |__  ___                   ///
///                     ▓▓  ▒▒▒▒▒▒ | |   | | '_ \| |/ / | |   / _` | '_ \/ __|                  ///
///                     ▓▓         | |___| | | | |   <  | |__| (_| | |_) \__ \                  ///
///                     ▓▓▓▓▓▓▓▓▓▓ |_____|_|_| |_|_|\_\ |_____\__,_|_.__/|___/                  ///
///                                                                                             ///
///                         Copyright (C) 2018 Link Labs - All Rights Reserved                  ///
///                Unauthorized copying of this file, via any medium is strictly prohibited     ///
///                                                                                             ///
///                     Thomas Steinholz  <thomas.steinholz@link-labs.com>, FEB 2018            ///
///                                                                                             ///
///////////////////////////////////////////////////////////////////////////////////////////////////

#include "ll-hal.h"
#include "ll-backhaul-hal.h"

#include "lte_ifc.h" // for lte_ifc_time

int32_t lte_ifc_hal_get_wake_status(bool* p_wake)
{
    // LL_LOGF("Get Wake Request!");
    *p_wake = (bool) ll_bh_lte_wake_status_pin_get();
    return 0;
}

int32_t lte_ifc_hal_get_irq_flags_gpio(bool* p_level)
{
    *p_level = (bool) ll_bh_lte_irq_pin_get();
    return 0;
}

int32_t lte_ifc_hal_gettime(struct lte_ifc_time *tp)
{
    // LL_LOGF("LTE: Get Time!");
    struct time sym_tp;
    ll_bh_hal_gettime(&sym_tp);
    tp->tv_sec = sym_tp.tv_sec;
    tp->tv_nsec = sym_tp.tv_nsec;
    return 0;
}

int32_t lte_ifc_hal_set_wake_request(bool wake)
{
    // LL_LOGF("LTE: Set Wake Request!");
    ll_bh_lte_wake_request_pin_set(wake);
    return 0;
}

int32_t lte_ifc_hal_write(uint8_t *buff, uint16_t len)
{
    // LL_LOGF("LTE: Write!");
    return ll_bh_hal_uart_write(LL_LTE_M_BACKHAUL, buff, len);
}

int32_t lte_ifc_hal_read_byte(uint8_t *buff)
{
    return ll_bh_hal_uart_read(LL_LTE_M_BACKHAUL, buff, 1);
}

int32_t transport_write(uint8_t *buff, uint16_t len)
{
    return ll_bh_hal_uart_write(LL_SYM_BACKHAUL, buff, len);
}

int32_t transport_read(uint8_t *buff, uint16_t len)
{
    return ll_bh_hal_uart_read(LL_SYM_BACKHAUL, buff, len);
}

int32_t gettime(struct time *tp)
{
    return ll_bh_hal_gettime(tp);
}

int32_t sleep_ms(int32_t millis)
{
    return ll_bh_hal_sleep_ms(millis);
}

